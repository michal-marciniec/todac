/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public final class ExceptionMessages {
	
	public static final String MESSAGE_UNKNOWN = "Unknown error occured";
	public static final String MESSAGE_VALIDATOR_SOLUTION = "Correct your arguments";

	// Configuration exceptions
	public static final ExceptionMessage CONFIGURATION_FILE = new ExceptionMessage(
			"Unable to load configuration from specified file",
			"First, make sure that specified file exists and is accessible by the application. Then check file format and its required content");
	
	public static final ExceptionMessage CONFIGURATION_NO_PROPERTY = new ExceptionMessage(
			"No specified property in the configuration file",
			"Make sure that your configuration file contains all required properties set properly");
	
	public static final ExceptionMessage CONFIGURATION_PROPERTY_MISSING = new ExceptionMessage(
			"Configuration property missing",
			"Check your configuration file");
	
	// File access exceptions	
	public static final ExceptionMessage FILE_PARSING = new ExceptionMessage(
			"Unable to parse provided source file",
			"Read TODAC documentation to get familiar with acceptable file formats");

	public static final ExceptionMessage UNKNOWN_FILE_FORMAT = new ExceptionMessage(
			"Unknown file format", 
			FILE_PARSING.solution);
	
	public static final ExceptionMessage FILE_ACCESS = new ExceptionMessage(
			"Unable to access specified file",
			"Make sure that provided file exists and is accessible");

	public static final ExceptionMessage INVALID_FILEPATH_SUPPLIED = new ExceptionMessage(
			"Unable to extract filename from the specified filepath",
			"Check if filename is compatible with desired format or delete your cache directory");

	// CLI exceptions
	public static final ExceptionMessage CLI_WRONG_ARGUMENTS = new ExceptionMessage(
			"Wrong command line arguments",
			"Type -h to see how to use program properly");
	
	public static final ExceptionMessage CLI_STRING_TOO_LONG = new ExceptionMessage(
			"Specified string exceeds maximum character length",
			"Try specifying a shorter string");
	
	public static final ExceptionMessage CLI_R_FILELIST_LABEL = new ExceptionMessage(
			"You may not use -R or -filelist options when you specify a label",
			"Run the program without the -label option or specify the label for each file separately");
	
	public static final ExceptionMessage CLI_R_FILELIST = new ExceptionMessage(
			"You may not use -R option with -filelist option",
			"Run the program without the -R or -filelist option");
	
	
	public static final ExceptionMessage CLI_VARIABLE_SET = new ExceptionMessage(
			"You may not use -variable and -set options at the same time",
			"Run the program without the -variable option or without the -set option");
	
	// RunInfo exceptions
	public static final ExceptionMessage THREADING = new ExceptionMessage(
			"Java VM threading exception",
			MESSAGE_UNKNOWN);
	
	public static final ExceptionMessage COMMAND_EXECUTION = new ExceptionMessage(
			"Unable to execute external command",
			"Check your environment variables");
	
	public static final ExceptionMessage NO_CMSRUN = new ExceptionMessage(
			"No 'cmsRun' in your environment",
			"Make sure you have CMSSW installed");
	
	public static final ExceptionMessage NO_NSFIND = new ExceptionMessage(
			"No 'nsfind' in your environment",
			"Make sure your have CASTOR service configured");
	
	public static final ExceptionMessage NO_STAGER_QRY = new ExceptionMessage(
			"No 'stager_qry' in your environment",
			"Make sure your have CASTOR service configured");
	
	public static final ExceptionMessage NO_STAGER_GET = new ExceptionMessage(
			"No 'stager_get' in your environment",
			"Make sure your have CASTOR service configured");
	
	public static final ExceptionMessage COMMAND_ASYNC_EXECUTION = new ExceptionMessage(
			"Unable to asynchronously execute external command",
			"Check your environment variables");
	
	public static final ExceptionMessage CASTOR_FILE_ACCESS = new ExceptionMessage(
			"Unable to access specified file on CASTOR",
			"Make sure that provided file exists and is accessible");
	
	public static final ExceptionMessage MISSING_RUN_PART = new ExceptionMessage(
			"One of the run parts (a VMEA file) has not been properly processed",
			"Run the application again or make sure that the desired files are located on CASTOR");
	
	public static final ExceptionMessage CMSSW_FILE_OUTPUT_PARSING = new ExceptionMessage(
			"Failed to parse cmsRun's file output",
			"Make sure that cmsRun uses the same Analyzer as specified in application's configuration. Then check exsistence of cmsRun's output file or its format");
	
	public static final ExceptionMessage CMSSW_STREAM_OUTPUT_PARSING = new ExceptionMessage(
			"Failed to parse cmsRun's output stream",
			"Make sure that cmsRun uses the same Analyzer as specified in application's configuration. Then check output stream's format and does it start and end with the flags specified in application's configuration");
	
	public static final ExceptionMessage CMSSW_SCRIPT_TEMPLATE_NOT_EXISTS = new ExceptionMessage(
			"Specified cmsRun script temaplate does not exist",
			"Make sure that template script file specified in configuration exists and is accessible");
	
	public static final ExceptionMessage CMSSW_SCRIPT_TEMPLATE_WRITE_FAIL = new ExceptionMessage(
			"Unable to create cmsRun script for a VMEA file failed",
			"Make sure that application has access to file writing");

	public static final ExceptionMessage CMSSW_COMMAND_EXECUTION_FAIL = new ExceptionMessage(
			"cmsRun execution failed",
			"Make sure your environment is adjusted to run cmsRun commands. Check other provided information about that error");
	
	// TUDAS exceptions
	public static final ExceptionMessage TUDAS_UNKNOWN = new ExceptionMessage(
			"Unknown excption has been thrown by TUDAS server",
			"Please get familiar with information from TUDAS below");

	public static final ExceptionMessage TUDAS_DEAD = new ExceptionMessage(
			"Unable to establish connection with TUDAS server",
			"Your TUDAS client is not up-to-date or TUDAS server is offline");

	public static final ExceptionMessage NO_SUCH_RUN = new ExceptionMessage(
			"There is no information about specified run in the database",
			"Populate database with run information first");

	public static final ExceptionMessage TUDAS_MANAGER_SAVE = new ExceptionMessage(
			"TUDAS manager was unable to save data into the database",
			TUDAS_UNKNOWN.solution);
	
	public static final ExceptionMessage TUDAS_MANAGER_LOAD = new ExceptionMessage(
			"TUDAS manager was unable to load data from the database",
			TUDAS_UNKNOWN.solution);
	
	public static final ExceptionMessage TUDAS_LOAD_RUN_TIME = new ExceptionMessage( 
			"Unable to load run's timestamp from database",
			NO_SUCH_RUN.solution);

	public static final ExceptionMessage TUDAS_GET_MANAGER = new ExceptionMessage( 
			"Unable to get instance of database manager",
			TUDAS_DEAD.solution);
	
	
	// LHC Logging Database exceptions
	public static final ExceptionMessage LHC_LOGGING_DB_UNKNOWN = new ExceptionMessage(
			"Unknown LHC Logging Database exception", 
			"Unknown LHC Logging Database exception. Try updating your LHC Logging Database client");
	
	
	public static final ExceptionMessage LHC_LOGGING_DB = new ExceptionMessage(
			"Unable to fetch data from LHC Logging Database", 
			"Your LHC Logging Database libraries are out of date or LHC Logging Database is offline");
	
	public static final ExceptionMessage LHC_LOGGING_DB_EXCEPTION_CONVERSION = new ExceptionMessage(
			"Unable to convert LHC Logging Database exception", 
			LHC_LOGGING_DB_UNKNOWN.solution);
	
	public static final ExceptionMessage LHC_LOGGING_DB_NO_SUCH_METHOD = new ExceptionMessage(
			"Unable to retrieve vector of double values from provided TimeseriesData", 
			LHC_LOGGING_DB_UNKNOWN.solution);
	
	public static final ExceptionMessage LHC_LOGGING_DB_UNKNOWN_VARIABLE_TYPE = new ExceptionMessage(
			"Unknown VariableType for General Measurement", 
			LHC_LOGGING_DB_UNKNOWN.solution);
	
	public static final ExceptionMessage LHC_LOGGING_DB_NO_SUCH_VARIABLE = new ExceptionMessage(
			"No such variable in the LHC Logging Database", 
			LHC_LOGGING_DB_UNKNOWN.solution);
	
	
	//Optics exceptions
	public static final ExceptionMessage OPTICS_NOT_FOUND = new ExceptionMessage(
			"Unable to found one of optics measurement in a specified directory", 
			"Please make sure that your source files are named properly. Note that it is strongly recommended to use option \"-type\" and specify each optics file separately. Also, please get familiar with the TODAC documentation");
	
	//Geometry exceptions
	public static final ExceptionMessage RP_NAME_INVALID = new ExceptionMessage(
			"Unable to convert Roman Pot name to database identificator", 
			"Please provide correct Roman Pot name");
	
	public static final ExceptionMessage LABEL_TOO_LONG = new ExceptionMessage(
			"Provided measurement label is too long to be inserted into the database", 
			"Please change label in order to meet database requirements");
	
	//RunInfo exceptions
	public static final ExceptionMessage RUNINFO_WRONG_FILENAME = new ExceptionMessage(
			"Invalid filename or this run consists of just one file",
			"Delete your CASTOR cache files");
	
	public static final ExceptionMessage RUNINFO_EXCEPTION_COLLECTION = new ExceptionMessage(
			"Multiple TUDAS exceptions have been thrown",
			"Check log file and get familiar with all exceptions messages");
	
	//Validator exceptions
	public static final ExceptionMessage VALIDATOR_INVALID_INTERVAL = new ExceptionMessage(
			"Interval is invalid",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_NEGATIVE_VALUE = new ExceptionMessage(
			"Value is negative",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_NON_POSITIVE_VALUE = new ExceptionMessage(
			"Value is not positive",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_IS_FILE = new ExceptionMessage(
			"Specified path does not point to a file",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_FILE_NOT_EXISTS = new ExceptionMessage(
			"Specified file does not exist",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_FILE_ACCESS = new ExceptionMessage(
			"Unable to access specified file",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage VALIDATOR_STRING_NULL_OR_EMPTY = new ExceptionMessage(
			"Specified value is null or empty",
			MESSAGE_VALIDATOR_SOLUTION);
	
	public static final ExceptionMessage INVALID_PARAMETERS = new ExceptionMessage(
			"Specified parameters are invalid",
			MESSAGE_VALIDATOR_SOLUTION);
	
//	public static final ExceptionMessage _ = new ExceptionMessage(
//			"", 
//			"");
	
//	public static final ExceptionMessage _ = new ExceptionMessage(
//			"", 
//			"");


	public static final ExceptionMessage SERVER_ERROR = new ExceptionMessage(
			"Server error",
			"Reload the webpage");
	

}
