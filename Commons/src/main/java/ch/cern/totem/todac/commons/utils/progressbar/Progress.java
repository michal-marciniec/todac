/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.utils.progressbar;

public class Progress implements IProgressStatus {

	protected final static float START_PERCENTS = 0.0f;

	protected float percents;

	public Progress(IProgressStatus status) {
		this.percents = status.get();
	}

	public Progress(float progress) {
		this.percents = progress;
	}

	public Progress() {
		reset();
	}

	@Override
	public float get() {
		return percents;
	}

	public void set(float percents) {
		this.percents = percents;
	}

	public void set(IProgressStatus status) {
		this.percents = status.get();
	}

	public void add(float percents) {
		this.percents += percents;
	}

	public void reset() {
		percents = START_PERCENTS;
	}

}
