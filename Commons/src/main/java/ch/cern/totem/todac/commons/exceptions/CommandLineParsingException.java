/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class CommandLineParsingException extends TodacException {

	private static final long serialVersionUID = 1L;

	public CommandLineParsingException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}

	public CommandLineParsingException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}

	public CommandLineParsingException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}
}
