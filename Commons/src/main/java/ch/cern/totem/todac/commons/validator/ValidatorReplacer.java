/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.validator;

public class ValidatorReplacer {

	public static String replaceNullOrEmpty(String what, String with){
		return what == null || what.isEmpty() ? with : what;
	}
	
}
