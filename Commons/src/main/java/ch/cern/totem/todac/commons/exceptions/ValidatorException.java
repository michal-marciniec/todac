/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class ValidatorException extends TodacException {

	private static final long serialVersionUID = 1L;

	public ValidatorException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}

	public ValidatorException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}
	
	public ValidatorException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}
	
	public ValidatorException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}
}
