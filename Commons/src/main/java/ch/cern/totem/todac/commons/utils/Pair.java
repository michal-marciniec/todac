/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.utils;

public class Pair<K, V> {

	protected K k;
	protected V v;
	
	public Pair(K first, V second){
		this.k = first;
		this.v = second;
	}
	
	public K getFirst(){
		return k;
	}
	
	public V getSecond(){
		return v;
	}
	
	public void setFirst(K first){
		this.k = first;
	}
	
	public void setSecond(V second){
		this.v = second;
	}
}
