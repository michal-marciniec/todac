/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class TodacException extends Exception {

	private static final long serialVersionUID = -2492348397416856568L;

	protected ExceptionMessage exceptionMessage = null;
	protected String message = null;
	protected String additionalInformation = null;
	protected Exception innerException = null;
	protected boolean tudasException = false;
	
	public TodacException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage.message + ", " + exceptionMessage.solution);
		this.exceptionMessage = exceptionMessage;
	}

	public TodacException(ExceptionMessage exceptionMessage, TudasException e) {
		super(exceptionMessage.message + ", " + exceptionMessage.solution, e);
		this.exceptionMessage = exceptionMessage;
		this.innerException = e;
		this.tudasException = true;
	}
	
	
	public TodacException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage.message + ", " + exceptionMessage.solution, e);
		this.exceptionMessage = exceptionMessage;
		this.innerException = e;
	}

	public TodacException(ExceptionMessage exceptionMessage, String additionalInformation, TudasException e) {
		super(exceptionMessage.message + " (" + additionalInformation + "(, " + exceptionMessage.solution, e);
		this.exceptionMessage = exceptionMessage;
		this.additionalInformation = additionalInformation;
		this.innerException = e;
		this.tudasException = true;
	}
	
	public TodacException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage.message + " (" + additionalInformation + "(, " + exceptionMessage.solution, e);
		this.exceptionMessage = exceptionMessage;
		this.additionalInformation = additionalInformation;
		this.innerException = e;
	}
	
	public TodacException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage.message + " (" + additionalInformation + "(, " + exceptionMessage.solution);
		this.exceptionMessage = exceptionMessage;
		this.additionalInformation = additionalInformation;
	}
	
	public String getSolution(){
		if(exceptionMessage == null) return null;
		return exceptionMessage.solution;
	}
	
	public String getMessage(){
		String msg = (exceptionMessage == null) ? message : exceptionMessage.message;
		msg = (additionalInformation == null) ? msg : msg + "\n" + additionalInformation;
		return msg;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("*** TodacException has been thrown ***");
		stringBuilder.append("\n\tMessage:\t" + getMessage());
		if(getCause() != null && getCause().getMessage() != null){
			stringBuilder.append("\n\tCause:\t\t" + getCause().getMessage());
		}
		if(getSolution() != null){
			stringBuilder.append("\n\tSolution:\t" + getSolution());
		}
		stringBuilder.append("\n*** StackTrace ***\n");
		StackTraceElement[] stackTraceElements = getStackTrace();
		for(StackTraceElement stackTraceElement : stackTraceElements){
			stringBuilder.append("\t").append(stackTraceElement).append("\n");
		}
		if(tudasException){
			stringBuilder.append("\n*** TudasException has been thrown ***");
			TudasException tudas = (TudasException)innerException;
			stringBuilder.append(tudas.toString());
		}
		stringBuilder.append("\n");
		return stringBuilder.toString();
	}
}
