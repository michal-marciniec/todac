/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class DatabaseException extends TodacException {

	private static final long serialVersionUID = 1L;

	public DatabaseException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}

	public DatabaseException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}

	public DatabaseException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}

	public DatabaseException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}
}
