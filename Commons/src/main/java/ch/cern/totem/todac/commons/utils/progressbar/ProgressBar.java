/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.utils.progressbar;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.utils.Out;

public final class ProgressBar {

	private final static Logger logger = Logger.getLogger(ProgressBar.class);
	
	private static final int SLEEP_TIME = 1000;
	private static final int MAX_PERCENT = 100;
	private static final int COLUMNS = 50;
	private static final String MESSAGE = "CURRENT PROGRESS:\t";
	private static final String BAR = "|";
	private static final String ARROW = ">";
	private static final String DONE = "=";
	private static final String NOT_DONE = ".";
	
	private MeasurableProgress populator;
	private Thread progressBarThread;
	private float totalProgress;
	
	public ProgressBar(MeasurableProgress populator){
		this.populator = populator;
	}
	
	public void start(){
		progressBarThread = new Thread(new Runnable(){
			public void run() {
				logger.debug("Progressbar started");
				totalProgress = Float.MIN_VALUE;
				
				while(totalProgress <= 1.0f){
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						logger.trace("Thread.sleep error / interrupted", e);
					}
					
					if(populator == null){
						logger.trace("No reference to object to measure it's progress");
						break;
					}
					Progress p = populator.getProgress();
					if(p == null){
						logger.warn("Progressbar received a null progress value");
						continue;
					}
					
					float progress = p.get();
					
					if(totalProgress == progress){
						continue;
					}
					
					printProgress(progress);
					totalProgress = progress;
				}
				logger.debug("Progressbar finished");
			}});
		
		progressBarThread.start();
	}
	
	public static void printProgress(float progress){
		int max = MAX_PERCENT;
		int columns = COLUMNS;
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(MESSAGE);
		stringBuilder.append(BAR);
		int percent = Math.round(progress * max);
		for(int i = 0 ; i < percent/(max / columns) - 1; ++i){
			stringBuilder.append(DONE);
		}
		if(percent != max){
			stringBuilder.append(ARROW);
		}
		for(int i = percent/(max / columns) + 1; i < columns; ++i){
			stringBuilder.append(NOT_DONE);
		}
		stringBuilder.append(BAR);
		stringBuilder.append(" ");
		stringBuilder.append("[");
		stringBuilder.append(percent);
		stringBuilder.append("%]");
		
		Out.println(stringBuilder);
		
	}

	public void stop(){
		if(progressBarThread != null){
			totalProgress = 1.0f;
			populator = null;
			progressBarThread.interrupt();
			printProgress(1.0f);
		}
	}

}
