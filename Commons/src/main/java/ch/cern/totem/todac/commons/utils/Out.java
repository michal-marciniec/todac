/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.utils;

import java.io.PrintStream;

public class Out {
	public static final PrintStream stream = System.out;

	public static void print(Object... objectArray) {
		for (Object object : objectArray) {
			stream.print(object.toString());
		}
	}

	public static void println(Object... objectArray) {
		if (objectArray.length == 0)
			stream.println();
		else
			for (Object object : objectArray) {
				stream.println(object.toString());
			}
	}

}
