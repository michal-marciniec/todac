/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.utils.progressbar;

public interface MeasurableProgress {
	Progress getProgress();
}
