/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class FileAccessException extends ValidatorException {

	private static final long serialVersionUID = -708021601147660956L;

	public FileAccessException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}
	
	public FileAccessException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}
	
	public FileAccessException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}
	
	public FileAccessException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}
}
