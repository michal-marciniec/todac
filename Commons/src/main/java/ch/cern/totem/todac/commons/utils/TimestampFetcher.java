/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.utils;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class TimestampFetcher {
	protected static final Logger logger = Logger.getLogger(TimestampFetcher.class);

	protected RunInformationManager runInfoManager;

	public TimestampFetcher(RunInformationManager runInfoManager) {
		this.runInfoManager = runInfoManager;
	}

	public long getRunStartTimestamp(int runNumber) throws DatabaseException {
		logger.debug("Fetching start timestamp for run " + runNumber);
		try {
			return runInfoManager.loadRunStartTime(runNumber);
		} catch (TudasException e) {
			throw new DatabaseException(ExceptionMessages.TUDAS_LOAD_RUN_TIME, e);
		}
	}

	public long getRunEndTimestamp(int runNumber) throws DatabaseException {
		logger.debug("Fetching end timestamp for run " + runNumber);
		try {
			if (runExists(runNumber))
				return runInfoManager.loadRunEndTime(runNumber);
			else
				throw new DatabaseException(ExceptionMessages.NO_SUCH_RUN, String.valueOf(runNumber));
		} catch (TudasException e) {
			throw new DatabaseException(ExceptionMessages.TUDAS_LOAD_RUN_TIME, e);
		}
	}

	public boolean runExists(int runNumber) throws DatabaseException {
		try {
			int[] existingRuns = runInfoManager.listAllRuns();

			for (int existingRunNumber : existingRuns) {
				if (runNumber == existingRunNumber)
					return true;
			}
			return false;
		} catch (TudasException e) {
			throw new DatabaseException(ExceptionMessages.TUDAS_UNKNOWN, e);
		}
	}
}
