/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class ArgumentFormatException extends ValidatorException {

	private static final long serialVersionUID = 298058329290475826L;

	public ArgumentFormatException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}

	public ArgumentFormatException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}
	
	public ArgumentFormatException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}
	
	public ArgumentFormatException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}

}
