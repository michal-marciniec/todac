package ch.cern.totem.todac.commons.validator;


import java.io.File;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.FileAccessException;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.ValidatorException;

public class Validator {

	public static void validateInterval(Number a, Number b) throws ValidatorException {
		if (b.doubleValue() < a.doubleValue()) {
			throw new InvalidArgumentException(
					ExceptionMessages.VALIDATOR_INVALID_INTERVAL,
					a + " > " + b);
		}
	}

	public static void validateNonNegative(Number a) throws ValidatorException {
		if (a.doubleValue() < 0) {
			throw new InvalidArgumentException(
					ExceptionMessages.VALIDATOR_NEGATIVE_VALUE,
					"" + a);
		}
	}

	public static void validatePositive(Number a) throws ValidatorException {
		if (a.doubleValue() <= 0) {
			throw new InvalidArgumentException(
					ExceptionMessages.VALIDATOR_NON_POSITIVE_VALUE,
					"" + a);
		}
	}
	
	public static void validateStringLength(String str, int len) throws ValidatorException {
		if (str.length() > len) {
			throw new InvalidArgumentException(
					ExceptionMessages.CLI_STRING_TOO_LONG,
					str);
		}
	}
	
	public static void validateIsFile(String path) throws ValidatorException {
		File file = new File(path);
		if(!file.isFile()) throw new FileAccessException(
				ExceptionMessages.VALIDATOR_IS_FILE, 
				path);
	}
	
	public static void validateAccess(String path) throws ValidatorException {
		File file = new File(path);
		if(!file.exists()) throw new FileAccessException(
				ExceptionMessages.VALIDATOR_FILE_NOT_EXISTS, 
				path);
		if(!file.canRead()) throw new FileAccessException(
				ExceptionMessages.VALIDATOR_FILE_ACCESS, 
				path);
	}
	
	public static void validateNotEmpty(String string) throws ValidatorException {
		if(string == null || string.isEmpty()) throw new InvalidArgumentException(ExceptionMessages.VALIDATOR_STRING_NULL_OR_EMPTY);
	}
	
}
