/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.commons.utils.progressbar;

public interface IProgressStatus {
	float get();
}
