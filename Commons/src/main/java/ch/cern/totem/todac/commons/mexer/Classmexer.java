/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.mexer;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.javamex.classmexer.MemoryUtil;

public final class Classmexer {
	
	private static Logger logger = Logger.getLogger(Classmexer.class);
	private static final String[] units = {"B", "KB", "MB", "GB"};
	
	private static void measure(Object object, String name, long divisionVactor, String unit){
		if(logger.getParent() == null || logger.getParent().getLevel() == null) return;
		if(logger.getParent().getLevel().toInt() > Level.DEBUG_INT) return;
		if(object == null){
			logger.error("Object is null: " + name);
			return;
		}
		
		
		try{
			long bytes = MemoryUtil.deepMemoryUsageOf(object);
			logger.debug("Object `" + name + "` weighs " + bytes/divisionVactor + " " + unit);
		}
		catch(Exception e){
			logger.info("Classmexer not initialized");
		}
	}
	
	public static void measureInGigaBytes(Object object, String name){
		measure(object, name, 1024*1024*1024, "GB");
	}
	
	public static void measureInMegaBytes(Object object, String name){
		measure(object, name, 1024*1024, "MB");
	}
	
	public static void measureInKiloBytes(Object object, String name){
		measure(object, name, 1024, "KB");
	}
	
	public static void measureInBytes(Object object, String name){
		measure(object, name, 1, "B");
	}
	
	public static void measure(Object object, String name){
		if(logger.getParent() == null || logger.getParent().getLevel() == null) return;
		if(logger.getParent().getLevel().toInt() > Level.DEBUG_INT) return;
		if(object == null){
			logger.error("Object is null: " + name);
			return;
		}
		
		try{
			long bytes = MemoryUtil.deepMemoryUsageOf(object);
			int divisions = 0;
			while(bytes > 1024 && divisions < units.length - 1){
				bytes /= 1024;
				divisions++;
			}
			logger.debug("`" + name + "` weighs " + bytes + " " + units[divisions]);
		}
		catch(Exception e){
			logger.info("Classmexer not initialized");
		}
	}
}
