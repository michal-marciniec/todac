/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class InvalidArgumentException extends ValidatorException {

	private static final long serialVersionUID = -3394878856078376500L;

	public InvalidArgumentException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}
	
	public InvalidArgumentException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}
	
	public InvalidArgumentException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}
	
	public InvalidArgumentException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}
}
