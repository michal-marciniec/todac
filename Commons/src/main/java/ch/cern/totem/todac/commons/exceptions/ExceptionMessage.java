/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.commons.exceptions;

public class ExceptionMessage {

	private final static String SUFFIX = ".";
	
	public String message;
	public String solution;
	
	public ExceptionMessage(String message, String solution) {
		this.message = message + SUFFIX;
		this.solution = solution + SUFFIX;
	}
	
}
