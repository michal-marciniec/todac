/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.magnetstrength;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.optics.common.CSVParser;
import ch.cern.totem.tudas.clients.java.data.manager.OpticsManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class MagnetStrengthPopulator {
	private static Logger logger = Logger.getLogger(MagnetStrengthPopulator.class);

	protected final int CSV_MAGNET_INDEX = 0;
	protected final int CSV_STRENGTH_INDEX = 1;
	protected OpticsManager opticsManager;

	public MagnetStrengthPopulator(OpticsManager opticsManager) {
		this.opticsManager = opticsManager;
	}

	public int populate(Map<String, Double> magnetStrengthData, String magnetStrengthCSVPath, long startTimestamp, long endTimestamp) throws TodacException {
		try {
			logger.debug("Insertion of " + magnetStrengthCSVPath);
			int magnetStrengthVersion = opticsManager.saveMagnetStrength(startTimestamp, endTimestamp, magnetStrengthData);
			logger.debug("OK");

			return magnetStrengthVersion;
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, magnetStrengthCSVPath, e);
		}
	}

	public Map<String, Double> parseMagnetStrengthCSV(String magnetStrengthCSVPath) throws TodacException {
		logger.debug("Parsing magnet strength data source " + magnetStrengthCSVPath);

		Map<String, Double> magnetStrengthData = new HashMap<String, Double>();
		List<String[]> parsedCSV = CSVParser.parse(magnetStrengthCSVPath);

		for (String[] row : parsedCSV) {
			try {
				String magnetName = row[CSV_MAGNET_INDEX];
				double strengthValue = Double.valueOf(row[CSV_STRENGTH_INDEX]);

				magnetStrengthData.put(magnetName, strengthValue);

			} catch (IndexOutOfBoundsException e) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + magnetStrengthCSVPath + " - improper format\n" + ArrayUtils.toString(row));
			} catch (NumberFormatException e) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + magnetStrengthCSVPath + " - improper number format\n" + ArrayUtils.toString(row));
			}
		}

		return magnetStrengthData;
	}
}
