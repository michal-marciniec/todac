/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.common;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;

public class CSVParser {
	private final static Logger logger = Logger.getLogger(CSVParser.class);

	public static List<String[]> parse(String csvInputFile) throws TodacException {
		List<String[]> parsedCSV = new LinkedList<String[]>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(csvInputFile));

			String line = null;
			while ((line = br.readLine()) != null) {
				String[] splitted = line.split(",");
				parsedCSV.add(splitted);
			}
		} catch (IOException e) {
			throw new TodacException(ExceptionMessages.FILE_ACCESS, csvInputFile, e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					logger.warn("Unable to close BufferedReader");
				}
		}

		return parsedCSV;
	}
}
