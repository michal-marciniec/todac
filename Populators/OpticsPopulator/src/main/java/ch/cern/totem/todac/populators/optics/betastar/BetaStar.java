/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.betastar;

public class BetaStar {
	protected String sourceFilepath;
	protected long startTimestamp;
	protected long endTimestamp;
	protected double betastarinmeter;

	public BetaStar(String sourceFilepath) {
		this.sourceFilepath = sourceFilepath;
	}

	public long getStartTimestamp() {
		return startTimestamp;
	}

	public void setStartTimestamp(long startTimestamp) {
		this.startTimestamp = startTimestamp;
	}

	public long getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(long endTimestamp) {
		this.endTimestamp = endTimestamp;
	}

	public double getBetatarinmeter() {
		return betastarinmeter;
	}

	public void setBetatarinmeter(double betatarinmeter) {
		this.betastarinmeter = betatarinmeter;
	}

	public String getSourceFilepath() {
		return sourceFilepath;
	}

	@Override
	public String toString() {
		return "BetaStar [" + startTimestamp + "," + endTimestamp + "," + betastarinmeter + "] (" + sourceFilepath +")";
	}
}
