/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.common;

public enum OpticsTypes {
	BETA_STAR, CURRENTS, MAGNET_STRENGTH, ALL
}
