/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.current;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.optics.common.CSVParser;
import ch.cern.totem.tudas.clients.java.data.manager.OpticsManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class CurrentsPopulator {
	private static Logger logger = Logger.getLogger(CurrentsPopulator.class);

	protected final int CSV_CONVNAME_INDEX = 0;
	protected final int CSV_CURRENT_INDEX = 1;
	protected OpticsManager opticsManager;

	public CurrentsPopulator(OpticsManager opticsManager) {
		this.opticsManager = opticsManager;
	}

	public int populate(Map<String, Double> currentsData, String currentsCSVPath, long startTimestamp, long endTimestamp) throws TodacException {
		try {
			logger.debug("Insertion of data from " + currentsCSVPath);
			int currentsVersion = opticsManager.saveCurrents(startTimestamp, endTimestamp, currentsData);
			logger.debug("OK");

			return currentsVersion;
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, currentsCSVPath, e);
		}
	}

	public Map<String, Double> parseCurrentsCSV(String currentsCSVPath) throws TodacException {
		logger.debug("Parsing currents data source " + currentsCSVPath);
		
		Map<String, Double> currentsData = new HashMap<String, Double>();
		List<String[]> parsedCSV = CSVParser.parse(currentsCSVPath);
		
		for (String[] row : parsedCSV) {
			try {
				String owerConverterName = row[CSV_CONVNAME_INDEX];
				double currentInAmper = Double.valueOf(row[CSV_CURRENT_INDEX]);

				currentsData.put(owerConverterName, currentInAmper);

			} catch (IndexOutOfBoundsException e1) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + currentsCSVPath + " - improper format\n" + ArrayUtils.toString(row));
			} catch (NumberFormatException e2) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + currentsCSVPath + " - improper format\n" + ArrayUtils.toString(row));
			}
		}

		return currentsData;
	}
}
