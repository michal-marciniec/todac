/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.betastar;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.optics.common.CSVParser;
import ch.cern.totem.tudas.clients.java.data.manager.OpticsManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class BetaStarPopulator {
	private static Logger logger = Logger.getLogger(BetaStarPopulator.class);

	protected final int CSV_STARTTIME_INDEX = 0;
	protected final int CSV_ENDTIME_INDEX = 1;
	protected final int CSV_BETASTAR_INDEX = 2;

	protected OpticsManager opticsManager;

	public BetaStarPopulator(OpticsManager opticsManager) {
		this.opticsManager = opticsManager;
	}

	public int populate(BetaStar betaStar) throws TodacException {
		try {
			logger.debug("Insertion of " + betaStar.toString());
			int betaStarVersion = opticsManager.saveBetaStar(betaStar.getStartTimestamp(), betaStar.getEndTimestamp(), betaStar.getBetatarinmeter());
			logger.debug("OK");
			return betaStarVersion;
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, betaStar.toString(), e);
		}
	}

	public List<BetaStar> parseBetaStarCSV(String betaStarCSVPath) throws TodacException {
		logger.debug("Parsing beta star data source " + betaStarCSVPath);

		List<BetaStar> betaStarData = new LinkedList<BetaStar>();
		List<String[]> parsedCSV = CSVParser.parse(betaStarCSVPath);

		for (String[] row : parsedCSV) {
			try {
				BetaStar betaStar = new BetaStar(betaStarCSVPath);

				long startTimestamp = Long.valueOf(row[CSV_STARTTIME_INDEX]);
				betaStar.setStartTimestamp(startTimestamp);

				long endTimestamp = Long.valueOf(row[CSV_ENDTIME_INDEX]);
				betaStar.setEndTimestamp(endTimestamp);

				double betastarinmeter = Double.valueOf(row[CSV_BETASTAR_INDEX]);
				betaStar.setBetatarinmeter(betastarinmeter);

				logger.trace(betaStar.toString());
				betaStarData.add(betaStar);
			} catch (IndexOutOfBoundsException e) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + betaStarCSVPath + " - improper format\n" + ArrayUtils.toString(row));
			} catch (NumberFormatException e) {
				logger.warn("Ignoring line " + parsedCSV.indexOf(row) + " in " + betaStarCSVPath + " - improper number format\n" + ArrayUtils.toString(row));
			}
		}

		return betaStarData;
	}
}
