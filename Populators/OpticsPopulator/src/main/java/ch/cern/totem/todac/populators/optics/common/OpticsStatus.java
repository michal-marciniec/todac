/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics.common;

import ch.cern.totem.todac.commons.utils.progressbar.IProgressStatus;

public enum OpticsStatus implements IProgressStatus {
	START(0.0f), // 0%
	FETCHING_TIMESTAMPS(0.05f), // 5%
	PARSING_CSV(0.1f), // 10%
	UPLOADING(0.6f), // 60%
	DONE(1.0f), // 100%
	
	BETASTAR(0.05f),
	MAGNET_STRENGTH(0.3f),
	CURRENTS(0.7f);

	private final float progress;

	OpticsStatus(float percentage) {
		this.progress = percentage;
	}

	@Override
	public float get() {
		return progress;
	}
}
