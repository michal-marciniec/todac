/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.optics;

import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.TimestampFetcher;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.todac.populators.optics.betastar.BetaStar;
import ch.cern.totem.todac.populators.optics.betastar.BetaStarPopulator;
import ch.cern.totem.todac.populators.optics.common.OpticsStatus;
import ch.cern.totem.todac.populators.optics.current.CurrentsPopulator;
import ch.cern.totem.todac.populators.optics.magnetstrength.MagnetStrengthPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.OpticsManager;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.Map;

public class OpticsPopulator implements MeasurableProgress {
	private static Logger logger = Logger.getLogger(OpticsPopulator.class);

	public static final String NAME_BETASTAR = "beta.csv";
	public static final String NAME_STRENGTH = "strength.csv";
	public static final String NAME_CURRENTS = "currents.csv";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected OpticsManager opticsManager;
	protected RunInformationManager runInfoManager;
	protected TimestampFetcher timestampFetcher;

	protected Progress progress;

	public OpticsPopulator(DatabaseAccessProvider databaseAccessProvider) {
		this.databaseAccessProvider = databaseAccessProvider;
	}

	public void initialize() throws DatabaseException {
		logger.debug("Initialization");
		try {
			progress = new Progress();
			opticsManager = databaseAccessProvider.getOpticsManager();
			runInfoManager = databaseAccessProvider.getRunInformationManager();
			timestampFetcher = new TimestampFetcher(runInfoManager);
		} catch (TudasException e) {
			throw new DatabaseException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
	}

	public void parseAndPopulateOptics(String opticsDirectory, int startRun, int endRun) throws TodacException {
		progress.reset();
		logger.info("Populating database with set of optics data from: " + opticsDirectory);

		progress.set(OpticsStatus.BETASTAR);
		parseAndPopulateBetaStar(opticsDirectory + File.separatorChar + NAME_BETASTAR, new Progress());
		progress.set(OpticsStatus.MAGNET_STRENGTH);
		parseAndPopulateMagnetStrength(opticsDirectory + File.separatorChar + NAME_STRENGTH, startRun, endRun, new Progress());
		progress.set(OpticsStatus.CURRENTS);
		parseAndPopulateCurrents(opticsDirectory + File.separatorChar + NAME_CURRENTS, startRun, endRun, new Progress());

		logger.debug("[success]\t" + "Populating database with optics data from: " + opticsDirectory);
	}

	public int parseAndPopulateBetaStar(String betaStarCSV) throws TodacException {
		return parseAndPopulateBetaStar(betaStarCSV, progress);
	}

	protected int parseAndPopulateBetaStar(String betaStarCSV, Progress progress) throws TodacException {
		progress.reset();
		logger.info("Populating database with beta star data from file: " + betaStarCSV);

		BetaStarPopulator bsPopulator = new BetaStarPopulator(opticsManager);

		progress.set(OpticsStatus.PARSING_CSV);
		List<BetaStar> betaStarData = bsPopulator.parseBetaStarCSV(betaStarCSV);

		if (betaStarData.isEmpty()) {
			logger.warn("No beta star data in " + betaStarCSV + " to be inserted");
			progress = new Progress(OpticsStatus.DONE);
			return -1;
		}

		progress.set(OpticsStatus.UPLOADING);
        int version = 0;
		for (BetaStar bs : betaStarData) {
			version = bsPopulator.populate(bs);
		}
		progress.set(OpticsStatus.DONE);
		logger.debug("[success]\t" + "Populating database with beta star data from file: " + betaStarCSV);
        return version;
	}

	public int parseAndPopulateCurrent(String currentsCSV, int startRun, int endRun) throws TodacException {
		return parseAndPopulateCurrents(currentsCSV, startRun, endRun, progress);
	}

	protected int parseAndPopulateCurrents(String currentsCSV, int startRun, int endRun, Progress progress) throws TodacException {
		progress.reset();
		logger.info("Populating database with currents data from file: " + currentsCSV);

		progress.set(OpticsStatus.FETCHING_TIMESTAMPS);
		long startTimestamp = timestampFetcher.getRunStartTimestamp(startRun);
		long endTimestamp = timestampFetcher.getRunEndTimestamp(endRun);

		CurrentsPopulator currentsPopulator = new CurrentsPopulator(opticsManager);
		progress.set(OpticsStatus.PARSING_CSV);
		Map<String, Double> currentsData = currentsPopulator.parseCurrentsCSV(currentsCSV);
		progress.set(OpticsStatus.UPLOADING);
		int version = currentsPopulator.populate(currentsData, currentsCSV, startTimestamp, endTimestamp);
		progress.set(OpticsStatus.DONE);
		logger.debug("[success]\t" + "Populating database with currents data from file: " + currentsCSV);
        return version;
	}

	public int parseAndPopulateMagnetStrength(String magnetStrengthCSV, int startRun, int endRun) throws TodacException {
		return parseAndPopulateMagnetStrength(magnetStrengthCSV, startRun, endRun, progress);
	}

	protected int parseAndPopulateMagnetStrength(String magnetStrengthCSV, int startRun, int endRun, Progress progress) throws TodacException {
		progress.reset();
		logger.info("Populating database with magnet strength data from file: " + magnetStrengthCSV);

		progress.set(OpticsStatus.FETCHING_TIMESTAMPS);
		long startTimestamp = timestampFetcher.getRunStartTimestamp(startRun);
		long endTimestamp = timestampFetcher.getRunEndTimestamp(endRun);

		MagnetStrengthPopulator msPopulator = new MagnetStrengthPopulator(opticsManager);
		progress.set(OpticsStatus.PARSING_CSV);
		Map<String, Double> magnetStrengthData = msPopulator.parseMagnetStrengthCSV(magnetStrengthCSV);
		progress.set(OpticsStatus.UPLOADING);
		int version = msPopulator.populate(magnetStrengthData, magnetStrengthCSV, startTimestamp, endTimestamp);
		progress.set(OpticsStatus.DONE);
		logger.debug("[success]\t" + "Populating database with magnet strength data from file: " + magnetStrengthCSV);
        return version;
	}

	@Override
	public Progress getProgress() {
		return progress;
	}

	public static void validateOpticsFound(String opticsDirectory) throws InvalidArgumentException {
		File betastar = new File(opticsDirectory + File.separatorChar + NAME_BETASTAR);
		File strength = new File(opticsDirectory + File.separatorChar + NAME_STRENGTH);
		File currents = new File(opticsDirectory + File.separatorChar + NAME_CURRENTS);

		if (betastar.exists() && strength.exists() && currents.exists())
			return;
		else
			throw new InvalidArgumentException(ExceptionMessages.OPTICS_NOT_FOUND);
	}
}
