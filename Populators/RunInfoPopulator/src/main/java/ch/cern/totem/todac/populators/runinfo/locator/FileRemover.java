package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.File;
import java.util.LinkedList;
import java.util.Queue;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.populators.runinfo.commons.Configuration;

/**
 * Instances of this class work as a thread and allow scheduling local files for removal.
 * @author Kamil Mielnik
 */
public class FileRemover extends Thread {
	
	private final static Logger logger = Logger.getLogger(FileRemover.class);
	
	protected Queue<String> scheduled;
	protected Boolean run;
	
	public FileRemover(){
		this.scheduled = new LinkedList<String>();
		this.run = true;
	}
	
	/**
	 * Schedules file for deletion
	 * @param filepath - file to be deleted
	 */
	public void schedule(String filepath){
		synchronized(scheduled){
			logger.debug(filepath + " scheduled for deletion");
			scheduled.add(filepath);
		}
	}
	
	/**
	 * Schedules this thread for cancelling at the nearest opportunity. This method returns immediately.
	 */
	public void kill(){
		run = false;
	}
	
	@Override
	public void run(){
		while(run || scheduled.size() != 0){
			try {
				Thread.sleep(Configuration.DEFAULT_REMOVER_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error("Thread.sleep has been interrupted", e);
			}
			
			String filepath = null;
			synchronized(scheduled){
				filepath = scheduled.poll();
			}
			if(filepath == null){
				continue;
			}
			
			delete(filepath);
		}
	}
	
	/**
	 * Delete specified file from local file system.
	 * @param filepath
	 * @return Return `true` on success and `false` on failure.
	 */
	protected boolean delete(String filepath){
		File file = new File(filepath);
		logger.debug("Deleting file: " + filepath);
		
		boolean deleted = file.delete();
		if(!deleted){
			logger.warn("Could not delete: " + filepath);
			synchronized(scheduled){
				scheduled.add(filepath);
			}
		}
		else{
			logger.debug("Deleted file: " + filepath);
		}
		
		return deleted;
	}
	
}
