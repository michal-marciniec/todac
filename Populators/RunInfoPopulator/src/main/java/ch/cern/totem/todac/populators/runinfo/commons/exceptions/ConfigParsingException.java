/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.runinfo.commons.exceptions;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessage;
import ch.cern.totem.todac.commons.exceptions.TodacException;

public class ConfigParsingException extends TodacException {

	private static final long serialVersionUID = 3147333049813701003L;

	public ConfigParsingException(ExceptionMessage exceptionMessage) {
		super(exceptionMessage);
	}	

	public ConfigParsingException(ExceptionMessage exceptionMessage, Exception e) {
		super(exceptionMessage, e);
	}

	public ConfigParsingException(ExceptionMessage exceptionMessage, String additionalInformation, Exception e) {
		super(exceptionMessage, additionalInformation, e);
	}

	public ConfigParsingException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}
}
