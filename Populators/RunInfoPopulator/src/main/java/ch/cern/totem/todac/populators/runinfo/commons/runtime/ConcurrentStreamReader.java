package ch.cern.totem.todac.populators.runinfo.commons.runtime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * Provides means to concurrently reading input streams until they're closed. 
 * Used mainly while reading process execution input.
 * @author Kamil Mielnik
 */
public class ConcurrentStreamReader extends Thread {

	private final static Logger logger = Logger.getLogger(ConcurrentStreamReader.class);
	
	protected InputStream inputStream;
	protected List<String> result;
	protected boolean loggingEnabled;
	protected String command;
	
	public ConcurrentStreamReader(InputStream inputStream){
		this.inputStream = inputStream;
		this.result = new ArrayList<String>(); 
		this.loggingEnabled = false;
	}
	
	public ConcurrentStreamReader(InputStream inputStream, String command){
		this(inputStream); 
		this.loggingEnabled = true;
		this.command = command;
	}
	
	public synchronized void run(){
		logger.trace("Concurrent stream reading started");
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		try {
			line = reader.readLine();
			while(line != null){
				result.add(line);
				if(loggingEnabled){
					logger.info(command + ":\n\t" + line);
				}
				Thread.yield();
				line = reader.readLine();
			}
		} catch (IOException e) {
			logger.error("Unable to read stream concurrently", e);
		}
		logger.trace("Finished reading stream concurrently");
	}
	
	public synchronized List<String> getResult(){
		return result;
	}
	
}
