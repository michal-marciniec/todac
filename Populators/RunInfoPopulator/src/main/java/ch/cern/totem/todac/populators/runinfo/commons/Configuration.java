package ch.cern.totem.todac.populators.runinfo.commons;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.exceptions.ConfigParsingException;

/**
 * Facade for java Properties class. Reads properties from configuration file. Also contains constants coupled with configuration file.
 * 
 * @author Michal Marciniec
 * @author Kamil Mielnik
 */
public class Configuration implements IInitializable {
	private static Logger logger = Logger.getLogger(Configuration.class);

	protected static final String PROPERTIES_FILEPATH = "/configuration/config.properties";

	// default run description
	public static final String DEFAULT_RUN_DESCRIPTION = "Populated with TODAC";

	// regular expressions
	public static final String REGEXP_FILEPATH = ".*run_[0-9]+\\.([0-9]{3,4}\\.)?vmea$";
	public static final String REGEXP_FILENAME = "run_[0-9]+\\.([0-9]{3,4}\\.)?vmea$";
	public static final String REGEXP_FILENAME_SINGLE_RUN = "^run_[0-9]+\\.vmea$";
	public static final String REGEXP_FILENUMBER = "\\.[0-9]{3,4}\\.";
	public static final String REGEXP_DOT = "\\.";
	public static final String REGEXP_PARAMETER = "\\[\\[[^\\]]+\\]\\]";
	public static final String REGEXP_PARAMETER_LEFT = "[[";
	public static final String REGEXP_PARAMETER_RIGHT = "]]";
	public static final String REGEXP_INLINE_CMD = "(('[^']+')|[\\S]+)";

	// locator, vmea && runtime constants
	public static final int DEFAULT_STAGER_SLEEP_TIME = 60000; // in miliseconds
	public static final int DEFAULT_DOWNLOADER_SLEEP_TIME = 2000; // in miliseconds
	public static final int DEFAULT_REMOVER_SLEEP_TIME = 1000;
	public static final int DEFAULT_POPULATOR_ATTEMPTS_COUNT = 3;
	public static final int DEFAULT_POPULATOR_ATTEMPT_SLEEP_TIME = 5000;

	public static final String CASTOR_ACCESS_PARAM = "castor.access";
	public static final String CASTOR_ACCESS_DOWNLOAD_PARAM = "download";
	public static final String SAVE_METHOD = "save.method";
	public static final String SAVE_METHOD_CHUNKS = "chunks";
	public static final String SAVE_ATTEMPS = "save.attempts";
	public static final String SAVE_ATTEMPT_DELAY = "save.attempt.delay";
	public static final String CHECK_TIME_PARAM = "download.check.time";
	public static final String DOWNLOAD_DIRECTORY_PARAM = "download.directory";
	public static final String REMOVE_DATA_PARAM = "download.remove.data";
	public static final String CASTOR_THREADS_PARAM = "castor.threads";
	public static final String PATH_LOCAL_PARAM = "path.local";
	public static final String PATH_CASTOR_PARAM = "path.castor";

	public static final String COMMAND_QUERY = "stager_qry";
	public static final String COMMAND_QUERY_ARG = "path";
	public static final String COMMAND_QUERY_ENV = "command.stager_qry.env";
	public static final String COMMAND_GET = "stager_get";
	public static final String COMMAND_GET_ARG = "path";
	public static final String COMMAND_GET_ENV = "command.stager_get.env";
	public static final String COMMAND_FIND = "nsfind";
	public static final String COMMAND_FIND_ARG = "number";
	public static final String COMMAND_DOWNLOAD = "download";
	public static final String COMMAND_SHELL = "shell";
	public static final String COMMAND_WHICH = "which";
	public static final String COMMAND_WHICH_ARG = "command";
	

	public static final String STATUS_STAGED = "status.staged";
	public static final String STATUS_STAGEIN = "status.stagein";
	public static final String STATUS_RECALL = "status.recall";

	public static final String COMMAND_PREFIX = "command.";
	public static final String ARGUMENT_SUFFIX = ".args";

	// extractor constants
	public static final String CMSSW_ENVIRONMENT = "cmsrun.environment";
	public static final String CMSSW_VMEA_PLACEHOLDER_CONFIG = "cmsrun.script.placeholder.file";
	public static final String CMSSW_PLUGIN_PLACEHOLDER_CONFIG = "cmsrun.script.placeholder.plugin";
	public static final String CMSSW_SCRIPT_TEMPLATE_CONFIG = "cmsrun.script.path";
	public static final String CMSSW_OUTPUT_TYPE = "cmsrun.output.type";
	public static final String CMSSW_OUTPUT_PATH = "cmsrun.output.filepath";
	public static final String CMSSW_ANALYZER_PLUGIN = "cmsrun.plugin";
	public static final String CMSSW_OUTPUT_STREAM_START = "cmsrun.output.stream.start";
	public static final String CMSSW_OUTPUT_STREAM_END = "cmsrun.output.stream.end";
	public static final String CMSSW_SCRIPT_TEMP_DIR = "cmssw.temp.script.dir";
	public static final String CMSSW_SCRIPT_TEMP_REMOVE = "cmssw.temp.script.remove";

	// logger
	public static final String LOGGING_FILE_DIR = "logger.file.directory";
	public static final String LOGGING_LEVEL_CONSOLE = "logger.level.console";
	public static final String LOGGING_LEVEL_FILE = "logger.level.file";

	// CASTOR filepaths caching
	public static final String CASTOR_CACHE_DIR = "cache.dir";
	public static final String CASTOR_CACHE_LOAD = "cache.load.on";
	public static final String CASTOR_CACHE_SAVE = "cache.save.on";
	public static final String CASTOR_CACHE_SAVE_OVERWRITE = "cache.save.overwrite";

	// file status progress
	public static final float PROGRESS_RECALL = 0.0f;
	public static final float PROGRESS_STAGEIN = 0.05f;
	public static final float PROGRESS_STAGED = 0.35f;
	public static final float PROGRESS_DOWNLOADING = 0.55f;
	public static final float PROGRESS_PROCESSING = 0.85f;
	public static final float PROGRESS_DONE = 1.0f;

	private Properties properties;

	public void initialize() throws TodacException {
		logger.debug("Initialize - loading properties from config file.");

		properties = new Properties();
		try {
			InputStream propertiesStream = Configuration.class.getResourceAsStream(PROPERTIES_FILEPATH);			
			properties.load(propertiesStream);
		} catch (IOException e) {
			throw new TodacException(ExceptionMessages.CONFIGURATION_FILE, e);
		}

		logger.info("Properties loaded from config file.");
	}
	
	public static InputStream getResourceInputStream(String projectRelativePath) {
		InputStream resourceInputStream = Configuration.class.getResourceAsStream("/" + projectRelativePath);
		return resourceInputStream;
	}

	public boolean containsKey(Object arg0) {
		return properties.containsKey(arg0);
	}

	/**
	 * Gets property value from configuration file. Trims result before returning.
	 * 
	 * @param key
	 *            - property name
	 * @return String property value if key was specified in configuration file, null otherwise.
	 */
	public String getProperty(String key) {
		String property = properties.getProperty(key);
		if (property != null) {
			property = property.trim();
		} else
			logger.trace("Property " + key + " does not exist. Returning null value.");

		return property;
	}

	/**
	 * Gets property value from configuration file. Trims result before returning.
	 * 
	 * @param key
	 *            - property name
	 * @param checkExistance
	 *            - if true then then not-null value is expected to be returned, otherwise null can be returned
	 * @return null only in case the key wasn't present in configuration file AND checkExistance was set false. Otherwise returns property value for the specified key.
	 * @throws ConfigParsingException
	 *             if checkExistance flag was set true and key wasn't find in configuration file.
	 */
	public String getProperty(String key, boolean checkExistance) throws ConfigParsingException {
		String property = properties.getProperty(key);
		if (property == null) {
			if (checkExistance)
				throw new ConfigParsingException(ExceptionMessages.CONFIGURATION_NO_PROPERTY, key);
			else
				return property;
		} else
			property = property.trim();

		return property;
	}
}
