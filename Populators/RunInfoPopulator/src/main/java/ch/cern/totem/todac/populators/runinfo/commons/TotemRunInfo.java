package ch.cern.totem.todac.populators.runinfo.commons;

import java.io.Serializable;

import ch.cern.totem.tudas.tudasUtil.structure.Event;

/**
 * Represents a single Totem Run consisting of Events.
 * 
 * @author Przemyslaw Wyszkowski
 */
public class TotemRunInfo implements Serializable {

	private static final long serialVersionUID = -5457825173614511113L;
	
	private int runNo;
	private Event firstEvent;
	private Event lastEvent;
	private Event[] events;
	private String description;

	public TotemRunInfo() { }

	public TotemRunInfo(int runNo, Event firstEvent, Event lastEvent,
			Event[] events, String description) {
		this.runNo = runNo;
		this.firstEvent = firstEvent;
		this.lastEvent = lastEvent;
		this.events = events;
		this.description = description;
	}

	public int getRunNo() {
		return runNo;
	}

	public void setRunNo(int runNo) {
		this.runNo = runNo;
	}

	public Event getFirstEvent() {
		return firstEvent;
	}

	public void setFirstEvent(Event firstEvent) {
		this.firstEvent = firstEvent;
	}

	public Event getLastEvent() {
		return lastEvent;
	}

	public void setLastEvent(Event lastEvent) {
		this.lastEvent = lastEvent;
	}

	public Event[] getEvents() {
		return events;
	}

	public void setEvents(Event[] events) {
		this.events = events;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
