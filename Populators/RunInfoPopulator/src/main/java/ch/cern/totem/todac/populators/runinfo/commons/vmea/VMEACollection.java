package ch.cern.totem.todac.populators.runinfo.commons.vmea;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.utils.progressbar.Progress;

/**
 * Class for storing a synchronized collection of VMEAFile objects. Also
 * provides means for monitoring current progress of extraction. VMEAFile
 * objects are differentiated by their part number.
 * 
 * @author Kamil Mielnik
 */
public class VMEACollection implements Iterable<VMEAFile> {
	
	private final static Logger logger = Logger.getLogger(VMEACollection.class);

	protected List<VMEAFile> list;

	public VMEACollection() {
		list = new LinkedList<VMEAFile>();
	}

	public synchronized void add(VMEAFile vmeaFile) {
		for (VMEAFile file : list) {
			if (file.getPartNumber().equals(vmeaFile.getPartNumber())) {
				logger.debug("Duplicate file for part no." + file.getPartNumber() + ": " + vmeaFile.getCastorFilepath());
				return;
			}
		}
		logger.debug(vmeaFile.getCastorFilepath() + " added to collection.");
		list.add(vmeaFile);
	}

	public synchronized VMEAFile get(Integer partNumber) {
		for (VMEAFile file : list) {
			if (file.getPartNumber().equals(partNumber)) {
				return file;
			}
		}
		logger.debug("No file found with part number: " + partNumber);
		return null;
	}

	public synchronized boolean contains(VMEAFile vmeaFile) {
		return list.contains(vmeaFile);
	}

	public synchronized void remove(VMEAFile vmeaFile) {
		for (VMEAFile file : list) {
			if (file.getPartNumber().equals(vmeaFile.getPartNumber())) {
				logger.debug("Removing " + vmeaFile.getPartNumber() + " part from " + VMEACollection.class.getName());
				list.remove(file);
				return;
			}
		}
	}

	public synchronized int size() {
		return list.size();
	}

	/**
	 * @return Returns percentage value of current cumulative extraction
	 *         progress - a value between 0.0f and 1.0f
	 */
	public synchronized Progress getProgress() {
		if (list.size() == 0) {
			return new Progress(0.0f);
		}
		
		float progress = 0.0f;
		for (VMEAFile file : list) { 
			progress += file.getFileStatus().get();
		}

		return new Progress(progress / list.size());
	}

	public synchronized Iterator<VMEAFile> iterator() {
		return list.iterator();
	}

}
