package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.AsyncCommandExecutor;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandBuilder;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.INotifier;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.FileStatus;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEACollection;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;
import ch.cern.totem.todac.populators.runinfo.extractor.EventsCollector;

/**
 * This class provides resources to download files from CASTOR in a concurrent way.
 * A LocalFileProvider should be runned in a thread in order to finish downloading all 
 * scheduled files. Thread finishes when all scheduled files has been downloaded. 
 * @author Kamil Mielnik
 */
public class LocalFileProvider implements IFileProvider {

	private final static Logger logger = Logger.getLogger(LocalFileProvider.class);
	
	protected Configuration configuration;
	protected VMEACollection requested;
	protected EventsCollector eventsCollector;
	protected FileRemover fileRemover;
	protected Integer currentThreads;
	protected Integer filesToDownload;
	protected TodacException returnedException;
	protected INotifier<VMEAFile> stager;
	
	protected boolean removeData;
	protected String downloadDirectory;
	protected int castorThreads;
	
	public LocalFileProvider(Configuration configuration, INotifier<VMEAFile> stager, EventsCollector eventsCollector, int filesToProvide){
		this.configuration = configuration;
		this.stager = stager;
		this.eventsCollector = eventsCollector;
		this.filesToDownload = filesToProvide;
		this.fileRemover = null;
		this.currentThreads = 0;
		this.returnedException = null;
		this.requested = new VMEACollection();
	}
	
	public void initialize() throws TodacException {
		logger.debug("Initializing " + LocalFileProvider.class.getName());
		
		removeData = Boolean.parseBoolean(configuration.getProperty(Configuration.REMOVE_DATA_PARAM, true));
		downloadDirectory = configuration.getProperty(Configuration.DOWNLOAD_DIRECTORY_PARAM, true);
		castorThreads = Integer.parseInt(configuration.getProperty(Configuration.CASTOR_THREADS_PARAM, true));
		if(removeData){
			fileRemover = new FileRemover();
			fileRemover.start();
		}
	}
	
	public void notify(VMEAFile vmeaFile) throws TodacException {
		logger.debug(vmeaFile.getCastorFilepath() + " downloaded");
		
		try{
			eventsCollector.extractEvents(vmeaFile);			
		}
		catch(TodacException e){
			throw e;
		}
		finally{
			synchronized(currentThreads){
				--filesToDownload;
				--currentThreads;
			}
			stager.notify(vmeaFile);
			if(removeData){
				fileRemover.schedule(vmeaFile.getLocalFilepath());
			}
		}
	}

	public void notify(VMEAFile vmeaFile, TodacException exception) {
		returnedException = exception;
		logger.error("Exception has been thrown asynchronously", exception);
	}
	
	/**
	 * Tries to schedule a file for downloading.
	 * @param vmeaFile
	 * @return Returns `true` only when a file has been queued for downloading. Returns `false` if all threads are busy - try to reinvoke this method later. 
	 * @throws IOException
	 */
	public boolean provide(VMEAFile vmeaFile) throws TodacException {
		if(currentThreads >= castorThreads){
			logger.trace("Download queue is full");
			return false;
		}
		else{
			if(!requested.contains(vmeaFile)){
				requested.add(vmeaFile);
			}
			else{
				return false;
			}
		}
	
		synchronized(currentThreads){
			++currentThreads;
		}
		
		vmeaFile.setFileStatus(FileStatus.DOWNLOADING);
		logger.debug("Downloading " + vmeaFile.getCastorFilepath());
		String command = getCommand(vmeaFile);
		AsyncCommandExecutor<VMEAFile> downloadCommand = 
				new AsyncCommandExecutor<VMEAFile>(configuration, command, this, vmeaFile);
		Thread download = new Thread(downloadCommand);
		download.start();
		
		if(returnedException != null){
			throw returnedException;
		}
		
		return true;
	}

	public void run() {
		logger.debug("Thread loop started");
		
		while(filesToDownload > 0){
			try {
				Thread.sleep(Configuration.DEFAULT_DOWNLOADER_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error("Thread.sleep has failed", e);
			}
			
			if(returnedException != null){
				break;
			}
		}
		
		if(removeData){
			fileRemover.schedule(downloadDirectory);
			fileRemover.kill();
			
			try {
				fileRemover.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		logger.debug("Thread loop finished");
	}
	
	/**
	 * Builds a string containing system command that allows downloading file from CASTOR.
	 * @param vmeaFile
	 * @return String containing desired system command.
	 * @throws IOException
	 */
	protected String getCommand(VMEAFile vmeaFile) throws TodacException {
		CommandBuilder builder = new CommandBuilder(configuration, Configuration.COMMAND_DOWNLOAD);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(Configuration.PATH_LOCAL_PARAM, vmeaFile.getLocalFilepath());
		parameters.put(Configuration.PATH_CASTOR_PARAM, vmeaFile.getCastorFilepath());
		return builder.build(parameters);
	}
}
