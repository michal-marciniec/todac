package ch.cern.totem.todac.populators.runinfo.uploader;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.TotemRunInfo;
import ch.cern.totem.todac.populators.runinfo.commons.exceptions.ExceptionCollection;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

/**
 * Class for managing connection with TUDAS server instance and populating 
 * database with desired run info structure either in chunks or in a single batch.
 * 
 * @author Kamil Mielnik
 */
public class Uploader implements IInitializable {

	private final static Logger logger = Logger.getLogger(Uploader.class);

	protected Configuration configuration;
	protected RunInformationManager runInformationManager;
	protected ExceptionCollection exceptionCollection;
	protected boolean saveInChunks;
	protected int numberOfChunks;
	protected int chunksProcessed;
	protected int maximumAttempts;
	protected int attemptDelay;
	
	public Uploader(Configuration configuration, RunInformationManager runInformationManager, int numberOfChunks){
		this.configuration = configuration;
		this.runInformationManager = runInformationManager;
		this.exceptionCollection = new ExceptionCollection(numberOfChunks);
		this.numberOfChunks = numberOfChunks;
		this.saveInChunks = false;
		this.chunksProcessed = 0;
		this.maximumAttempts = 0;
		this.attemptDelay = 0;
	}
	
	public void initialize() throws TodacException {
		logger.debug("Initializing " + Uploader.class.getName());
		saveInChunks = configuration.getProperty(Configuration.SAVE_METHOD, true).equals(Configuration.SAVE_METHOD_CHUNKS);
		
		maximumAttempts = Integer.parseInt(configuration.getProperty(Configuration.SAVE_ATTEMPS, true));
		if(maximumAttempts <= 0) maximumAttempts = Configuration.DEFAULT_POPULATOR_ATTEMPTS_COUNT;
		
		attemptDelay = Integer.parseInt(configuration.getProperty(Configuration.SAVE_ATTEMPT_DELAY, true));
		if(attemptDelay < 0) attemptDelay = Configuration.DEFAULT_POPULATOR_ATTEMPT_SLEEP_TIME;
	}

	/**
	 * Attempts to populate database with specified runInfo structure. Returns false when
	 * waiting for all chunks to be processed and true on successfull data insertion.
	 * 
	 * @param runInfo
	 * @return true on success and false on failure
	 * @throws TudasException
	 */
	public boolean populate(TotemRunInfo runInfo, int partNumber){		
		chunksProcessed++;
		if(saveInChunks){
			logger.debug("Chunk " + chunksProcessed + ": populating database with " + runInfo.getEvents().length + " events");
		}
		else return false;
		
		int attemptsCount = 0;
		boolean success = false;
		while(!success && attemptsCount < maximumAttempts){
			logger.debug((attemptsCount + 1) + " attempt for populating database with part number: " + partNumber);
			success = insert(runInfo, partNumber);
			++attemptsCount;
			try {
				Thread.sleep(attemptDelay);
			} catch (InterruptedException e) {
				logger.warn("Interrupted exception during sleeping inbetween save attempts.", e);
			}
		}
        
        return true;
	}
	
	public void populate(TotemRunInfo runInfo){
		int attemptsCount = 0;
		boolean success = false;
		while(!success && attemptsCount < maximumAttempts){
			logger.debug((attemptsCount + 1) + " attempt for populating database with complete run information.");
			success = insert(runInfo, -1);
			++attemptsCount;
			try {
				Thread.sleep(attemptDelay);
			} catch (InterruptedException e) {
				logger.warn("Interrupted exception during sleeping inbetween save attempts.", e);
			}
		}
	}
	
	/**
	 * Returns exceptions encountered during database population associated with each chunk.
	 * @return
	 */
	public ExceptionCollection getExceptions(){
		return exceptionCollection;
	}
	
	public boolean hasThrownAnException(){
		return exceptionCollection.getExceptions().size() > 0;
	}
	
	protected boolean insert(TotemRunInfo runInfo, int partNumber){
		try {
            runInformationManager.saveRunInformation(runInfo.getRunNo(), runInfo.getDescription(), runInfo.getEvents());
            logger.trace("OK");
            exceptionCollection.clearExceptions(partNumber);
        } catch (TudasException e) {
            exceptionCollection.addException(partNumber, 
            		new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e));
            if(partNumber == -1)
            	logger.error("Could not populate the database with complete run information");
            else
            	logger.error("Could not populate the database with part: " + partNumber);
            
            return false;
        }
		return true;
	}
	
}
