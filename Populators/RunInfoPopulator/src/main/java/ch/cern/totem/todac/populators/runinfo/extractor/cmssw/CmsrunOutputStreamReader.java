package ch.cern.totem.todac.populators.runinfo.extractor.cmssw;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.tudas.tudasUtil.structure.Event;
import org.apache.log4j.Logger;

import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

/**
 * EventDataDumper's standard output stream reader. EventDataDumper is a CMSSW reader plugin. Output format:
 * 
 * {defined data beginnning} {event_id},{timestamp} ... {event_id},{timestamp} {defined data ending}
 * 
 * @author Michal Marciniec
 */
public class CmsrunOutputStreamReader {
	private static Logger logger = Logger.getLogger(CmsrunOutputStreamReader.class);

	protected Configuration configuration;
	protected List<String> sourceStream;

	protected String startString;
	protected String endString;

	public CmsrunOutputStreamReader(Configuration configuration) {
		this.configuration = configuration;
	}

	public void initialize() {
		this.startString = configuration.getProperty(Configuration.CMSSW_OUTPUT_STREAM_START);
		this.endString = configuration.getProperty(Configuration.CMSSW_OUTPUT_STREAM_END);
	}

	public List<Event> parse(List<String> sourceStream, int fileNo, String vmeaFile) throws TodacException {
		List<Event> events = new LinkedList<Event>();
		if (sourceStream == null || sourceStream.size() == 0) {
			logger.info("Provided cmsRun output was empty! Returning empty list of events' data.");
			return events;
		}

		logger.info("cmsRun output stream parsing (" + vmeaFile + ")");

		int st = sourceStream.indexOf(startString);
		int end = sourceStream.indexOf(endString);

		try {
			for (int i = st + 1; i < end; ++i) {
				String line = sourceStream.get(i);
                //TODO: refactor
                if(line.startsWith("from CMSSW:")) {
                    String[] eventInfo = line.split(",");
                    int eventNo = Integer.parseInt(eventInfo[1].split("=")[1]);
                    Timestamp time = new Timestamp(Long.parseLong(eventInfo[2].split("=")[1]));
                    events.add(new Event(time.getTime(), fileNo, eventNo));
                }
			}
		} catch (Exception e) {
			throw new TodacException(ExceptionMessages.CMSSW_STREAM_OUTPUT_PARSING);
		}
		logger.info("cmsRun output stream parsing (" + vmeaFile + ") [finished]");
		return events;
	}
}
