package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandBuilder;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandExecutor;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAUtils;

/**
 * The CastorFinder class provides method to search for files directly on CASTOR (using appropriate commands) that are associated with particular run.
 * 
 * @author Kamil Mielnik, Michal Marciniec
 */
public class CastorFinder {

	private final static Logger logger = Logger.getLogger(CastorFinder.class);

	protected Configuration configuration;

	public CastorFinder(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Looks up for ALL files (including duplicates and backups) associated with desired run on CASTOR.
	 * 
	 * @param runNumber
	 * @return Returns a List of Strings which are filepaths pointing to CASTOR files for each part. Note that there may be more than one path pointing to the same vmea part, because CASTOR may
	 *         possess duplicate files.
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public List<String> find(int runNumber) throws TodacException {
		logger.debug("Searching CASTOR for files associated with run " + runNumber);

		String command = getCommand(runNumber);
		CommandExecutor processExecutor = new CommandExecutor(configuration, command);
		try {
			processExecutor.execute();
		} catch (TodacException e) {
			logger.error("Could not locate files on CASTOR. Execution will be terminated.", e);
			throw new TodacException(ExceptionMessages.CASTOR_FILE_ACCESS, e);
		}

		List<String> castorPaths = new LinkedList<String>();
		List<String> output = processExecutor.getInputStream();

		for (String path : output) {
			if (VMEAUtils.isFilepathValid(path)) {
				castorPaths.add(path);
				logger.debug("Found file: " + path);
			} else {
				logger.warn("Found path is invalid: " + path);
			}
		}

		return castorPaths;
	}

	/**
	 * Builds a string containing system command that allows finding files on CASTOR.
	 * 
	 * @param runNumber
	 * @return String containing desired system command.
	 * @throws IOException
	 */
	protected String getCommand(int runNumber) throws TodacException {
		CommandBuilder commandBuilder = new CommandBuilder(configuration, Configuration.COMMAND_FIND);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(Configuration.COMMAND_FIND_ARG, Integer.toString(runNumber));
		String cmd = commandBuilder.build(parameters);
		logger.trace("Command to be used for finding files on CASTOR: \n\t" + cmd);
		return cmd;
	}
}
