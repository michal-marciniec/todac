package ch.cern.totem.todac.populators.runinfo.extractor.enums;

public enum OutputType {
	FILE, OUTPUTSTREAM
}
