package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEACollection;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAUtils;

/**
 * File locator is responsible for providing VMEAFiles collection.
 * To accomplish that it uses CastorCache or CastorFinder (depends on configuration).
 * 
 * @author Kamil Mielnik
 */
public class FileLocator implements IInitializable {
	
	private final static Logger logger = Logger.getLogger(FileLocator.class);

	protected Configuration configuration;
	protected String downloadDirectory;

	protected CastorCache cache;

	protected String cacheDir;
	protected boolean loading;
	protected boolean caching;
	protected boolean overwritingCaching;

	/**
	 * Constructor that works equally to FileLocator(configuration, null)
	 */
	public FileLocator(Configuration configuration) {
		this.configuration = configuration;
		this.downloadDirectory = null;
	}

	/**
	 * Constructor
	 * @param configuration - application's configuration
	 * @param downloadDirectory - if not null then localPath fields of VMEAFiles will be filled (used when data extraction is to be performed on local files not directly on CASTOR files).
	 */
	public FileLocator(Configuration configuration, String downloadDirectory) {
		this.configuration = configuration;
		this.downloadDirectory = downloadDirectory;
	}

	public void initialize() {
		logger.debug("Initializing " + FileLocator.class.getName());

		loading = Boolean.valueOf(configuration.getProperty(Configuration.CASTOR_CACHE_LOAD));
		caching = Boolean.valueOf(configuration.getProperty(Configuration.CASTOR_CACHE_SAVE));
		overwritingCaching = Boolean.valueOf(configuration.getProperty(Configuration.CASTOR_CACHE_SAVE_OVERWRITE));

		if (loading || caching) {
			cacheDir = configuration.getProperty(Configuration.CASTOR_CACHE_DIR);

			cache = new CastorCache(configuration, cacheDir);
			cache.initialize();
		}
	}

	/**
	 * Main method locating files associated with particular TOTEM Run.
	 * Note: If cache usage fails there will be no exception raised apart from logging a warning and then CastorFinder will be used to locate files.
	 * @param runNumber - TOTEM Run number
	 * @return VMEACollection containing VMEAFiles describing relevant TOTEM Run
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public VMEACollection locate(int runNumber) throws TodacException {
		logger.debug("Locating VMEA files for run: " + runNumber);

		List<String> filepaths = null;
		if (loading) {
			filepaths = cache.load(runNumber);
		}
		if (filepaths == null) {
			CastorFinder finder = new CastorFinder(configuration);
			filepaths = finder.find(runNumber);

			if (caching) {
				cache.update(runNumber, filepaths, overwritingCaching);
			}
		}
		logger.debug(filepaths.size() + " files for run " + runNumber + " has been found");

		VMEACollection files = new VMEACollection();
		for (String path : filepaths) {
			if (VMEAUtils.isFilepathValid(path)) {
				String filename = VMEAUtils.getFilename(path);
				String localFilepath = VMEAUtils.getLocalFilepath(path, downloadDirectory);

				if (VMEAUtils.isSingleFileRun(filename)) {
					logger.debug("Run " + runNumber + " consists of single file");

					files.add(new VMEAFile(0, path, localFilepath));
				} else {
					int fileNumber = VMEAUtils.getFileNumber(filename);
					files.add(new VMEAFile(fileNumber, path, localFilepath));
				}
			} else {
				logger.warn("Invalid path:" + path);
			}
		}

		return files;
	}
}
