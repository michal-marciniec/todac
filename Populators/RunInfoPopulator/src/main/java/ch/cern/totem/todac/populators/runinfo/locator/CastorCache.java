package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;

/**
 * This class provides efficient CASTOR's filepaths caching to avoid using FileLocator when looking for files associated with TOTEM Run. Cache files are mentioned to be stored in the directory
 * specified in configuration file.
 * 
 * @author Michal Marciniec
 */
public class CastorCache implements IInitializable {
	private static final String DEFAULT_CACHE_DIRECTORY = "castor_cache";

	private static Logger logger = Logger.getLogger(CastorCache.class);

	protected String directory;

	public CastorCache(Configuration configuration, String cacheDir) {
		this.directory = cacheDir;
	}

	public void initialize() {
		try {
			if (new File(directory).isDirectory()) {
			} else {
				if (!new File(directory).mkdir())
					throw new Exception("Couln't create directory: " + directory);
			}
		} catch (Exception e) {
			logger.info("Couldn't use specified caching directory: " + directory + ". Default directory will be used: " + DEFAULT_CACHE_DIRECTORY, e);
			this.directory = DEFAULT_CACHE_DIRECTORY;
			new File(directory).mkdir();
		}
	}

	/**
	 * Loads CASTOR's filepaths for particular run from cache.
	 * 
	 * @param runNumber
	 *            -TOTEM Run number
	 * @return List of filepaths that were read from cache file associated with specified runNumber. Null if no cache file was found or exception was raised.
	 */
	public List<String> load(int runNumber) {
		List<String> filepaths = new LinkedList<String>();
		File runCache = new File(directory + File.separator + runNumber);
		BufferedReader br = null;

		logger.info("Loading cached filepaths for run " + runNumber + " from: " + runCache.getAbsolutePath());
		try {
			br = new BufferedReader(new FileReader(runCache));
			String line = null;
			while ((line = br.readLine()) != null) {
				filepaths.add(line);
			}
		} catch (NullPointerException e) {
			logger.info("No filepaths for run " + runNumber + " cached.");
			return null;
		} catch (Exception e) {
			logger.info("Unable to load CASTOR paths for run " + runNumber + " from cache file: " + runCache.getAbsolutePath());
			return null;
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					logger.trace("Exception during buffered reader closing.", e);
				}
		}

		return filepaths;
	}

	/**
	 * Saves CASTOR's filepaths for particular run into the cache file.
	 * 
	 * @param runNumber
	 *            - number of TOTEM Run
	 * @param filepaths
	 *            - list of filepaths to be cached
	 * @param overwrite
	 *            - if false then caching will be performed only if there wasn't existing cache file associated with this runNumber.
	 */
	public void update(int runNumber, List<String> filepaths, boolean overwrite) {
		File runCache = new File(directory + File.separator + runNumber);
		logger.debug("Caching filepaths for run " + runNumber + " in: " + runCache.getAbsolutePath());

		if (runCache.exists() && !overwrite) {
			logger.debug("Cache file for run " + runNumber + " already exists. Nothing will be done.");
			return;
		}

		try {
			PrintWriter writer = new PrintWriter(runCache);
			for (String path : filepaths) {
				writer.write(path + "\n");
			}
			writer.close();
		} catch (IOException e) {
			logger.debug("Exception during caching filepaths for run " + runNumber, e);
		}
	}
}
