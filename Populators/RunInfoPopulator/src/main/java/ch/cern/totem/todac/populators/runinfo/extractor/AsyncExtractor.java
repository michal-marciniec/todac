package ch.cern.totem.todac.populators.runinfo.extractor;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.INotifier;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;

/**
 * Runs events extracting (and collecting) in a separate thread.
 * 
 * @author Kamil Mielnik
 */
public class AsyncExtractor implements Runnable {
	
	protected INotifier<VMEAFile> notifier;
	protected EventsCollector eventsCollector;
	protected VMEAFile vmeaFile;
	
	public AsyncExtractor(INotifier<VMEAFile> notifier, EventsCollector eventsCollector, VMEAFile vmeaFile){
		this.notifier = notifier;
		this.eventsCollector = eventsCollector;
		this.vmeaFile = vmeaFile;
	}
	
	public void run() {
		try{
			eventsCollector.extractEvents(vmeaFile);
			notifier.notify(vmeaFile);
		}
		catch(TodacException e){
			notifier.notify(vmeaFile, e);
		}
	}
}
