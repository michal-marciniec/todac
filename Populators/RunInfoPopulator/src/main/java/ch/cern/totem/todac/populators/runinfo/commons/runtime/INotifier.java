package ch.cern.totem.todac.populators.runinfo.commons.runtime;

import ch.cern.totem.todac.commons.exceptions.TodacException;

/**
 * Provides interface for notification of finished method execution during asynchronous communication.
 * @author Kamil Mielnik
 *
 * @param <T> Type of value passed to the listener as a result of method invocation. 
 */
public interface INotifier<T> {
	
	void notify(T parameter) throws TodacException;
	void notify(T parameter, TodacException exception);
	
}
