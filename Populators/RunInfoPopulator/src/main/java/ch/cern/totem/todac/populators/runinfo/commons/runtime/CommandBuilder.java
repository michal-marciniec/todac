package ch.cern.totem.todac.populators.runinfo.commons.runtime;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.exceptions.ConfigParsingException;

/**
 * Allows building system commands based on the configuration file. Allows to 
 * read parameters specified in the said configuration file and also to pass
 * those parameters in an overloaded `build` method.  
 * 
 * @author Kamil Mielnik
 */
public class CommandBuilder {
	
	private final static Logger logger = Logger.getLogger(CommandBuilder.class);

	protected Configuration configuration;
	protected String commandName;

	public CommandBuilder(Configuration configuration, String commandName) {
		this.configuration = configuration;
		this.commandName = commandName;
	}

	public String build() throws TodacException {
		return build(null);
	}

	public String build(Map<String, String> parameters) throws TodacException {
		logger.debug("Command builder started");
		
		// check if this command exists in the configuration file
		String parameterName = Configuration.COMMAND_PREFIX + commandName;
		if (!configuration.containsKey(parameterName)) {
			throw new TodacException(ExceptionMessages.CONFIGURATION_PROPERTY_MISSING, parameterName);
		}

		// extract command name
		String command = configuration.getProperty(parameterName);

		// check if this command has any arguments specified
		parameterName += Configuration.ARGUMENT_SUFFIX;
		if (!configuration.containsKey(parameterName)) {
			return command;
		}

		// extract arguments
		String args = configuration.getProperty(parameterName);
		final Pattern pattern = Pattern.compile(Configuration.REGEXP_PARAMETER);
		Matcher matcher = pattern.matcher(args);

		while (matcher.find()) {
			String match = matcher.group();
			String variable = matcher.group()
					.replace(Configuration.REGEXP_PARAMETER_LEFT, "")
					.replace(Configuration.REGEXP_PARAMETER_RIGHT, "");	//perhaps should have used substring(2, n-2)			
			
			//look for the variable in configuration file
			if(configuration.containsKey(variable)){
				logger.debug("Parameter: " + variable + " found in configuration file");
				args = args.replace(match, configuration.getProperty(variable));
			}
			// look for the variable in supplied method arguments
			else if (parameters != null && parameters.containsKey(variable)) {
				logger.debug("Parameter: " + variable + " found in method supplied arguments");
				args = args.replace(match, parameters.get(variable));
			}
			// variable not found
			else {
				logger.error("Parameter: " + variable + " not found");
				throw new TodacException(ExceptionMessages.CONFIGURATION_PROPERTY_MISSING, variable);
			}
		}

		String result = command + " " + args;
		logger.debug("Built command: " + result);
		return result;
	}

	public String[] buildShellCommand(Map<String, String> parameters) throws TodacException {
		logger.debug("Command builder started for shell command");
		
		String command = build(parameters);
		parameters.put("shell_command", command);
		
		try {
			// check if this command exists in the configuration file
			String parameterName = Configuration.COMMAND_PREFIX + Configuration.COMMAND_SHELL;
			String shell = configuration.getProperty(parameterName, true);

			// arguments
			parameterName += Configuration.ARGUMENT_SUFFIX;

			// extract arguments
			String args = configuration.getProperty(parameterName);
			final Pattern pattern = Pattern.compile(Configuration.REGEXP_PARAMETER);
			Matcher matcher = pattern.matcher(args);

			while (matcher.find()) {
				String match = matcher.group();
				String variable = matcher.group()
						.replace(Configuration.REGEXP_PARAMETER_LEFT, "")
						.replace(Configuration.REGEXP_PARAMETER_RIGHT, "");

				// look for the variable in configuration file
				if (configuration.containsKey(variable)) {
					logger.debug("Parameter: " + variable + " found in configuration file");
					args = args.replace(match, configuration.getProperty(variable));
				}
				// look for the variable in supplied method arguments
				else if (parameters != null && parameters.containsKey(variable)) {
					logger.debug("Parameter: " + variable + " found in method supplied arguments");
					args = args.replace(match, parameters.get(variable));
				}
				// variable not found
				else {
					logger.error("Parameter: " + variable + " not found");
					throw new TodacException(ExceptionMessages.CONFIGURATION_PROPERTY_MISSING, variable);
				}
			}

			final Pattern shellCmdPattern = Pattern.compile(Configuration.REGEXP_INLINE_CMD);
			Matcher m = shellCmdPattern.matcher(args);

			List<String> shellCmd = new LinkedList<String>();
			shellCmd.add(shell);
			while (m.find()) {
				String arg = m.group();
				arg = arg.replace("'", "");
				shellCmd.add(arg);
			}
			
			return shellCmd.toArray(new String[1]);
			
		} catch (ConfigParsingException e) {
			logger.warn("Couldn't turn command: " + command + " to a shell command. Check if " + Configuration.COMMAND_SHELL
					+ " property is specified in the configuration file.");
		}
		
		logger.debug("Built shell command: " + command);
		return new String[] {command};
	}
}
