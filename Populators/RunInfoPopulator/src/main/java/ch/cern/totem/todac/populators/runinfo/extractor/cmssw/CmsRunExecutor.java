package ch.cern.totem.todac.populators.runinfo.extractor.cmssw;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandBuilder;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandExecutor;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Class responsible for cmsRun commmands execution. It concerns loading template cmsRun script, generating proper script for each run part, loading cmssw environment from configuration (or using
 * user's environment), executing cmsRun.
 * 
 * @author Michal Marciniec
 */
public class CmsRunExecutor implements IInitializable {
	protected static final int EV_INVALID_ANALYZER = 65;
	protected static final int EV_INVALID_SCRIPT = 90;
	protected static final int EV_INVALID_SHELL_CMD = 127;
	protected static final int EV_CMSRUN_NOT_FOUND = 255;

	protected String scriptDirPath = "scripts";
	protected boolean scriptRemoval = true;
	protected final String scriptSuffix = "_script.py";

	private static final Logger logger = Logger.getLogger(CmsRunExecutor.class);

	protected String[] environment = null;
	protected Configuration configuration;

	protected String analyzer;
	protected String scriptTemplate;
	protected String pluginPlaceholder;
	protected String filePlaceholder;

	public CmsRunExecutor(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * Executor's initialization: analyzer's plugin info is read from configuration file; cmsRun template script is loaded; CMSSW environment is loaded from file specified in configuration file. Note:
	 * if there is no file with environment specified in configuration file, user's environment will be used to run cmsRun (what is recommended option).
	 */
	public void initialize() throws TodacException {
		logger.debug("Initialization");

		analyzer = configuration.getProperty(Configuration.CMSSW_ANALYZER_PLUGIN, true);
		filePlaceholder = configuration.getProperty(Configuration.CMSSW_VMEA_PLACEHOLDER_CONFIG, true);
		pluginPlaceholder = configuration.getProperty(Configuration.CMSSW_PLUGIN_PLACEHOLDER_CONFIG, true);
		String path = configuration.getProperty(Configuration.CMSSW_SCRIPT_TEMPLATE_CONFIG, true);

		if (configuration.containsKey(Configuration.CMSSW_SCRIPT_TEMP_DIR))
			scriptDirPath = configuration.getProperty(Configuration.CMSSW_SCRIPT_TEMP_DIR);
		if (configuration.containsKey(Configuration.CMSSW_SCRIPT_TEMP_REMOVE))
			scriptRemoval = Boolean.valueOf(configuration.getProperty(Configuration.CMSSW_SCRIPT_TEMP_REMOVE));

		loadTemplate(path);
		loadEnvironment();

		logger.trace("Initialization [finished]");
	}

	public List<String> run(String vmeaFilePath) throws TodacException {
		logger.info("Executing cmsRun for " + vmeaFilePath);

		String scriptPath = generateScript(vmeaFilePath);

		logger.trace("Building cmsRun command");
		CommandBuilder cmdb = new CommandBuilder(configuration, "cmsrun");
		Map<String, String> args = new HashMap<String, String>(1);
		args.put("script", scriptPath);
		String[] shellCommand = cmdb.buildShellCommand(args);

		String command = "";
		for (int i = 0; i < shellCommand.length; ++i)
			command += shellCommand[i] + " ";
		logger.debug("cmsRun command to be executed: " + command);

		CommandExecutor exec = new CommandExecutor(configuration, shellCommand);
		int exitCode = exec.execute(environment);
		if (exitCode != 0 && exec.getErrorStream().size() > 0) {
			IOException e = new IOException(Arrays.toString(exec.getErrorStream().toArray(new String[1])));

			String msg = "";
			switch (exitCode) {
				case EV_INVALID_SCRIPT:
					msg = "Possible reason: invalid cmssw script template was provided";
					break;
				case EV_INVALID_ANALYZER:
					msg = "Possible reason: invalid cmssw analyzer module name has been provided in script template: " + analyzer;
					break;
				case EV_INVALID_SHELL_CMD:
					msg = "Possible reason: invalid shell command has been generated: " + command;
					break;
				case EV_CMSRUN_NOT_FOUND:
					msg = "Possible reason: no cmsRun in PATH or other binary hasn't been found in evnironment variables";
					break;
				default:
					msg = "Unknown situation";
			}
			throw new TodacException(ExceptionMessages.CMSSW_COMMAND_EXECUTION_FAIL, msg, e);
		}

		if (scriptRemoval) {
			new File(scriptPath).delete();
			new File(scriptDirPath).delete();
		}

		logger.info("cmsRun events extraction for " + vmeaFilePath + " [finished]");
		return exec.getInputStream();
	}

	private String generateScript(String vmeaFilePath) throws TodacException {
		logger.debug("Generating cmsRun script for " + vmeaFilePath);

		String path = scriptDirPath + File.separator + (new File(vmeaFilePath).getName()) + scriptSuffix;
		String absoluteVmeaPath = new File(vmeaFilePath).getAbsolutePath();

		String script = scriptTemplate.replace(filePlaceholder, absoluteVmeaPath);
		try {
			new File(path).delete();

			FileWriter fw = new FileWriter(path);
			fw.write(script);
			fw.close();
		} catch (IOException e) {
			throw new TodacException(ExceptionMessages.CMSSW_SCRIPT_TEMPLATE_WRITE_FAIL, path, e);
		}

		logger.trace("cmsRun script for " + vmeaFilePath + " generated in: " + path);
		return path;
	}

	private void loadTemplate(String path) throws TodacException {
		logger.debug("Loading CMSSW template script " + path);

		BufferedReader br = null;
		try {
			(new File(scriptDirPath)).mkdir();

			InputStream templateInputStream = Configuration.getResourceInputStream(path);
			br = new BufferedReader(new InputStreamReader(templateInputStream));
			StringBuilder sb = new StringBuilder();

			String line;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
			scriptTemplate = sb.toString();

			scriptTemplate = scriptTemplate.replace(pluginPlaceholder, analyzer);

		} catch (IOException e) {
			throw new TodacException(ExceptionMessages.CMSSW_SCRIPT_TEMPLATE_NOT_EXISTS, path, e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					logger.trace("Exception during buffered reader closing", e);
				}
		}
		logger.trace("Loading CMSSW template script " + path + " [finished]");
	}

	private void loadEnvironment() {
		String path = configuration.getProperty(Configuration.CMSSW_ENVIRONMENT);
		logger.debug("Loading CMSSW environment " + path);

		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(new File(path))));
			List<String> envList = new LinkedList<String>();
			String envVariable;

			while ((envVariable = br.readLine()) != null)
				envList.add(envVariable);
			environment = envList.toArray(new String[0]);

		} catch (Exception e) {
			logger.info("No valid environment file has been specified. Using user's environment to execute cmsRun");
			this.environment = null;
			return;
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					logger.trace("Exception during buffered reader closing", e);
				}
		}

		logger.trace("Loading CMSSW environment " + path + " [finished]");
	}

}
