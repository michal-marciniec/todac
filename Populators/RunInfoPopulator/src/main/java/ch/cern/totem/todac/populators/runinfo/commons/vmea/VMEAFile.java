package ch.cern.totem.todac.populators.runinfo.commons.vmea;

/**
 * Class for storing information associated with VMEAFile - local filepath, CASTOR filepath, 
 * part number and current status (extraction progress). 
 * @author Kamil Mielnik
 */
public class VMEAFile {
	
	protected String castorFilepath;
	protected String localFilepath;
	protected Integer partNumber;
	protected FileStatus status;
	
	public VMEAFile(Integer partNumber, String castorFilepath, String localFilepath){
		this.partNumber = partNumber;
		this.castorFilepath = castorFilepath;
		this.localFilepath = localFilepath;
		this.status = FileStatus.RECALL;
	}

	public VMEAFile(Integer partNumber, String castorFilepath, String localFilepath, FileStatus status){
		this(partNumber, castorFilepath, localFilepath);
		this.status = status;
	}

	public VMEAFile(Integer partNumber, String castorFilepath){
		this(partNumber, castorFilepath, null);
	}
	
	public Integer getPartNumber(){
		return partNumber;
	}
	
	public String getCastorFilepath(){
		return castorFilepath;
	}
	
	public synchronized String getLocalFilepath(){
		return localFilepath;
	}
	
	public FileStatus getFileStatus(){
		return status;
	}
	
	public void setLocalFilepath(String localFilepath) {
		this.localFilepath = localFilepath;
	}
	
	public synchronized void setFileStatus(FileStatus status){
		this.status = status;
	}
}
