package ch.cern.totem.todac.populators.runinfo.commons.runtime;

import java.util.Arrays;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;

/**
 * Works just like CommandExecutor (inherits its behavior) but runs in a different thread 
 * and returns value using INotifier<T> interface.
 * 
 * @author Kamil Mielnik
 * @param <T> Type of returned value.
 */
public class AsyncCommandExecutor<T> extends CommandExecutor implements Runnable {

	private final static Logger logger = Logger.getLogger(AsyncCommandExecutor.class);
	
	protected INotifier<T> notifier;
	protected T parameter;
	
	public AsyncCommandExecutor(Configuration configuration, String[] commandArray, INotifier<T> notifier, T parameter) {
		super(configuration, commandArray);
		this.notifier = notifier;
		this.parameter = parameter;
	}

	public AsyncCommandExecutor(Configuration configuration, String command, INotifier<T> notifier, T parameter) {
		super(configuration, command);
		this.notifier = notifier;
		this.parameter = parameter;
	}

	public void run() {
		logger.debug("Executing: " + Arrays.toString(commandArray));
		try {
			this.execute();
		} catch (TodacException e) {
			logger.error(ExceptionMessages.COMMAND_ASYNC_EXECUTION, e);
			notifier.notify(parameter, e);
		}
		logger.debug("Executed: " + Arrays.toString(commandArray));
		
		logger.debug("Notifying: " + Arrays.toString(commandArray));
		try {
			notifier.notify(parameter);
		} catch (TodacException e) {
			logger.error(ExceptionMessages.COMMAND_ASYNC_EXECUTION, e);
			notifier.notify(parameter, e);
		}
		logger.debug("Notified: " + Arrays.toString(commandArray));
	}

}
