package ch.cern.totem.todac.populators.runinfo.commons.vmea;

import ch.cern.totem.todac.commons.utils.progressbar.IProgressStatus;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;

public enum FileStatus implements IProgressStatus {
	
	RECALL		(Configuration.PROGRESS_RECALL),		// 0%
	STAGEIN		(Configuration.PROGRESS_STAGEIN),		// 5%
	STAGED		(Configuration.PROGRESS_STAGED),		// 35%
	DOWNLOADING	(Configuration.PROGRESS_DOWNLOADING),	// 55%
	PROCESSING	(Configuration.PROGRESS_PROCESSING),	// 85%
	DONE		(Configuration.PROGRESS_DONE);			// 100%
	
	private final float progress;
	
	FileStatus(float percentage){
		this.progress = percentage;
	}
	
	@Override
	public float get(){
		return progress;
	}
}
