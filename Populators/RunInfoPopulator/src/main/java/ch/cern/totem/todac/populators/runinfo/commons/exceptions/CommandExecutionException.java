package ch.cern.totem.todac.populators.runinfo.commons.exceptions;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessage;
import ch.cern.totem.todac.commons.exceptions.TodacException;

/**
 * Occurs when JVM is unable to execute system command. 
 * @author Kamil Mielnik
 */
public class CommandExecutionException extends TodacException {

	private static final long serialVersionUID = 1L;

	public CommandExecutionException(ExceptionMessage exceptionMessage, String command, Exception e){
		super(exceptionMessage, command, e);
	}

}
