package ch.cern.totem.todac.populators.runinfo.locator;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.INotifier;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEACollection;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;
import ch.cern.totem.todac.populators.runinfo.extractor.AsyncExtractor;
import ch.cern.totem.todac.populators.runinfo.extractor.EventsCollector;

/**
 * This class provides resources for extracting events data directly from CASTOR
 * without downloading files and without storing them locally.
 * @author Kamil Mielnik
 */
public class CastorFileProvider implements IFileProvider {
	
	private final static Logger logger = Logger.getLogger(CastorFileProvider.class);

	protected Configuration configuration;
	protected INotifier<VMEAFile> stager;
	protected EventsCollector eventsCollector;
	protected VMEACollection requested;
	protected int filesToProvide;
	protected int castorThreads;
	protected TodacException returnedException;
	protected Integer currentThreads;
	
	public CastorFileProvider(Configuration configuration, INotifier<VMEAFile> stager, EventsCollector eventsCollector, int filesToProvide){
		this.configuration = configuration;
		this.stager = stager;
		this.eventsCollector = eventsCollector;
		this.filesToProvide = filesToProvide;
		this.currentThreads = 0;
		this.castorThreads = 0;
		this.returnedException = null;
		this.requested = new VMEACollection();
	}
	
	public void initialize() throws TodacException {
		logger.debug("Initializing " + CastorFileProvider.class.getName());
		castorThreads = Integer.parseInt(configuration.getProperty(Configuration.CASTOR_THREADS_PARAM, true));
	}
	
	public void notify(VMEAFile vmeaFile) throws TodacException {
		synchronized(currentThreads){
			--filesToProvide;
			--currentThreads;
		}
		stager.notify(vmeaFile);
	}

	public void notify(VMEAFile parameter, TodacException exception) {
		returnedException = exception;
		logger.error("Exception has been thrown asynchronously", exception);
	}
	
	public boolean provide(VMEAFile vmeaFile) throws TodacException {
		if(currentThreads >= castorThreads){
			logger.trace("Providing queue is full");
			return false;
		}
		else{
			if(!requested.contains(vmeaFile)){
				requested.add(vmeaFile);
			}
			else{
				return false;
			}
		}
		
		synchronized(currentThreads){
			++currentThreads;
		}
		
		logger.debug("Extracting events with direct castor access " + vmeaFile.getCastorFilepath());
		
		AsyncExtractor extractor = new AsyncExtractor(this, eventsCollector, vmeaFile);
		Thread extractorThread = new Thread(extractor);
		extractorThread.start();
		
		if(returnedException != null){
			throw returnedException;
		}
		
		return true;
	}

	public void run(){
		logger.debug("Thread loop started");
		
		while(filesToProvide > 0){
			try {
				Thread.sleep(Configuration.DEFAULT_DOWNLOADER_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error("Thread.sleep has failed", e);
			}
			
			if(returnedException != null){
				break;
			}
		}	
		
		logger.debug("Thread loop finished");
	}
}
