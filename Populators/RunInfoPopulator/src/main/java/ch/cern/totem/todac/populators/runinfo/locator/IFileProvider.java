package ch.cern.totem.todac.populators.runinfo.locator;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.INotifier;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;

public interface IFileProvider extends IInitializable, Runnable, INotifier<VMEAFile> {
	
	boolean provide(VMEAFile vmeaFile) throws TodacException;
	
}
