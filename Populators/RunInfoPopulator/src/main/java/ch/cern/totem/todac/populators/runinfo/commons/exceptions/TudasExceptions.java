package ch.cern.totem.todac.populators.runinfo.commons.exceptions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class TudasExceptions extends Exception {

	private static final long serialVersionUID = 9064148470061034348L;
	
	protected Map<Integer, List<TudasException>> exceptions;
	
	public TudasExceptions(int numberOfParts){
		this.exceptions = new HashMap<Integer, List<TudasException>>();
	}
	
	public void addException(int partNumber, TudasException exception){
		if(!exceptions.containsKey(partNumber)){
			exceptions.put(partNumber, new LinkedList<TudasException>());
		}
		
		List<TudasException> partExceptions = exceptions.get(partNumber);
		partExceptions.add(exception);
	}
	
	public void clearExceptions(int partNumber){
		exceptions.remove(partNumber);
	}
	
	public Map<Integer, List<TudasException>> getExceptions(){
		return exceptions;
	}

}
