package ch.cern.totem.todac.populators.runinfo.commons.runtime;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.exceptions.CommandExecutionException;

/**
 * Provides means for executing runtime commands synchronously, 
 * reading their outputs and reading their exit codes. 
 * 
 * @author Kamil Mielnik
 */
public class CommandExecutor {

	private final static Logger logger = Logger.getLogger(CommandExecutor.class);
	
	protected String[] commandArray;
	protected Process process;
	protected Configuration configuration;
	protected List<String> inputStream;
	protected List<String> errorStream;
	
	public CommandExecutor(Configuration configuration, String command){
		this(configuration, command.split(" "));
	}
	
	public CommandExecutor(Configuration configuration, String[] commandArray){
		this.configuration = configuration;
		this.process = null;
		this.inputStream = null;
		this.errorStream = null;
		this.commandArray = commandArray;
	}
	
	public int execute() throws TodacException {
		return execute(null);
	}
	
	public int execute(String[] environment) throws TodacException {
		logger.debug("Executing command: " + Arrays.toString(commandArray));
		
		Runtime runtime = Runtime.getRuntime();
		try {
			process = runtime.exec(commandArray, environment);
			
			ConcurrentStreamReader stdinReader =
					new ConcurrentStreamReader(process.getInputStream());
			stdinReader.start();
			
			ConcurrentStreamReader stderrReader = 
					new ConcurrentStreamReader(process.getErrorStream());
			stderrReader.start();
			
			int exitCode = process.waitFor();
			stdinReader.join();
			stderrReader.join();
			logger.debug("Execution finished: " + Arrays.toString(commandArray));
			
			inputStream = stdinReader.getResult();
			errorStream = stderrReader.getResult();
			
			if(exitCode != 0){
				logger.warn(Arrays.toString(commandArray) + " exited with error code: " + exitCode);
				for(String err : errorStream){
					logger.warn(Arrays.toString(commandArray) + ": " + err);
				}
			}
			
			return exitCode;
		} catch (Exception e) {
			logger.error("Could not execute command:" + Arrays.toString(commandArray));
			throw new CommandExecutionException(ExceptionMessages.COMMAND_EXECUTION, Arrays.toString(commandArray), e);
		}
	}
	
	public List<String> getInputStream(){
		return inputStream;
	}
	
	public List<String> getErrorStream(){
		return errorStream;
	}
}
