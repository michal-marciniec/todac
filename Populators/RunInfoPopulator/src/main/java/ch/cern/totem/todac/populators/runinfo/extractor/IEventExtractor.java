/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.runinfo.extractor;

import java.util.List;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.tudasUtil.structure.Event;

public interface IEventExtractor {
	List<Event> extractEvents(String vmeaFilePath, int fileNo) throws TodacException;
	void initialize() throws TodacException;
}
