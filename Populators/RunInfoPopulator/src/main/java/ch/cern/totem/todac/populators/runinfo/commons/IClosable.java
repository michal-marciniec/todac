package ch.cern.totem.todac.populators.runinfo.commons;

import java.io.IOException;

public interface IClosable {
	void close() throws IOException;
}
