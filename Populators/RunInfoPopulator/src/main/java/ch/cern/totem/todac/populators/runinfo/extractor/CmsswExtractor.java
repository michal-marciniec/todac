package ch.cern.totem.todac.populators.runinfo.extractor;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.extractor.cmssw.CmsRunExecutor;
import ch.cern.totem.todac.populators.runinfo.extractor.cmssw.CmsrunOutputStreamReader;
import ch.cern.totem.todac.populators.runinfo.extractor.enums.OutputType;
import ch.cern.totem.tudas.tudasUtil.structure.Event;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Main class that controls events' data extraction using CMSSW. Uses CmsRunExecutor to run cmsRun for each run part and then uses IReader to parse cmsRun's output.
 * 
 * @author Michal Marciniec
 */
public class CmsswExtractor implements IEventExtractor {
	private static final Logger logger = Logger.getLogger(CmsswExtractor.class);

	private Configuration configuration;

	private OutputType outputType;

	private CmsRunExecutor cmsRunExecutor;
	protected CmsrunOutputStreamReader outputReader;

	public CmsswExtractor(Configuration config) {
		this.configuration = config;
	}

	public void initialize() throws TodacException {
		logger.trace("Initialization");

		cmsRunExecutor = new CmsRunExecutor(configuration);
		cmsRunExecutor.initialize();

        outputReader = new CmsrunOutputStreamReader(configuration);
		outputReader.initialize();
	}

	public List<Event> extractEvents(String vmeaFilePath, int fileNo) throws TodacException {
		logger.info("Events' data extractrion from: " + vmeaFilePath);

		List<String> outputStream = cmsRunExecutor.run(vmeaFilePath);
		List<Event> events = outputReader.parse(outputStream, fileNo, vmeaFilePath);

		if (events.size() == 0)
			logger.warn("No events extraced from " + vmeaFilePath + ". File contains no events' data?");
		else
			logger.info("Events' data extraction from " + vmeaFilePath + " [finished]");

		return events;
	}
}
