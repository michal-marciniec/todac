package ch.cern.totem.todac.populators.runinfo.commons.vmea;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;

/**
 * Class with static methods associated with extracting information from filenames using
 * regular expressions.
 * @author Kamil Mielnik
 */
public class VMEAUtils {
	
	private final static Logger logger = Logger.getLogger(VMEAUtils.class);
	
	public static boolean isFilepathValid(String filepath){
		return filepath.matches(Configuration.REGEXP_FILEPATH);
	}
	
	public static String getFilename(String filepath) throws TodacException {
		final Pattern pattern = Pattern.compile(Configuration.REGEXP_FILENAME);
		Matcher matcher = pattern.matcher(filepath);
		if(matcher.find()){
			logger.debug("Extracted filename: " + matcher.group());
			return matcher.group();
		}
		
		throw new TodacException(ExceptionMessages.INVALID_FILEPATH_SUPPLIED, filepath);
	}
	
	public static boolean isSingleFileRun(String filename){
		return filename.matches(Configuration.REGEXP_FILENAME_SINGLE_RUN);
	}	
	
	public static int getFileNumber(String filename) throws TodacException {
		final Pattern pattern = Pattern.compile(Configuration.REGEXP_FILENUMBER);
		Matcher matcher = pattern.matcher(filename);
		if(matcher.find()){
			String match = matcher.group();
			match = match.replaceAll(Configuration.REGEXP_DOT, "");
			
			logger.debug("Extracted file number: " + match);
			return Integer.parseInt(match);
		}
		
		throw new TodacException(ExceptionMessages.RUNINFO_WRONG_FILENAME, filename);
	}
	
	public static String getLocalFilepath(String castorFilepath, String downloadDirectory) throws TodacException {
		if(downloadDirectory == null)
			return null;
		
		if(!downloadDirectory.endsWith(File.separator)){
			downloadDirectory += File.separator;
		}
		return downloadDirectory + VMEAUtils.getFilename(castorFilepath);
	}
	
}
