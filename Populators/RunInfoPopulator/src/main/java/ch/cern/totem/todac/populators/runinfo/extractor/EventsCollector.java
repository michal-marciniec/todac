package ch.cern.totem.todac.populators.runinfo.extractor;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.mexer.Classmexer;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.TotemRunInfo;
import ch.cern.totem.todac.populators.runinfo.commons.exceptions.ConfigParsingException;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.FileStatus;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;
import ch.cern.totem.todac.populators.runinfo.uploader.Uploader;
import ch.cern.totem.tudas.tudasUtil.structure.Event;

/**
 * This class provies means for aggregating and extracting results from each part (file) of a run.
 * @author Kamil Mielnik
 */
public class EventsCollector implements IInitializable {

	private final static Logger logger = Logger.getLogger(EventsCollector.class);
	
	protected Map<Integer, List<Event>> eventsCollection;
	protected Configuration configuration;
	protected boolean download;
	protected Uploader uploader;
	protected int runNo;
	protected String description;
	
	public EventsCollector(Configuration configuration, Uploader uploader, int runNo, String description){
		this.configuration = configuration;
		this.eventsCollection = new HashMap<Integer, List<Event>>();
		this.download = false;
		this.uploader = uploader;
		this.runNo = runNo;
		this.description = description;
	}

	public void initialize() throws TodacException {
		download = configuration.getProperty(Configuration.CASTOR_ACCESS_PARAM, true).equals(Configuration.CASTOR_ACCESS_DOWNLOAD_PARAM);
	}
	
	/**
	 * Runs CMSSWExtractor and aggregates results returned by it. 
	 * @param vmeaFile
	 * @throws IOException
	 * @throws ConfigParsingException
	 */
	public void extractEvents(VMEAFile vmeaFile) throws TodacException {
		logger.debug("Extracting events from file: " + vmeaFile.getCastorFilepath());
		
		vmeaFile.setFileStatus(FileStatus.PROCESSING);
		
		IEventExtractor eventExtractor = new CmsswExtractor(configuration);
		eventExtractor.initialize();
		String filepath = download ? vmeaFile.getLocalFilepath() : vmeaFile.getCastorFilepath();
		List<Event> events = eventExtractor.extractEvents(filepath, vmeaFile.getPartNumber());
		synchronized (eventsCollection) {
			eventsCollection.put(vmeaFile.getPartNumber(), events);
		}
		
		vmeaFile.setFileStatus(FileStatus.DONE);
		logger.debug(events.size() + " events extracted from file: " + vmeaFile.getCastorFilepath());
		
		TotemRunInfo runInfo = aggregateResults(vmeaFile.getPartNumber());
		boolean result = uploader.populate(runInfo, vmeaFile.getPartNumber());
		if(result){
			synchronized(eventsCollection){
				eventsCollection.remove(vmeaFile.getPartNumber());
			}
		}

	}
	
	/**
	 * Attempts to create a TotemRunInfo object containing information about the run for a single part. 
	 * @param partNumber - number of part to be encapsuled in a TotemRunInfo structure
	 * @param description - Description to be put in a returned object
	 * @return TotemRunInfo object containing events data.
	 * @throws IOException
	 */
	public synchronized TotemRunInfo aggregateResults(int partNumber) throws TodacException {
		synchronized(eventsCollection){ 
			logger.debug("Aggregating results for part: " + partNumber);
		
			List<Event> list = eventsCollection.get(partNumber);
			Event[] events = list.toArray(new Event[1]);
			Classmexer.measure(events, "eventsCollection part no. " + partNumber);
			logger.debug("Results aggregated for part: " + partNumber);
			
			Event firstEvent = events.length > 0 ? events[0] : null;
			Event lastEvent = events.length > 0 ? events[events.length - 1] : null;
			
			return new TotemRunInfo(runNo, firstEvent, lastEvent, events, description);
		}
	}
	
	/**
	 * Attempts to create a TotemRunInfo object containing all the information about the run. 
	 * @param description - Description to be put in a returned object
	 * @return TotemRunInfo object containing all the events data.
	 * @throws IOException
	 */
	public synchronized TotemRunInfo aggregateResults() throws TodacException {
		synchronized(eventsCollection){ 
			logger.debug("Aggregating results");
			
			int eventsCount = 0;
			for(Integer i : eventsCollection.keySet()){
				eventsCount += eventsCollection.get(i).size();
				logger.debug(i + " part in map - OK");
			}
			
			if(eventsCount == 0) {
				logger.warn("No events in result set");
				return null;
			}
			
			Event[] events = new Event[eventsCount];
		
			int eventsSoFar = 0;
			for(int partNumber = 0; partNumber < eventsCollection.size(); ++partNumber){
				if(!eventsCollection.containsKey(partNumber)){
					throw new TodacException(ExceptionMessages.MISSING_RUN_PART, "Part number: " + partNumber);
				}
				
				Classmexer.measure(eventsCollection.get(partNumber), "eventsCollection part no. " + partNumber);
				
				List<Event> part = eventsCollection.get(partNumber);
				
				int eventsInPart = eventsCollection.get(partNumber).size();
				eventsCollection.remove(partNumber);	// we hope to save some memory
				System.arraycopy(part.toArray(new Event[1]), 0, events, eventsSoFar, eventsInPart);
				eventsSoFar += eventsInPart;
			}
			logger.debug("Results aggregated");
			
			Classmexer.measure(eventsCollection, "Overall eventsCollection");
			
			Event firstEvent = events.length > 0 ? events[0] : null;
			Event lastEvent = events.length > 0 ? events[events.length - 1] : null;
			
			return new TotemRunInfo(runNo, firstEvent, lastEvent, events, description);
		}
	}
}
