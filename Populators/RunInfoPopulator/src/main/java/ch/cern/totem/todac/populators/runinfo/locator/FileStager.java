package ch.cern.totem.todac.populators.runinfo.locator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.IInitializable;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandBuilder;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandExecutor;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.INotifier;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.FileStatus;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEACollection;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEAFile;
import ch.cern.totem.todac.populators.runinfo.extractor.EventsCollector;

/**
 * This class provides resources to stage files on CASTOR.
 * @author Kamil Mielnik
 */
public class FileStager implements IInitializable, INotifier<VMEAFile> {

	private final static Logger logger = Logger.getLogger(FileStager.class);

	protected Configuration configuration;
	protected EventsCollector eventsCollector;
	protected VMEACollection scheduled;
	protected IFileProvider provider;
	
	protected int checkTime;
	protected String[] queryEnvironment;
	protected String[] getEnvironment;
	protected String statusStaged;
	protected String statusStageIn;
	protected String statusRecall;
	protected boolean download;
	
	protected TodacException returnedException;
	
	public FileStager(Configuration configuration, EventsCollector eventsCollector){
		this.configuration = configuration;
		this.eventsCollector = eventsCollector;
		this.scheduled = new VMEACollection();
		this.provider = null;
		this.checkTime = 0;
		this.returnedException = null;
		this.download = false;
	}

	public void initialize() throws TodacException {
		logger.debug("Initializing " + FileStager.class);
		
		checkTime = Integer.parseInt(configuration.getProperty(Configuration.CHECK_TIME_PARAM, true));
		queryEnvironment = new String[1];
		queryEnvironment[0] = configuration.getProperty(Configuration.COMMAND_QUERY_ENV);
		getEnvironment = new String[1];
		getEnvironment[0] = configuration.getProperty(Configuration.COMMAND_GET_ENV);
		statusStaged = configuration.getProperty(Configuration.STATUS_STAGED, true);
		statusStageIn = configuration.getProperty(Configuration.STATUS_STAGEIN, true);
		statusRecall = configuration.getProperty(Configuration.STATUS_RECALL, true);
		download = configuration.getProperty(Configuration.CASTOR_ACCESS_PARAM, true).equals(Configuration.CASTOR_ACCESS_DOWNLOAD_PARAM);
	}
	
	/**
	 * Starts first level staging (checks all files for current state and requests unavailable files) 
	 * and then second level staging - updating file status and choosing appropriate actions based upon
	 * the status. Schedules files for downloading.
	 * @param files
	 * @return Returns object responsible for downloading all staged files.
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public IFileProvider stage(VMEACollection files) throws TodacException {
		logger.debug("Staging requested for " + files.size() + " files");
		if(download){
			logger.debug(LocalFileProvider.class.getName() + " chosen as FileProvider");
			provider = new LocalFileProvider(configuration, this, eventsCollector, files.size());
		}
		else{
			logger.debug(CastorFileProvider.class.getName() + " chosen as FileProvider");
			provider = new CastorFileProvider(configuration, this, eventsCollector, files.size());
		}
		
		provider.initialize();
		
		firstLevelStaging(files);
		secondLevelStaging();
		
		return provider;
	}
	
	public void notify(VMEAFile vmeaFile) throws TodacException {
		try {
			stageNext();
		} catch (TodacException e) {
			logger.error("Thread.sleep has failed", e);
		}
	}

	public void notify(VMEAFile vmeaFile, TodacException exception) {
		scheduled.add(vmeaFile);
		returnedException = exception;
		logger.error("Exception has been thrown asynchronously", exception);
	}
	
	protected void firstLevelStaging(VMEACollection files) throws TodacException {
		logger.debug("1st level staging started with " + files.size() + " files to stage");
	
		for(VMEAFile vmeaFile : files){
			FileStatus status = FileStatus.RECALL;
			status = updateStatus(vmeaFile).getFileStatus();
			
			switch(status){
				case RECALL:
					recall(vmeaFile);
				case STAGEIN:
					scheduled.add(vmeaFile);
					break;
				case STAGED:
					if(!provider.provide(vmeaFile)){
						scheduled.add(vmeaFile);
					}
					break;
				default:
					break;
			}
		}
		logger.debug("1st level staging finished");
	}
	
	public void secondLevelStaging() throws TodacException {
		logger.debug("2nd level staging started with " + scheduled.size() + " files to stage");
		
		while(scheduled.size() > 0){

			try {
				Thread.sleep(checkTime);
			} catch (InterruptedException e) {
				logger.error("Thread.sleep has failed", e);
			}
			
			stageNext();
		}

		logger.debug("2nd level staging finished");
	}
	
	protected void stageNext() throws TodacException {
		synchronized(scheduled){
			Iterator<VMEAFile> iterator = scheduled.iterator();

			while(iterator.hasNext()){
				if(returnedException != null){
					throw returnedException;
				}
				
				VMEAFile vmeaFile = iterator.next();
				updateStatus(vmeaFile);
				FileStatus status = vmeaFile.getFileStatus();
				
				boolean downloadInProgress = true;
				switch(status)
				{
					case RECALL:
						recall(vmeaFile);
						logger.debug("A recall for " + vmeaFile.getCastorFilepath() + " is needed during 2nd level staging");
						break;
					case STAGED:
						downloadInProgress = provider.provide(vmeaFile);
						if(downloadInProgress){
							iterator.remove();
						}
						break;
					default:
						// wait some more for the files to be staged 
						break;
				}
				
				if(!downloadInProgress){
					break;
				}
			}
		}
	}
	
	protected VMEAFile updateStatus(VMEAFile vmeaFile){
		try{
			CommandBuilder commandBuilder = new CommandBuilder(configuration, Configuration.COMMAND_QUERY);
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put(Configuration.COMMAND_QUERY_ARG, vmeaFile.getCastorFilepath());
			String command = commandBuilder.build(parameters);
			
			CommandExecutor processExecutor = new CommandExecutor(configuration, command);
			processExecutor.execute(queryEnvironment);
			
			FileStatus status = FileStatus.RECALL;
			List<String> output = processExecutor.getInputStream();
			for(String line : output){
				if(line.contains(statusStaged)){
					status = FileStatus.STAGED;
					break;
				}
				if(line.contains(statusStageIn)){
					status = FileStatus.STAGEIN;
					break;
				}
				if(line.contains(statusRecall)){
					status = FileStatus.RECALL;
					break;
				}
			}
			
			vmeaFile.setFileStatus(status);
			logger.trace(vmeaFile.getCastorFilepath() + " : " + vmeaFile.getFileStatus().toString());
		}
		catch(TodacException e){
			logger.warn("Could not update status for " + vmeaFile.getCastorFilepath() 
					+ "\n\t" + e.getMessage());
		}
		
		return vmeaFile;
	}

	/**
	 * Builds a string containing system command that allows requesting a file on CASTOR to be available.
	 * @param vmeaFile
	 * @return String containin desired system command.
	 * @throws IOException
	 */
	protected void recall(VMEAFile vmeaFile) throws TodacException {
		logger.debug("Recalling file from CASTOR: " + vmeaFile.getCastorFilepath());
		CommandBuilder commandBuilder = new CommandBuilder(configuration, Configuration.COMMAND_GET);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(Configuration.COMMAND_GET_ARG, vmeaFile.getCastorFilepath());
		String command = commandBuilder.build(parameters);
		
		CommandExecutor processExecutor = new CommandExecutor(configuration, command);
		processExecutor.execute(getEnvironment);
	}

}
