package ch.cern.totem.todac.populators.runinfo;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.todac.populators.runinfo.commons.Configuration;
import ch.cern.totem.todac.populators.runinfo.commons.TotemRunInfo;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandBuilder;
import ch.cern.totem.todac.populators.runinfo.commons.runtime.CommandExecutor;
import ch.cern.totem.todac.populators.runinfo.commons.vmea.VMEACollection;
import ch.cern.totem.todac.populators.runinfo.extractor.EventsCollector;
import ch.cern.totem.todac.populators.runinfo.locator.FileLocator;
import ch.cern.totem.todac.populators.runinfo.locator.FileStager;
import ch.cern.totem.todac.populators.runinfo.locator.IFileProvider;
import ch.cern.totem.todac.populators.runinfo.uploader.Uploader;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;

/**
 * The RunInfoExtractor class provides means to find files
 * associated with specified TOTEM Run on CASTOR, access them
 * in a transparent way and extract events data from those files. 
 * 
 * @author Kamil Mielnik
 */

public class RunInfoPopulator implements MeasurableProgress {

	private final static Logger logger = Logger.getLogger(RunInfoPopulator.class);
	
	protected RunInformationManager runInformationManager;
	protected Configuration configuration;
	protected VMEACollection files;
	protected TotemRunInfo runInfo;
	protected String downloadDirectory;
	protected boolean download; 
	
	public RunInfoPopulator(RunInformationManager runInformationManager){
		this.runInformationManager = runInformationManager;
		this.files = null;
		this.runInfo = null;
		this.download = false;
	}
	
	public void initialize() throws TodacException {
		logger.debug("Initializing " + RunInfoPopulator.class.getName());
		configuration = new Configuration();
		configuration.initialize();
		downloadDirectory = configuration.getProperty(Configuration.DOWNLOAD_DIRECTORY_PARAM, true);
		download = configuration.getProperty(Configuration.CASTOR_ACCESS_PARAM, true).equals(Configuration.CASTOR_ACCESS_DOWNLOAD_PARAM);
		checkCommands();
	}
	
	protected void checkCommands() throws TodacException {
		String commandWhich = Configuration.COMMAND_WHICH;
		CommandBuilder whichBuilder = new CommandBuilder(configuration, commandWhich);
		String whichArg = Configuration.COMMAND_WHICH_ARG;
		Map<String, String> argMap = new HashMap<String, String>();
		String command = null;
		CommandExecutor commandExecutor = null;
		
		String cmsRun = "cmsRun";
		argMap.put(whichArg, cmsRun);
		command = whichBuilder.build(argMap);
		commandExecutor = new CommandExecutor(configuration, command);
		if(commandExecutor.execute() != 0) throw new TodacException(ExceptionMessages.NO_CMSRUN);

		String nsfind = Configuration.COMMAND_FIND;
		argMap.put(whichArg, nsfind);
		command = whichBuilder.build(argMap);
		commandExecutor = new CommandExecutor(configuration, command);
		if(commandExecutor.execute() != 0) throw new TodacException(ExceptionMessages.NO_NSFIND);
		
		String stager_get = Configuration.COMMAND_GET;
		argMap.put(whichArg, stager_get);
		command = whichBuilder.build(argMap);
		commandExecutor = new CommandExecutor(configuration, command);
		if(commandExecutor.execute() != 0) throw new TodacException(ExceptionMessages.NO_STAGER_GET);

		String stager_qry = Configuration.COMMAND_QUERY;
		argMap.put(whichArg, stager_qry);
		command = whichBuilder.build(argMap);
		commandExecutor = new CommandExecutor(configuration, command);
		if(commandExecutor.execute() != 0) throw new TodacException(ExceptionMessages.NO_STAGER_QRY);		
	}
	
	public void saveRunInfo(int runNumber, String description) throws TodacException {
		FileLocator fileLocator = download ? new FileLocator(configuration,
				downloadDirectory) : new FileLocator(configuration);
		fileLocator.initialize();
		files = fileLocator.locate(runNumber);

		Uploader uploader = new Uploader(configuration, runInformationManager, files.size());
		uploader.initialize();

		EventsCollector eventsCollector = new EventsCollector(configuration, uploader, runNumber, description);
		eventsCollector.initialize();

		FileStager fileStager = new FileStager(configuration, eventsCollector);
		fileStager.initialize();

		IFileProvider provider = fileStager.stage(files);

		Thread downloaderThread = new Thread(provider);
		downloaderThread.start();
		try {
			downloaderThread.join();
		} catch (InterruptedException e) {
			throw new TodacException(ExceptionMessages.THREADING, e);
		}

		TotemRunInfo runInfo = eventsCollector.aggregateResults();
		if(runInfo != null){
			uploader.populate(runInfo);
		}
		
		if(uploader.hasThrownAnException()){
			throw new TodacException(ExceptionMessages.RUNINFO_EXCEPTION_COLLECTION, uploader.getExceptions());
		}
		else{
			System.out.println("No exceptions!");
		}
	}
	
	public void saveRunInfo(int runNumber) throws TodacException {
		saveRunInfo(runNumber, Configuration.DEFAULT_RUN_DESCRIPTION);
	}
	
	@Override
	public Progress getProgress(){
		return files == null ? new Progress(0.0f) : files.getProgress();
	}
	
}
