/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.runinfo.commons.exceptions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.cern.totem.todac.commons.exceptions.TodacException;

public class ExceptionCollection extends Exception {

	private static final long serialVersionUID = 9064148470061034348L;
	
	protected Map<Integer, List<TodacException>> exceptions;
	
	public ExceptionCollection(int numberOfParts){
		this.exceptions = new HashMap<Integer, List<TodacException>>();
	}
	
	public void addException(int partNumber, TodacException exception){
		if(!exceptions.containsKey(partNumber)){
			exceptions.put(partNumber, new LinkedList<TodacException>());
		}
		
		List<TodacException> partExceptions = exceptions.get(partNumber);
		partExceptions.add(exception);
	}
	
	public void clearExceptions(int partNumber){
		exceptions.remove(partNumber);
	}
	
	public Map<Integer, List<TodacException>> getExceptions(){
		return exceptions;
	}

}