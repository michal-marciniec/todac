package ch.cern.totem.todac.populators.runinfo.commons;

import ch.cern.totem.todac.commons.exceptions.TodacException;

public interface IInitializable {
	void initialize() throws TodacException;
}
