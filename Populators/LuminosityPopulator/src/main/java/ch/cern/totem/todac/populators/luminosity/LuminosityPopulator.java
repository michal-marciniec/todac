package ch.cern.totem.todac.populators.luminosity;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.tudas.clients.java.data.manager.LuminosityManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

/**
 * It is required for csv files to have no comments at all. First line should
 * contain just luminosity label and all the following lines should contain
 * actual luminosity data in the according format.
 * run:fill,ls,UTCTime,delivered(/ub),recorded(/ub),"[bx,Hz/ub]"
 * 
 * @author Kamil Mielnik
 */
public class LuminosityPopulator implements MeasurableProgress {

	private final static Logger logger = Logger.getLogger(LuminosityPopulator.class);

	protected LuminosityManager luminosityManager = null;
	protected double lsLengthSec = Configuration.LSLENGTHSEC;
	protected String label = null;
	protected Progress progress = null;

	public LuminosityPopulator(LuminosityManager luminosityManager) {
		this.luminosityManager = luminosityManager;
		this.progress = new Progress(PopulationProgress.START);
	}

	public void setLsLengthSec(double lsLengthSec) {
		logger.debug("Setting lsLengthSec value for all luminosity: " + lsLengthSec);
		this.lsLengthSec = lsLengthSec;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Format for csv files: Run:Fill,LS,UTCTime,Beam
	 * Status,E(GeV),Delivered(/ub),Recorded(/ub),avgPU
	 * 
	 * @param filepath
	 * @throws TodacException
	 */
	public void saveLuminosityByLs(String filepath) throws TodacException {
		logger.debug("Saving luminosityByLs from: " + filepath);
		progress.reset();

		progress.set(PopulationProgress.READING);
		saveSingleLuminosityByLs(filepath);
		
		progress.set(PopulationProgress.DONE);
	}

	/**
	 * Format for csv files:
	 * run:fill,ls,UTCTime,delivered(/ub),recorded(/ub),"[bx,Hz/ub]"
	 * 
	 * @param filepath
	 * @throws TodacException
	 */
	public void saveLuminosityByLsXing(String filepath) throws TodacException {
		logger.debug("Saving luminosityByLsXing from: " + filepath);
		progress.reset();
		
		progress.set(PopulationProgress.READING);
		saveSingleLuminosityByLsXing(filepath);
		
		progress.set(PopulationProgress.DONE);
	}

	protected void saveSingleLuminosityByLs(String filepath)
			throws TodacException {
		ReaderLs readerLs = new ReaderLs(filepath, lsLengthSec, label);
		LuminosityByLsStruct luminosityls = null;
		try {
			luminosityls = readerLs.read();
		} catch (Exception e) {
			logger.error("Unable to parse file: " + filepath);
			throw new TodacException(ExceptionMessages.FILE_PARSING, e);
		}
		
		try {
			progress.set(PopulationProgress.POPULATING);
			logger.debug("Saving single luminosityByLs structure");
			luminosityManager.saveLuminosityByLs(luminosityls.startTime,
					luminosityls.endTime, luminosityls.label,
					luminosityls.luminosity);
		} catch (TudasException e) {
			logger.error("Unable to save luminosityByLs into the database via LuminosityManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
		logger.trace("OK");
	}

	protected void saveSingleLuminosityByLsXing(String filepath)
			throws TodacException {
		ReaderLsXing readerLsXing = new ReaderLsXing(filepath, lsLengthSec,
				label);
		LuminosityByLsXingStruct luminositylsxing = null;
		try {
			luminositylsxing = readerLsXing.read();
		} catch (Exception e) {
			logger.error("Unable to parse file: " + filepath);
			throw new TodacException(ExceptionMessages.FILE_PARSING, e);
		}

		try {
			progress.set(PopulationProgress.POPULATING);
			logger.debug("Saving single luminosityByLsXing structure");
			luminosityManager.saveLuminosityByLsXing(
					luminositylsxing.startTime, luminositylsxing.endTime,
					luminositylsxing.label, luminositylsxing.luminosity);
		} catch (TudasException e) {
			logger.error("Unable to save luminosityByLsXing into the database via LuminosityManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
		logger.trace("OK");
	}

	@Override
	public Progress getProgress() {
		return progress;
	}
}
