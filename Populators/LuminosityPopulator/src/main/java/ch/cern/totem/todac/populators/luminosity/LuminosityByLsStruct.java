package ch.cern.totem.todac.populators.luminosity;

import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;

class LuminosityByLsStruct {

	public long startTime;
	public long endTime;
	public String label;
	public LuminosityByLs[] luminosity;

}
