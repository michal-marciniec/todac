package ch.cern.totem.todac.populators.luminosity;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;

class ReaderLs {

	private final static Logger logger = Logger.getLogger(ReaderLs.class);
	
	protected String filepath;
	protected SimpleDateFormat simpleDateFormat;
	protected double lsLengthSec;
	protected String label;

	public ReaderLs(String filepath, double lsLengthSec, String label) {
		this.filepath = filepath;
		this.simpleDateFormat = new SimpleDateFormat(Configuration.DATE_FORMAT);
		this.lsLengthSec = lsLengthSec;
		this.label = label;
	}

	public LuminosityByLsStruct read() throws Exception {
		logger.debug("Reading luminosityByLs file: " + filepath);
		
		LuminosityByLsStruct luminosity = new LuminosityByLsStruct();
		luminosity.label = label;
		List<LuminosityByLs> list = new ArrayList<LuminosityByLs>();

		
		BufferedReader br = null;
		String line = Configuration.EMPTY_STRING;
		try {
			br = new BufferedReader(new FileReader(filepath));

			long start = Long.MAX_VALUE;
			long end = Long.MIN_VALUE;

			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				lineNumber++;
				
				// comment
				if (line.startsWith(Configuration.COMMENT_PREFIX)) {
					logger.debug("Encountered line(" + lineNumber + ") is a comment");
					if (luminosity.label == null && line.matches(Configuration.REGEXP_LABEL)) {
						logger.debug("Encountered line(" + lineNumber + ") contains label information");
						luminosity.label = extractLabel(line);
					}
					continue;
				}

				// columns
				if (line.matches(Configuration.REGEXP_COLUMNS)) {
					logger.debug("Encountered line(" + lineNumber + ") is a columns description");
					if(!line.toLowerCase().matches(Configuration.REGEXP_COLUMNS_LS)){
						throw new TodacException(ExceptionMessages.FILE_PARSING);
					}
					continue;
				}

				// data
				String[] lines = line.split(Configuration.CSV_SEPARATOR);
				if (lines.length != Configuration.COLUMNS_LUMINOSITY_BY_LS) {
					logger.warn("Unrecognized line(" + lineNumber + ") ommitted: " + line);
					continue; // unexpected line
				}

				LuminosityByLs lum = new LuminosityByLs();

				String[] runFill = lines[0].split(Configuration.RUN_FILL_SEPARATOR);
				lum.cmsRun = Integer.parseInt(runFill[0]);
				lum.fill = Integer.parseInt(runFill[1]);
				lum.ls = lines[1];
				lum.timestamp = simpleDateFormat.parse(lines[2]).getTime();
				lum.beamStatus = lines[3];
				lum.energy = Double.parseDouble(lines[4]);
				lum.delivered = Double.parseDouble(lines[5]);
				lum.recorded = Double.parseDouble(lines[6]);
				lum.avgPU = Double.parseDouble(lines[7]);
				lum.lslengthsec = lsLengthSec;

				if (lum.timestamp < start) {
					start = lum.timestamp;
				}
				if (lum.timestamp > end) {
					end = lum.timestamp;
				}

				list.add(lum);
			}

			luminosity.startTime = start;
			luminosity.endTime = end;
			luminosity.luminosity = list.toArray(new LuminosityByLs[1]);
		} catch (Exception e) {
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e2) {
					throw e2;
				}
			}
		}

		logger.debug("File: " + filepath + " read");
		return luminosity;
	}

	public static String extractLabel(String line) {
		logger.debug("Extracting luminosityByLs label from line: " + line);
		
		String trimmed = line.replace(Configuration.COMMENT_PREFIX,
				Configuration.EMPTY_STRING).replace(Configuration.BLANK_CHARACTER,
				Configuration.EMPTY_STRING);
		String[] parts = trimmed.split(Configuration.CSV_SEPARATOR);
		String label = Configuration.EMPTY_STRING;
		for (String part : parts) {
			label += part
					.substring(part.indexOf(Configuration.LABEL_SEPARATOR) + 1)
					+ Configuration.LABEL_JOIN;
		}
		label = label.substring(0, label.lastIndexOf(Configuration.LABEL_JOIN));
		if(label.length() > Configuration.MAXIMUM_LABEL_LENGTH){
			logger.warn("Extracted label '" + label + "' is too long");
			label = label.substring(label.length() - Configuration.MAXIMUM_LABEL_LENGTH);
		}
		
		logger.debug("Extracted luminosity label is: " + label);
		return label;
	}
}
