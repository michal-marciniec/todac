package ch.cern.totem.todac.populators.luminosity;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;

class ReaderLsXing {

	private final static Logger logger = Logger.getLogger(ReaderLsXing.class);
	
	protected String filepath;
	protected SimpleDateFormat simpleDateFormat;
	protected double lsLengthSec;
	protected String label;

	public ReaderLsXing(String filepath, double lsLengthSec, String label) {
		this.filepath = filepath;
		this.simpleDateFormat = new SimpleDateFormat(Configuration.DATE_FORMAT);
		this.lsLengthSec = lsLengthSec;
		this.label = label;
	}

	public LuminosityByLsXingStruct read() throws Exception {
		logger.debug("Reading luminosityByLs file: " + filepath);
		
		LuminosityByLsXingStruct luminosity = new LuminosityByLsXingStruct();
		luminosity.label = label;
		List<LuminosityByLsXing> list = new ArrayList<LuminosityByLsXing>();

		BufferedReader br = null;
		String line = Configuration.EMPTY_STRING;
		try {
			br = new BufferedReader(new FileReader(filepath));

			long start = Long.MAX_VALUE;
			long end = Long.MIN_VALUE;

			int lineNumber = 0;
			while ((line = br.readLine()) != null) {
				lineNumber++;
				
				// comment
				if (line.startsWith(Configuration.COMMENT_PREFIX)) {
					logger.debug("Encountered line(" + lineNumber + ") is a comment");
					if (luminosity.label == null && line.matches(Configuration.REGEXP_LABEL)) {
						logger.debug("Encountered line(" + lineNumber + ") contains label information");
						luminosity.label = ReaderLs.extractLabel(line);
					}
					continue;
				}

				// columns
				if (line.matches(Configuration.REGEXP_COLUMNS)) {
					logger.debug("Encountered line(" + lineNumber + ") is a columns description");
					if(!line.toLowerCase().matches(Configuration.REGEXP_COLUMNS_LSXING)){
						throw new TodacException(ExceptionMessages.FILE_PARSING);
					}
					continue;
				}

				// data
				String[] lines = line.split(Configuration.CSV_SEPARATOR);
				if (lines.length < Configuration.COLUMNS_LUMINOSITY_BY_LSXING) {
					logger.warn("Unrecognized line(" + lineNumber + ") ommitted: " + line);
					continue; // unexpected line
				}

				LuminosityByLsXing lum = new LuminosityByLsXing();
				String[] runFill = lines[0].split(Configuration.RUN_FILL_SEPARATOR);
				lum.cmsRun = Integer.parseInt(runFill[0]);
				lum.fill = Integer.parseInt(runFill[1]);
				lum.ls = lines[1];
				lum.timestamp = simpleDateFormat.parse(lines[2]).getTime();
				lum.delivered = Double.parseDouble(lines[3]);
				lum.recorded = Double.parseDouble(lines[4]);
				lum.lslengthsec = lsLengthSec;
				lum.luminosityValues = new double[lines.length
						- Configuration.COLUMNS_LUMINOSITY_BY_LSXING];
				for (int i = 0; i < lum.luminosityValues.length; ++i) {
					lum.luminosityValues[i] = Double.parseDouble(lines[i
							+ Configuration.COLUMNS_LUMINOSITY_BY_LSXING]);
				}

				if (lum.timestamp < start) {
					start = lum.timestamp;
				}
				if (lum.timestamp > end) {
					end = lum.timestamp;
				}

				list.add(lum);
			}

			luminosity.startTime = start;
			luminosity.endTime = end;
			luminosity.luminosity = list.toArray(new LuminosityByLsXing[1]);
		} catch (Exception e) {
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e2) {
					throw e2;
				}
			}
		}

		logger.debug("File: " + filepath + " read");
		return luminosity;
	}

}
