package ch.cern.totem.todac.populators.luminosity;

import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;

class LuminosityByLsXingStruct {

	public long startTime;
	public long endTime;
	public String label;
	public LuminosityByLsXing[] luminosity;

}
