package ch.cern.totem.todac.populators.luminosity;

class Configuration {
	
	public static final String REGEXP_LABEL = ".*lumitype.*datatag.*normtag.*worktag.*";
	public static final String REGEXP_COLUMNS = "^(R|r).*";
	public static final String REGEXP_COLUMNS_LS = 
			"run:fill,ls,utctime,beam status,e\\(gev\\),delivered\\(/ub\\),recorded\\(/ub\\),avgpu";
	public static final String REGEXP_COLUMNS_LSXING = 
			"run:fill,ls,utctime,delivered\\(/ub\\),recorded\\(/ub\\),\"\\[bx,hz/ub\\]\"";

	public static final String DATE_FORMAT = "MM/dd/yy HH:mm:ss";
	public static final String CSV_SEPARATOR = ",";
	public static final String RUN_FILL_SEPARATOR = ":";
	public static final String LABEL_SEPARATOR = ":";
	public static final String LABEL_JOIN = ";";
	public static final String COMMENT_PREFIX = "*";
	public static final String EMPTY_STRING = "";
	public static final String BLANK_CHARACTER = " ";

	public static final int COLUMNS_LUMINOSITY_BY_LS = 8;
	public static final int COLUMNS_LUMINOSITY_BY_LSXING = 5;
	
	public static final int MAXIMUM_LABEL_LENGTH = 50;

	public static final double LSLENGTHSEC = 23.310779056686375;
	
	//progress
	public static final float PROGRESS_START = 0.0f;
	public static final float PROGRESS_READING = 0.05f;
	public static final float PROGRESS_POPULATING = 0.15f;
	public static final float PROGRESS_DONE = 1.0f;

}
