/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.alignment;

public enum AlignmentType {
	Alignment, AlignmentByRun
}
