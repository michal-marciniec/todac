package ch.cern.totem.todac.populators.alignment;

class Configuration {
	
	public static final String XML_EXTENSION = ".xml";

	public static final String DOCUMENT_TYPE = "DocumentType";
	public static final String DOCUMENT_TYPE_ALIGNMENT = "AlignmentSequenceDescription";
	public static final String DOCUMENT_TYPE_ALIGNMENT_BY_RUN = "AlignmentDescription";

	public static final String TAG_ROMAN_POT = "rp";
	public static final String TAG_DET = "det";
	public static final String TAG_TIME_INTERVAL = "TimeInterval";

	public static final String ATTRIBUTE_FIRST = "first";
	public static final String ATTRIBUTE_LAST = "last";
	public static final String ATTRIBUTE_ID = "id";

	public static final int MAXIMUM_LABEL_LENGTH = 50;
	
	public static final String ID_VALUE_PREFIX = "0000";
	public static final int ID_VALUE_LENGTH = 4;

	public static final String WINDOWS_SEPARATOR = "\\";
	public static final String UNIX_SEPARATOR = "/";
	public static final String EMPTY_STRING = "";

	public static final int LABEL_PARTS_FROM_FILEPATH = 3;
	
	//progress
	public static final float PROGRESS_START = 0.0f;
	public static final float PROGRESS_READING = 0.05f;
	public static final float PROGRESS_POPULATING = 0.15f;
	public static final float PROGRESS_DONE = 1.0f;
	
}
