/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.alignment;

import ch.cern.totem.todac.commons.utils.progressbar.IProgressStatus;

public enum PopulationProgress implements IProgressStatus {

	START		(Configuration.PROGRESS_START),			// 0%
	READING		(Configuration.PROGRESS_READING),		// 5%
	POPULATING	(Configuration.PROGRESS_POPULATING),	// 15%
	DONE		(Configuration.PROGRESS_DONE);			// 100%
	
	private final float progress;
	
	private PopulationProgress(float percentage) {
		this.progress = percentage;
	}
	
	public float get(){
		return progress;
	}
}
