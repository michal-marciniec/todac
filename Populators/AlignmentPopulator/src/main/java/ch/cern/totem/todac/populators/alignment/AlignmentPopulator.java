package ch.cern.totem.todac.populators.alignment;

import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Pair;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class AlignmentPopulator implements MeasurableProgress {

	private final static Logger logger = Logger.getLogger(AlignmentPopulator.class);

	protected RomanPotManager romanPotManager = null;
	protected String label = null;
	protected Progress progress = null;

	public AlignmentPopulator(RomanPotManager romanPotManager) {
		this.romanPotManager = romanPotManager;
		this.progress = new Progress(PopulationProgress.START);
	}

	public void setLabel(String label) {
		logger.debug("Setting label for all alignments: " + label);
		this.label = label;
	}

	public void saveAlignments(String filepath, int startRun, int endRun) throws TodacException {
		logger.debug("Saving any alignments from file: " + filepath);
		progress.reset();

		progress.set(PopulationProgress.READING);
		FileDescription fileDescription = new FileDescription(filepath);
		saveAlignments(fileDescription, startRun, endRun);

		progress.set(PopulationProgress.DONE);
	}

	protected void saveAlignments(FileDescription fileDescription, int startRun, int endRun) throws TodacException {
		Reader reader = new Reader(fileDescription);
		Pair<List<Alignment>, AlignmentType> alignments = null;

		try {
			alignments = reader.read();
		} catch (Exception e) {
			logger.error("Unable to parse file: " + fileDescription.getFilepath());
			throw new TodacException(ExceptionMessages.FILE_PARSING, e);
		}

		progress.set(PopulationProgress.POPULATING);
		try {
			switch (alignments.getSecond()) {
			case Alignment:
				saveAlignments(alignments.getFirst());
				break;
			case AlignmentByRun:
				saveAlignmentsByRun(alignments.getFirst(), startRun, endRun);
				break;
			}
		} catch (TudasException e) {
			logger.error("Unable to save alignments into the database via RomanPotManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
	}

	protected void saveAlignments(List<Alignment> alignments) throws TudasException {
		float progressLeft = PopulationProgress.DONE.get() - PopulationProgress.POPULATING.get();

		for (Alignment alignment : alignments) {
			logger.debug("Saving single alignment structure via RomanPotManager");
			romanPotManager.saveAlignments(alignment.startTime, alignment.endTime, label == null ? alignment.label : label, alignment.alignments);
			logger.trace("OK");
			progress.add(progressLeft / alignments.size());
		}
	}

	protected void saveAlignmentsByRun(List<Alignment> alignments, int startRun, int endRun) throws TudasException {
		float progressLeft = PopulationProgress.DONE.get() - PopulationProgress.POPULATING.get();

		for (Alignment alignment : alignments) {
			logger.debug("Saving single alignment by run structure via RomanPotManager");
			romanPotManager.saveAlignmentsByRun(startRun, endRun, label == null ? alignment.label : label, alignment.alignments);
			logger.trace("OK");
			progress.add(progressLeft / alignments.size());
		}
	}

	@Override
	public Progress getProgress() {
		return progress;
	}
}
