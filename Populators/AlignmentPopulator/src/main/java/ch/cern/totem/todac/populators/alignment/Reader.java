package ch.cern.totem.todac.populators.alignment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Pair;
import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;

class Reader {

	private final static Logger logger = Logger.getLogger(Reader.class);

	protected FileDescription fileDescription;

	public Reader(FileDescription fileDescription) {
		this.fileDescription = fileDescription;
	}

	public Pair<List<Alignment>, AlignmentType> read() throws IOException, SAXException, ParserConfigurationException, TodacException {
		logger.debug("Reading file: " + fileDescription.getFilepath());

		List<Alignment> alignments = new ArrayList<Alignment>();

		File file = new File(fileDescription.getFilepath());
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		Document doc = builder.parse(file);

		Element xml = (Element) doc.getDocumentElement();
		String documentType = xml.getAttribute(Configuration.DOCUMENT_TYPE);

		logger.debug("XML reading started");
		// AlignmentDescription other*.xml
		if (documentType.equals(Configuration.DOCUMENT_TYPE_ALIGNMENT_BY_RUN)) {
			logger.debug("Recognized document type: alignments by run");
			List<RomanPotAlignment> rplist = new ArrayList<RomanPotAlignment>();

			NodeList rps = doc.getElementsByTagName(Configuration.TAG_ROMAN_POT);
			aggregateTags(rps, rplist);
			NodeList dets = doc.getElementsByTagName(Configuration.TAG_DET);
			aggregateTags(dets, rplist);

			Alignment a = new Alignment();
			a.label = fileDescription.getLabel();
			a.alignments = rplist.toArray(new RomanPotAlignment[1]);
			alignments.add(a);

			logger.debug("File: " + fileDescription.getFilepath() + " read");
			return new Pair<List<Alignment>, AlignmentType>(alignments, AlignmentType.AlignmentByRun);
		}
		// AlignmentSequenceDescription corrections.xml
		else if (documentType.equals(Configuration.DOCUMENT_TYPE_ALIGNMENT)) {
			logger.debug("Recognized document type: alignments (corrections)");
			NodeList timeIntervals = doc.getElementsByTagName(Configuration.TAG_TIME_INTERVAL);
			for (int i = 0; i < timeIntervals.getLength(); ++i) {
				Node intervalNode = timeIntervals.item(i);

				Alignment alignment = new Alignment();
				List<RomanPotAlignment> list = new ArrayList<RomanPotAlignment>();

				if (intervalNode.getNodeType() == Node.ELEMENT_NODE) {
					Element elementInterval = (Element) intervalNode;

					String firstStr = elementInterval.getAttribute(Configuration.ATTRIBUTE_FIRST);
					String lastStr = elementInterval.getAttribute(Configuration.ATTRIBUTE_LAST);
					alignment.startTime = Long.parseLong(firstStr);
					alignment.endTime = Long.parseLong(lastStr);

					NodeList dets = elementInterval.getElementsByTagName(Configuration.TAG_DET);
					aggregateTags(dets, list);
				}

				alignment.alignments = list.toArray(new RomanPotAlignment[1]);
				alignment.label = fileDescription.getLabel();
				alignments.add(alignment);
			}

			logger.debug("File: " + fileDescription.getFilepath() + " read");
			return new Pair<List<Alignment>, AlignmentType>(alignments, AlignmentType.Alignment);
		}

		throw new TodacException(ExceptionMessages.UNKNOWN_FILE_FORMAT, documentType);
	}

	void aggregateTags(NodeList dets, List<RomanPotAlignment> rplist) throws TodacException {
		for (int i = 0; i < dets.getLength(); ++i) {
			Node det = dets.item(i);
			if (det.getNodeType() == Node.ELEMENT_NODE) {
				Element elementDet = (Element) det;

				Map<String, Double> alignmentValues = new HashMap<String, Double>();
				String id = null;

				NamedNodeMap map = elementDet.getAttributes();
				for (int attribute = 0; attribute < map.getLength(); ++attribute) {
					String name = map.item(attribute).getNodeName();
					if (name.equals(Configuration.ATTRIBUTE_ID)) {
						id = map.getNamedItem(name).getNodeValue().trim();
						id = Configuration.ID_VALUE_PREFIX + id;
						id = id.substring(id.length() - Configuration.ID_VALUE_LENGTH);
					} else {
						alignmentValues.put(name, Double.parseDouble(map.getNamedItem(name).getNodeValue()));
					}
				}

				if (id == null) {
					throw new TodacException(ExceptionMessages.FILE_PARSING, "Attribute '" + Configuration.ATTRIBUTE_ID + "' is missing");
				}

				RomanPotAlignment rp = new RomanPotAlignment(id, alignmentValues);
				rplist.add(rp);
			}
		}
	}

}
