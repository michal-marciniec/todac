package ch.cern.totem.todac.populators.alignment;

import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;

class Alignment {

	public long startTime;
	public long endTime;
	public String label;
	public RomanPotAlignment[] alignments;
	
}
