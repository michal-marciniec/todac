package ch.cern.totem.todac.populators.alignment;

import org.apache.log4j.Logger;

class FileDescription {

	private final static Logger logger = Logger.getLogger(FileDescription.class);
	
	protected String filepath;

	public FileDescription(String filepath) {
		this.filepath = filepath;
	}

	public String getFilepath() {
		return filepath;
	}

	public String getLabel() {
		logger.debug("Generating alignment label for file: " + filepath);
		
		String[] parts = filepath.replace(Configuration.WINDOWS_SEPARATOR,
				Configuration.UNIX_SEPARATOR).split(Configuration.UNIX_SEPARATOR);

		String label = Configuration.EMPTY_STRING;
		int startIndex = 0;
		
		if(parts.length - Configuration.LABEL_PARTS_FROM_FILEPATH >= 0){
			startIndex = parts.length - Configuration.LABEL_PARTS_FROM_FILEPATH;
		}
		else{
			logger.warn("Unable to generate proper label for file: " + filepath);
		}
		
		for (int i = startIndex; i < parts.length; ++i) {
			label += parts[i] + Configuration.UNIX_SEPARATOR;
		}
		label = label.replace(
					Configuration.XML_EXTENSION + Configuration.UNIX_SEPARATOR,
					Configuration.EMPTY_STRING
				);
		
		if(label.length() > Configuration.MAXIMUM_LABEL_LENGTH){
			logger.warn("Extracted label '" + label + "' is too long");
			label = label.substring(label.length() - Configuration.MAXIMUM_LABEL_LENGTH);
		}
		
		logger.debug("Alignment label for file: " + filepath + " set to: " + label);
		return label;
	}

}
