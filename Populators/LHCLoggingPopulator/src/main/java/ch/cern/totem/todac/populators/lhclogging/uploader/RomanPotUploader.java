package ch.cern.totem.todac.populators.lhclogging.uploader;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesDataSet;
import cern.accdm.timeseries.access.domain.timeseriesdata.spi.TimeseriesDataSetImpl;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.uploader.inserters.AbstractMeasurementInserter;
import ch.cern.totem.todac.populators.lhclogging.uploader.inserters.RomanPotInserter;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class RomanPotUploader implements LHCLoggingDataUploader {
	
	private static Logger logger = Logger.getLogger(RomanPotUploader.class);
	
	protected DatabaseAccessProvider serviceProvider;
	protected RomanPotManager rpManager;

	public RomanPotUploader(DatabaseAccessProvider serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	@Override
	public void initialize() throws TodacException {
		try {
			rpManager = serviceProvider.getRomanPotManager();
		} catch (TudasException e) {
			logger.error("Unable to get instance of RomanPotManager");
			throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
	}

	@Override
	public void processAndInsertMeasurement(Variable variable, TimeseriesDataSet timeseriesDataSet, long measurementEndTime) throws TodacException {
		logger.debug("Inserting measurements for variable `" + variable.getVariableName() + "`");
		TimeseriesDataSetImpl timeseriesDS = (TimeseriesDataSetImpl) timeseriesDataSet;
		AbstractMeasurementInserter rpInserter = new RomanPotInserter(rpManager);

		rpInserter.processAndPopulate(variable, timeseriesDS, measurementEndTime);
	}
}
