/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesDataSet;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.TimestampFetcher;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.lhclogging.configuration.Status;
import ch.cern.totem.todac.populators.lhclogging.downloader.LHCLoggingDownloader;
import ch.cern.totem.todac.populators.lhclogging.downloader.MeasurementDownloader;
import ch.cern.totem.todac.populators.lhclogging.measurements.MeasurementDistinguisher;
import ch.cern.totem.todac.populators.lhclogging.measurements.MeasurementReader;
import ch.cern.totem.todac.populators.lhclogging.measurements.MeasurementType;
import ch.cern.totem.todac.populators.lhclogging.uploader.GeneralMeasurementUploader;
import ch.cern.totem.todac.populators.lhclogging.uploader.LHCLoggingDataUploader;
import ch.cern.totem.todac.populators.lhclogging.uploader.RomanPotUploader;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

public class LHCLoggingPopulator implements MeasurableProgress {

    private static final Logger logger = Logger.getLogger(LHCLoggingPopulator.class);
    protected DatabaseAccessProvider databaseAccessProvider;
    protected MeasurementDistinguisher measurementDistinguisher;
    protected LHCLoggingDownloader lhcLoggingDownloader;
    protected Map<Integer, Progress> progressByRunMap;

    public LHCLoggingPopulator(DatabaseAccessProvider databaseAccessProvider) {
        this.databaseAccessProvider = databaseAccessProvider;
        this.measurementDistinguisher = new MeasurementDistinguisher();
        this.lhcLoggingDownloader = new LHCLoggingDownloader(databaseAccessProvider, measurementDistinguisher);
    }

    private static InputStream getRPInputStream() {
        return LHCLoggingPopulator.class.getResourceAsStream("/romanpotmeasurements.txt");
    }

    private static InputStream getGMInputStream() {
        return LHCLoggingPopulator.class.getResourceAsStream("/generalmeasurements.txt");
    }

    public static Set<String> getDefaultMeasurements() throws TodacException {
        final MeasurementDistinguisher distinguisher = new MeasurementDistinguisher();
        distinguisher.loadMeasurements(getGMInputStream(), getRPInputStream());
        Set<String> defaultSet = distinguisher.getAllMeasurements().keySet();

        return defaultSet;
    }

    public void initialize() throws TodacException {
        logger.debug("Initializing " + LHCLoggingPopulator.class.getName());
        measurementDistinguisher.loadMeasurements(getGMInputStream(), getRPInputStream());
        initializeProgressMap(0, 0);
    }

    public void populateByRun(int startRun, int endRun, String variableSetPath) throws TodacException {
        logger.debug("Populate by run: " + startRun + " - " + endRun + " for all measurements in " + variableSetPath);

        MeasurementReader measurementReader = new MeasurementReader();
        measurementReader.loadMeasurements(variableSetPath);
        List<String> measurements = measurementReader.getAllMeasurements();
        for (String measurementName : measurements) {
            populateByRun(measurementName, startRun, endRun);
        }
    }

    public void populateByRun(int startRun, int endRun) throws TodacException {
        logger.debug("Populate by run: " + startRun + " - " + endRun + " for default measurements");

        Set<String> measurements = measurementDistinguisher.getAllMeasurements().keySet();
        for (String measurementName : measurements) {
            populateByRun(measurementName, startRun, endRun);
        }
    }

    public void populateByRun(String measurementName, int startRun, int endRun) throws TodacException {
        logger.debug("Populate by run: " + measurementName + " : " + startRun + " - " + endRun);
        Validator.validateInterval(startRun, endRun);
        initializeProgressMap(startRun, endRun);

        for (int run = startRun; run <= endRun; ++run) {
            populateByRun(measurementName, run);
        }
    }

    protected void populateByRun(String measurementName, int run) throws TodacException {
        logger.debug("Populate by run: " + measurementName + " : " + run);
        Validator.validateNonNegative(run);

        RunInformationManager runInformationManager;
        try {
            runInformationManager = databaseAccessProvider.getRunInformationManager();
        } catch (TudasException e) {
            logger.error("Unable to get instance of RunInformationManager");
            throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
        }

        long startTime = 0, endTime = 0;
        TimestampFetcher timestampFetcher = new TimestampFetcher(runInformationManager);
        startTime = timestampFetcher.getRunStartTimestamp(run);
        endTime = timestampFetcher.getRunEndTimestamp(run);

        Progress runProgress = progressByRunMap.get(run);
        populate(measurementName, startTime, endTime, runProgress);
    }

    protected void populate(String measurementName, long startTime, long endTime, Progress progress) throws TodacException {
        logger.debug("Populating database for variable: " + measurementName);
        MeasurementType measurementType = measurementDistinguisher.distinguish(measurementName);
        progress.set(Status.FETCHING_LDB_VARIABLE);
        Variable variable = lhcLoggingDownloader.getVariable(measurementName);
        progress.set(Status.FETCHING_LDB_TIMESERIES);
        TimeseriesDataSet timeseriesDataSet = lhcLoggingDownloader.downloadTimeseriesDataSet(variable, startTime, endTime);
        if (timeseriesDataSet == null || timeseriesDataSet.size() == 0) {
            progress.set(Status.DONE);
            return;
        }
        long maxTimeStamp = timeseriesDataSet.getMaxStamp().getTime();
        LHCLoggingDataUploader uploader = getLhcLoggingDataUploader(measurementType);
        uploader.initialize();
        progress.set(Status.UPLOADING);
        uploader.processAndInsertMeasurement(variable, timeseriesDataSet, maxTimeStamp);
        progress.set(Status.DONE);
    }

    protected LHCLoggingDataUploader getLhcLoggingDataUploader(MeasurementType measurementType) {
        logger.debug("Matching TUDAS manager to supplied measurementType: " + measurementType.toString());
        switch (measurementType) {
            case GeneralMeasurement:
                logger.debug("GeneralMeasurementUploader chosen");
                return new GeneralMeasurementUploader(databaseAccessProvider);
            case RomanPotMeasurement:
                logger.debug("RomanPotUploader chosen");
                return new RomanPotUploader(databaseAccessProvider);
        }
        return null;
    }

    protected void initializeProgressMap(int startRun, int endRun) {
        progressByRunMap = new HashMap<Integer, Progress>();
        for (int i = startRun; i <= endRun; ++i) {
            progressByRunMap.put(i, new Progress());
        }
    }

    @Override
    public Progress getProgress() {
        if (progressByRunMap.isEmpty()) {
            return new Progress();
        } else {
            float sum = 0.0f;
            for (Progress progress : progressByRunMap.values()) {
                sum += progress.get();
            }
            float average = sum / progressByRunMap.size();

            return new Progress(average);
        }
    }

    public Map<GeneralMeasurementIOV, GeneralMeasurement[]> fetch(String variable, int startRun, int endRun) throws TodacException, TudasException {
        MeasurementDownloader measurementDownloader = new MeasurementDownloader(databaseAccessProvider);
        measurementDownloader.initialize();

        final MeasurementType measurementType = measurementDistinguisher.distinguish(variable);
        switch (measurementType) {
            case RomanPotMeasurement:
                return measurementDownloader.fetchRomanPotPositions(variable, startRun, endRun);
            case GeneralMeasurement:
                return measurementDownloader.fetchGeneralMeasurements(variable, startRun, endRun);
            default:
                return new HashMap<>();
        }
    }

}
