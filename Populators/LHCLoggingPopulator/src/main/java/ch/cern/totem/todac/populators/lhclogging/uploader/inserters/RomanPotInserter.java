package ch.cern.totem.todac.populators.lhclogging.uploader.inserters;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesData;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class RomanPotInserter extends AbstractMeasurementInserter {

	private static Logger logger = Logger.getLogger(RomanPotInserter.class);

	protected RomanPotManager rpManager;

	public RomanPotInserter(RomanPotManager rpManager) {
		this.rpManager = rpManager;
	}

	@Override
	protected void createAndPopulateMeasurement(Variable variable, TimeseriesData timeseriesData, long endTime) throws TodacException {
		logger.debug("Creating RomanPotMeasurement for variable: " + variable.getVariableName());
		long startTime = timeseriesData.getStamp().getTime();
		String label = variable.getVariableName();
		double value;
		try {
			value = timeseriesData.getDoubleValue();
		} catch (NoSuchMethodException e) {
			logger.error("Unable to retrieve double value from provided TimeseriesData");
			throw new TodacException(ExceptionMessages.LHC_LOGGING_DB_NO_SUCH_METHOD, e);
		}

		try {
			logger.debug("Saving roman pot position for variable: " + variable.getVariableName());
			rpManager.saveRomanPotPosition(startTime, endTime, label, value);
		} catch (TudasException e) {
			logger.error("Unable to save roman pot position via RomanPotManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
		logger.trace("OK");
	}

}
