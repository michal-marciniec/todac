package ch.cern.totem.todac.populators.lhclogging.uploader.inserters;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesData;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.clients.java.data.manager.GeneralMeasurementManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

public class VectorGeneralMeasurementInserter extends AbstractGeneralMeasurementInserter {

	private static Logger logger = Logger.getLogger(VectorGeneralMeasurementInserter.class);
	
	protected GeneralMeasurementManager gmManager;

	public VectorGeneralMeasurementInserter(GeneralMeasurementManager gmManager) {
		this.gmManager = gmManager;
	}

	@Override
	protected void createAndPopulateMeasurement(Variable variable, TimeseriesData timeseriesData, long endTime) throws TodacException {
		logger.debug("Creating GeneralMeasurementVector for variable: " + variable.getVariableName());
		GeneralMeasurement[] gmVector = createGeneralMeasurementVector(variable, timeseriesData);
		GeneralMeasurementIOV iov = createGeneralMeasurementIOV(variable, timeseriesData, endTime);

		try {
			logger.debug("Saving general measurement vector for variable: " + variable.getVariableName());
			gmManager.saveGeneralMeasurementVector(iov, gmVector);
		} catch (TudasException e) {
			logger.error("Unable to save general measurement vector via GeneralMeasurementManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
		logger.trace("OK");
	}
}
