/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.downloader;

import java.sql.Timestamp;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.client.FillDataController;
import cern.accdm.timeseries.access.client.TimeseriesDataController;
import cern.accdm.timeseries.access.client.VariableController;
import cern.accdm.timeseries.access.domain.filldata.LHCFill;
import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.metadata.VariableSet;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesDataSet;
import cern.accdm.timeseries.access.exceptions.DataAccessException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.mexer.Classmexer;
import ch.cern.totem.todac.populators.lhclogging.configuration.Configuration;
import ch.cern.totem.todac.populators.lhclogging.exceptions.ExceptionCodeConverter;
import ch.cern.totem.todac.populators.lhclogging.measurements.MeasurementDistinguisher;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;

public class LHCLoggingDownloader {

	private static Logger logger = Logger.getLogger(LHCLoggingDownloader.class);
	
	protected DatabaseAccessProvider databaseAccessProvider;
	protected MeasurementDistinguisher measurementDistinguisher;
	protected FillDataController fillDataController;
	protected TimeseriesDataController timeseriesDataController;
	private VariableController variableController;

	public LHCLoggingDownloader(DatabaseAccessProvider databaseAccessProvider,
			MeasurementDistinguisher measurementDistinguisher) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.measurementDistinguisher = measurementDistinguisher;
		this.fillDataController = new FillDataController(
				Configuration.LHC_LOGGING_API_APPLICATION_NAME,
				Configuration.LHC_LOGGING_API_CLIENT_NAME);
		this.timeseriesDataController = new TimeseriesDataController(
				Configuration.DATASOURCE_PREFERENCES, 
				Configuration.LHC_LOGGING_API_APPLICATION_NAME,
				Configuration.LHC_LOGGING_API_CLIENT_NAME);
		this.variableController = new VariableController(
				Configuration.LHC_LOGGING_API_APPLICATION_NAME,
				Configuration.LHC_LOGGING_API_CLIENT_NAME);
	}

	public TimeseriesDataSet downloadTimeseriesDataSet(Variable variable, long startTime, long endTime)
			throws TodacException {
		TimeseriesDataSet timeseriesDataSet = null;
		try{
			logger.debug("Downloading TimeseriesDataSet: " + variable.getVariableName() + ", " + new Timestamp(startTime) + " - " + new Timestamp(endTime));
			timeseriesDataSet = timeseriesDataController.getDataSet(variable, new Timestamp(startTime), new Timestamp(endTime));
			Classmexer.measure(timeseriesDataSet, "TimeSeriesDataSet");
		}
		catch(DataAccessException dataAccessException){
			int code = ExceptionCodeConverter.convert(dataAccessException);
			if(code == Configuration.LHC_NO_DATA_EXCEPTION_CODE){
				logger.warn("No data returned from the LHC Logging Database\n" +
						dataAccessException.getMessage());
			}
			else{
				logger.error("Unable to fetch TimeseriesDataSet data from LHC Logging Database for variable name: " + variable.getVariableName());
				throw new TodacException(ExceptionMessages.LHC_LOGGING_DB, dataAccessException);
			}
		}
		finally{
			if(timeseriesDataSet == null || timeseriesDataSet.size() == 0){
				logger.info("No entries in the downloaded TimeseriesDataSet.");
			}
			else{
				logger.debug("Downloaded TimeseriesDataSet contains " + timeseriesDataSet.size() + " entries.");
			}
		}
		
		return timeseriesDataSet;
	}
	
	public Variable getVariable(String measurementName) throws TodacException {
		VariableSet variableSet = null;
		try {
			logger.info("Downloading variable information: " + measurementName);
			variableSet = variableController.getVariables(measurementName, Configuration.VARIABLE_DATA_TYPE);
		} catch (DataAccessException e) {
			logger.error("Unable to fetch Variable data from LHC Logging Database for variable name: " + measurementName);
			throw new TodacException(ExceptionMessages.LHC_LOGGING_DB, e);
		}
		if(!variableSet.containsVariable(measurementName)) throw new InvalidArgumentException(
				ExceptionMessages.LHC_LOGGING_DB_NO_SUCH_VARIABLE, 
				measurementName);
		Variable variable = variableSet.getVariable(measurementName);
		Classmexer.measure(variable, "Variable");
		return variable;
	}

	public LHCFill getFill(int fill) throws TodacException {
		LHCFill lhcFill = null;
		try {
			logger.info("Downloading LHCFill information for fill: " + fill);
			lhcFill = fillDataController.getLHCFill(fill);
		} catch (DataAccessException e) {
			logger.error("Unable to fetch LHCFill data from LHC Logging Database for fill number: ");
			throw new TodacException(ExceptionMessages.LHC_LOGGING_DB, e);
		}
		Classmexer.measure(lhcFill, "LHCFill");
		return lhcFill;
	}

}
