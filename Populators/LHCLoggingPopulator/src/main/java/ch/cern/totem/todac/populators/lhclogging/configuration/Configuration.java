/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.configuration;

import cern.accdm.timeseries.access.domain.constants.VariableDataType;
import cern.accdm.timeseries.access.domain.datasource.DatasourcePreferences;

public class Configuration {

	public static final String LHC_LOGGING_API_CLIENT_NAME = "totem";
	public static final String LHC_LOGGING_API_APPLICATION_NAME = "dbpop";
	
	public static final String XML_GROUP_TAG = "group";
	public static final String XML_GROUP_NAME_ATTRIBUTE = "name";
	public static final String XML_MEASUREMENT_TAG = "measurement";

	public static final DatasourcePreferences DATASOURCE_PREFERENCES = DatasourcePreferences.LHCLOG_PRO_ONLY;
	public static final VariableDataType VARIABLE_DATA_TYPE = VariableDataType.ALL;
	
	public static final String LHC_EMPTY_STRING_REPLACEMENT = "Unknown";
	public static final String LHC_EXCEPTION_CODE_REGEXP = "^(\\d+)";
	public static final int LHC_NO_DATA_EXCEPTION_CODE = 6;

}
