package ch.cern.totem.todac.populators.lhclogging.downloader;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.TimestampFetcher;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.GeneralMeasurementManager;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

import java.util.HashMap;
import java.util.Map;

public class MeasurementDownloader {

    protected DatabaseAccessProvider databaseAccessProvider;
    protected TimestampFetcher timestampFetcher;

    public MeasurementDownloader(DatabaseAccessProvider databaseAccessProvider) {
        this.databaseAccessProvider = databaseAccessProvider;
    }

    public void initialize() throws TudasException {
        RunInformationManager runInformationManager = databaseAccessProvider.getRunInformationManager();
        timestampFetcher = new TimestampFetcher(runInformationManager);
    }

    public Map<GeneralMeasurementIOV, GeneralMeasurement[]> fetchGeneralMeasurements(String variable, int startRun, int endRun) throws TudasException, TodacException {
        long startTime = timestampFetcher.getRunStartTimestamp(startRun);
        long endTime = timestampFetcher.getRunEndTimestamp(endRun);

        GeneralMeasurementManager gmManager = databaseAccessProvider.getGeneralMeasurementManager();
        try { // single general measurement
            final Map<GeneralMeasurementIOV, GeneralMeasurement> singleMap = gmManager.loadGeneralMeasurements(startTime, endTime, variable);
            Map<GeneralMeasurementIOV, GeneralMeasurement[]> interfaceMatchingMap = new HashMap<>(singleMap.size());
            for (Map.Entry<GeneralMeasurementIOV, GeneralMeasurement> singleEntry : singleMap.entrySet()) {
                interfaceMatchingMap.put(singleEntry.getKey(), new GeneralMeasurement[]{singleEntry.getValue()});
            }
            return interfaceMatchingMap;
        } catch (TudasException e) { // vector general measurement
            final Map<GeneralMeasurementIOV, GeneralMeasurement[]> map = gmManager.loadGeneralMeasurementVectors(startTime, endTime, variable);
            return map;
        }
    }

    public Map<GeneralMeasurementIOV, GeneralMeasurement[]> fetchRomanPotPositions(String variable, int startRun, int endRun) throws TudasException, TodacException {
        RomanPotManager rpManager = databaseAccessProvider.getRomanPotManager();
        Map<GeneralMeasurementIOV, GeneralMeasurement[]> interfaceMatchingMap = new HashMap<>();
        for (int run = startRun; run <= endRun; ++run) {
            long startTime = timestampFetcher.getRunStartTimestamp(run);
            final double value = rpManager.loadRomanPotPosition(startTime, variable);
            interfaceMatchingMap.put(new GeneralMeasurementIOV(startTime, startTime, variable), new GeneralMeasurement[]{new GeneralMeasurement("", "", value)});
        }
        return interfaceMatchingMap;
    }
}
