/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.exceptions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.exceptions.DataAccessException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.configuration.Configuration;

public class ExceptionCodeConverter {
	
	private final static Logger logger = Logger.getLogger(ExceptionCodeConverter.class);
	
	public static int convert(DataAccessException dataAccessException) throws TodacException {
		logger.debug("Converting DataAccessException");
		String message = dataAccessException.getMessage();
		final Pattern p = Pattern.compile(Configuration.LHC_EXCEPTION_CODE_REGEXP);
		Matcher m = p.matcher(message);
		if (m.find()) {
			String result = m.group();
			logger.debug("Extracted exception code is: " + result);
		    return Integer.parseInt(result);
		}
		throw new TodacException(ExceptionMessages.LHC_LOGGING_DB_EXCEPTION_CONVERSION, dataAccessException);
	}
}
