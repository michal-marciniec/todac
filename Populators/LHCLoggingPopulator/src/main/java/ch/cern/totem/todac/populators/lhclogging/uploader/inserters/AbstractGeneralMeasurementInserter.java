package ch.cern.totem.todac.populators.lhclogging.uploader.inserters;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesData;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.validator.ValidatorReplacer;
import ch.cern.totem.todac.populators.lhclogging.configuration.Configuration;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

public abstract class AbstractGeneralMeasurementInserter extends AbstractMeasurementInserter {

	private static Logger logger = Logger.getLogger(AbstractGeneralMeasurementInserter.class);

	protected GeneralMeasurement[] createGeneralMeasurementVector(Variable variable, TimeseriesData timeseriesData) throws TodacException {
		logger.debug("Processing TimeseriesDataSet for variable: " + variable.getVariableName());
		String description = variable.getVariableName(); // DBPop: variable.getVariableName();
		description = ValidatorReplacer.replaceNullOrEmpty(description, Configuration.LHC_EMPTY_STRING_REPLACEMENT);
		String unit = variable.getUnit();
		unit = ValidatorReplacer.replaceNullOrEmpty(unit, Configuration.LHC_EMPTY_STRING_REPLACEMENT);

		double[] values;
		try {
			values = timeseriesData.getDoubleValues();
		} catch (NoSuchMethodException e) {
			logger.error("Unable to retrieve vector of double values from provided TimeseriesData");
			throw new TodacException(ExceptionMessages.LHC_LOGGING_DB_NO_SUCH_METHOD, e);
		}

		GeneralMeasurement[] generalMeasurements = new GeneralMeasurement[values.length];
		for (int j = 0; j < values.length; j++) {
			generalMeasurements[j] = new GeneralMeasurement(description, unit, values[j]);
		}

		return generalMeasurements;
	}

	protected GeneralMeasurement createGeneralMeasurement(Variable variable, TimeseriesData timeseriesData) throws TodacException {
		logger.debug("Processing TimeseriesDataSet for variable: " + variable.getVariableName());
		String description = variable.getDescription(); // DBPop: variable.getVariableName();
		String unit = variable.getUnit();
		double value;
		try {
			value = timeseriesData.getDoubleValue();
		} catch (NoSuchMethodException e) {
			logger.error("Unable to retrieve double value from provided TimeseriesData");
			throw new TodacException(ExceptionMessages.LHC_LOGGING_DB_NO_SUCH_METHOD, e);
		}

		GeneralMeasurement gm = new GeneralMeasurement(description, unit, value);
		return gm;
	}

	protected GeneralMeasurementIOV createGeneralMeasurementIOV(Variable variable, TimeseriesData timeseriesData, long timeseriesEndTime) {
		logger.debug("Processing TimeseriesDataSet for variable: " + variable.getVariableName());
		long startTime = timeseriesData.getStamp().getTime();
		String typeName = variable.getVariableName(); // DBPop: hardcoded "he"
		// String typeName = variable.getVariableType().name(); ?

		GeneralMeasurementIOV iov = new GeneralMeasurementIOV(startTime, timeseriesEndTime, typeName);
		return iov;
	}
}
