package ch.cern.totem.todac.populators.lhclogging.uploader;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesDataSet;
import ch.cern.totem.todac.commons.exceptions.TodacException;

public interface LHCLoggingDataUploader {
	
	void initialize() throws TodacException;
	void processAndInsertMeasurement(Variable variable, TimeseriesDataSet timeseriesDataSet, long measurementEndTime) throws TodacException;

}
