package ch.cern.totem.todac.populators.lhclogging.uploader;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.constants.VariableDataType;
import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesDataSet;
import cern.accdm.timeseries.access.domain.timeseriesdata.spi.TimeseriesDataSetImpl;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.uploader.inserters.AbstractGeneralMeasurementInserter;
import ch.cern.totem.todac.populators.lhclogging.uploader.inserters.SingleGeneralMeasurementInserter;
import ch.cern.totem.todac.populators.lhclogging.uploader.inserters.VectorGeneralMeasurementInserter;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.GeneralMeasurementManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class GeneralMeasurementUploader implements LHCLoggingDataUploader {
	
	private static Logger logger = Logger.getLogger(GeneralMeasurementUploader.class);
	
	protected DatabaseAccessProvider serviceProvider;
	protected GeneralMeasurementManager gmManager;

	public GeneralMeasurementUploader(DatabaseAccessProvider serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	@Override
	public void initialize() throws TodacException {
		try {
			gmManager = serviceProvider.getGeneralMeasurementManager();
		} catch (TudasException e) {
			logger.error("Unable to get instance of GeneralMeasurementManager");
			throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
	}

	@Override
	public void processAndInsertMeasurement(Variable variable, TimeseriesDataSet timeseriesDataSet, long measurementEndTime) throws TodacException {
		logger.debug("Inserting measurements for variable: " + variable.getVariableName());
		TimeseriesDataSetImpl timeseriesDS = (TimeseriesDataSetImpl) timeseriesDataSet;

		VariableDataType dataType = variable.getVariableDataType();
		AbstractGeneralMeasurementInserter gmInserter = getInserterByDataType(dataType);

		gmInserter.processAndPopulate(variable, timeseriesDS, measurementEndTime);
	}

	protected AbstractGeneralMeasurementInserter getInserterByDataType(VariableDataType dataType) throws TodacException {
		switch (dataType) {
			case NUMERIC:
				logger.debug("Returning SingleGeneralMeasurementInserter");
				return new SingleGeneralMeasurementInserter(gmManager);
			case VECTOR_NUMERIC:
				logger.debug("Returning VectorGeneralMeasurementInserter");
				return new VectorGeneralMeasurementInserter(gmManager);
			default:
				logger.error("Unknown variable type: " + dataType.toString());
				throw new TodacException(ExceptionMessages.LHC_LOGGING_DB_UNKNOWN_VARIABLE_TYPE, dataType.toString());
		}
	}
}
