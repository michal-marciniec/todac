/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.exceptions;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessage;
import ch.cern.totem.todac.commons.exceptions.ValidatorException;

public class UnsupportedVariableException extends ValidatorException {
	private static final long serialVersionUID = 1L;

	public UnsupportedVariableException(ExceptionMessage exceptionMessage, String additionalInformation) {
		super(exceptionMessage, additionalInformation);
	}

}
