/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.measurements;

public enum MeasurementType {

	RomanPotMeasurement, GeneralMeasurement

}
