/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.measurements;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.configuration.Configuration;

public class MeasurementReader {

private static Logger logger = Logger.getLogger(MeasurementDistinguisher.class);
	
	protected List<String> measurements;

	public MeasurementReader() {
		this.measurements = new LinkedList<String>();
	}

	public void loadMeasurements(String filepath) throws TodacException {
		measurements.clear();
		File file = new File(filepath);
		logger.debug("Loading measurements from: " + file.getAbsolutePath());
		
		DocumentBuilder documentBuilder = null;
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			logger.error("Unable to create Java XML Document");
			throw new TodacException(ExceptionMessages.FILE_ACCESS, e);
		}
		
		Document document = null;
		try {
			document = documentBuilder.parse(file);
		} catch (Exception e) {
			logger.error("Unable to parse file: " + filepath);
			throw new TodacException(ExceptionMessages.FILE_PARSING, e);
		}
		
		logger.debug("XML reading started");
		Element xml = document.getDocumentElement();
		NodeList measurementNodes = xml.getElementsByTagName(Configuration.XML_MEASUREMENT_TAG);
		for(int measurementIndex = 0; measurementIndex < measurementNodes.getLength(); ++measurementIndex){
			Node measurement = measurementNodes.item(measurementIndex);
			String measurementName = measurement.getTextContent();
			if(!measurements.contains(measurementName)){
				measurements.add(measurementName);
			}
			else{
				logger.warn("Duplicated entry in measurements file: " + measurementName);
			}
		}
		
		logger.info(measurements.size() + " measurements loaded properly from file: " + filepath);
	}
	
	public List<String> getAllMeasurements(){
		return measurements;
	}
	
}
