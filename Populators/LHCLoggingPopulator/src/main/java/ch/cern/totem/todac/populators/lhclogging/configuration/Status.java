/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.configuration;

import ch.cern.totem.todac.commons.utils.progressbar.IProgressStatus;

public enum Status implements IProgressStatus {
	START(0.0f), //
	FETCHING_LDB_VARIABLE(0.05f), //
	FETCHING_LDB_TIMESERIES(0.30f), //
	UPLOADING(0.75f), //
	DONE(1.0f); //

	private final float progress;

	Status(float percentage) {
		this.progress = percentage;
	}

	@Override
	public float get() {
		return progress;
	}
}
