package ch.cern.totem.todac.populators.lhclogging.uploader.inserters;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesData;
import cern.accdm.timeseries.access.domain.timeseriesdata.spi.TimeseriesDataSetImpl;
import ch.cern.totem.todac.commons.exceptions.TodacException;

public abstract class AbstractMeasurementInserter {
	
	private static Logger logger = Logger.getLogger(AbstractMeasurementInserter.class);
	
	protected abstract void createAndPopulateMeasurement(Variable variable, TimeseriesData timeseriesData, long endTime) throws TodacException;

	public void processAndPopulate(Variable variable, TimeseriesDataSetImpl timeseriesDataSet, long measurementEndTime) throws TodacException {		
		for (int i = 0; i < timeseriesDataSet.size() - 1; ++i) {
			logger.debug("Processing TimeseriesData " +  (i + 1) + "/" + timeseriesDataSet.size() + " for variable `" + variable.getVariableName() + "`");
			TimeseriesData singleData = timeseriesDataSet.get(i);
			TimeseriesData next = timeseriesDataSet.get(i + 1);
			long endTime = next.getStamp().getTime();

			createAndPopulateMeasurement(variable, singleData, endTime);
		}

		logger.debug("Processing TimeseriesData " +  timeseriesDataSet.size() + "/" + timeseriesDataSet.size() + " for variable `" + variable.getVariableName() + "`");
		int dataSetSize = timeseriesDataSet.size();
		TimeseriesData lastTimeseriesData = timeseriesDataSet.get(dataSetSize - 1);

		createAndPopulateMeasurement(variable, lastTimeseriesData, measurementEndTime);
	}
}
