package ch.cern.totem.todac.populators.lhclogging.uploader.inserters;

import org.apache.log4j.Logger;

import cern.accdm.timeseries.access.domain.metadata.Variable;
import cern.accdm.timeseries.access.domain.timeseriesdata.TimeseriesData;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.clients.java.data.manager.GeneralMeasurementManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

public class SingleGeneralMeasurementInserter extends AbstractGeneralMeasurementInserter {

	private static Logger logger = Logger.getLogger(SingleGeneralMeasurementInserter.class);
	
	protected GeneralMeasurementManager gmManager;

	public SingleGeneralMeasurementInserter(GeneralMeasurementManager gmManager) {
		this.gmManager = gmManager;
	}

	@Override
	protected void createAndPopulateMeasurement(Variable variable, TimeseriesData timeseriesData, long endTime) throws TodacException {
		logger.debug("Creating GeneralMeasurement for variable: " + variable.getVariableName());
		GeneralMeasurement gm = this.createGeneralMeasurement(variable, timeseriesData);
		GeneralMeasurementIOV iov = this.createGeneralMeasurementIOV(variable, timeseriesData, endTime);

		try {
			logger.debug("Saving general measurement for variable: " + variable.getVariableName());
			gmManager.saveGeneralMeasurement(iov, gm);
		} catch (TudasException e) {
			logger.error("Unable to save general measurement via GeneralMeasurementsManager");
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, e);
		}
		logger.trace("OK");
	}
}
