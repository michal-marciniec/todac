/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.populators.lhclogging.measurements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.exceptions.UnsupportedVariableException;

public class MeasurementDistinguisher {

	private static Logger logger = Logger.getLogger(MeasurementDistinguisher.class);
	
	protected Map<String, MeasurementType> measurements;

	public MeasurementDistinguisher() {
		this.measurements = new HashMap<String, MeasurementType>();
	}

	public void loadMeasurements(InputStream gmInputStream, InputStream rpInputStream) throws TodacException {

		measurements.clear();
		logger.debug("Loading measurements");
		try {
			String line = null;
			BufferedReader gmBufferedReader = new BufferedReader(new InputStreamReader(gmInputStream));
		
			while((line = gmBufferedReader.readLine()) != null){
				measurements.put(line, MeasurementType.GeneralMeasurement);
			}
		
			BufferedReader rpBufferedReader = new BufferedReader(new InputStreamReader(rpInputStream));
			while((line = rpBufferedReader.readLine()) != null){
				measurements.put(line, MeasurementType.RomanPotMeasurement);
			}
		} catch (IOException e) {
			logger.error("Unable to load default measurements");
			throw new TodacException(ExceptionMessages.FILE_PARSING);
		}
		
		logger.info(measurements.size() + " default measurements loaded properly");
	}
	
	public Map<String, MeasurementType> getAllMeasurements(){
		return measurements;
	}

	public MeasurementType distinguish(String variable)
			throws UnsupportedVariableException {
		logger.debug("Trying to resolve `" + variable + "` onto the MeasurementType");
		
		MeasurementType measurementType;
		if (!measurements.containsKey(variable)){
			measurementType = MeasurementType.GeneralMeasurement;
			logger.debug("Unable to resolve `" + variable + "`. Using default property: " + measurementType.toString());
			//throw new UnsupportedVariableException(variable);
		}
		else{
			measurementType = measurements.get(variable);
			logger.debug("Resolved `" + variable + "` onto `" + measurementType.toString() + "` property");
		}
		
		return measurementType;
	}

}
