/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.geometry;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

import ch.cern.totem.todac.commons.exceptions.ArgumentFormatException;
import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.TimestampFetcher;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.Progress;
import ch.cern.totem.todac.populators.geometry.commons.Convert;
import ch.cern.totem.todac.populators.geometry.commons.Parser;
import ch.cern.totem.todac.populators.geometry.commons.Status;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class GeometryPopulator implements MeasurableProgress {
	private final static Logger logger = Logger.getLogger(GeometryPopulator.class);

	protected final static int MAX_LABEL_LEN = 50;

	protected DatabaseAccessProvider databaseAccessProvider;
	protected RomanPotManager romanPotManager;
	protected RunInformationManager runInfoManager;
	protected TimestampFetcher timestampFetcher;
	protected Progress progress;

	public GeometryPopulator(DatabaseAccessProvider databaseAccessProvider) {
		this.databaseAccessProvider = databaseAccessProvider;
	}

	public void initialize() throws DatabaseException {
		logger.debug("Initialization");
		try {
			progress = new Progress();
			romanPotManager = databaseAccessProvider.getRomanPotManager();
			runInfoManager = databaseAccessProvider.getRunInformationManager();
			timestampFetcher = new TimestampFetcher(runInfoManager);
		} catch (TudasException e) {
			throw new DatabaseException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
	}

	public int parseAndPopulate(String xmlGeometrySource, int startRun, int endRun, String label) throws TodacException {
		if (label.length() > MAX_LABEL_LEN) {
			throw new ArgumentFormatException(ExceptionMessages.LABEL_TOO_LONG, "Provided: " + label + ", max length: " + MAX_LABEL_LEN);
		}

		progress.set(Status.PARSING_XML);
		Map<String, Double> geometry = parse(xmlGeometrySource);

		progress.set(Status.FETCHING_TIMESTAMPS);
		long startTimestamp = timestampFetcher.getRunStartTimestamp(startRun);
		long endTimestamp = timestampFetcher.getRunEndTimestamp(endRun);

		try {
			logger.info("Populating database with geometry data from file: " + xmlGeometrySource + " using label: " + label);
			progress.set(Status.UPLOADING);
			int geometryVersion = romanPotManager.saveGeometry(startTimestamp, endTimestamp, label, geometry);
			progress.set(Status.DONE);
			return geometryVersion;
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_MANAGER_SAVE, xmlGeometrySource, e);
		}
	}

	public int parseAndPopulate(String xmlGeometrySource, int startRun, int endRun) throws TodacException {
		String label = Convert.filenameToLabel(xmlGeometrySource, MAX_LABEL_LEN);
		return parseAndPopulate(xmlGeometrySource, startRun, endRun, label);
	}

	protected Map<String, Double> parse(String xmlGeometrySource) throws TodacException {
		logger.info("Parsing xml geometry source file: " + xmlGeometrySource);
		try {
			Parser geometryParser = new Parser();
			Document xmlGeometryDocument = geometryParser.createXmlDocument(xmlGeometrySource);
			Map<String, Double> geometry = geometryParser.parseXmlGeometry(xmlGeometryDocument);
			return geometry;
		} catch (IOException e) {
			throw new TodacException(ExceptionMessages.FILE_ACCESS, xmlGeometrySource, e);
		}
	}

	@Override
	public Progress getProgress() {
		return progress;
	}

}