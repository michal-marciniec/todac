/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.geometry.commons;

public class Constants {
	public final static String MAIN_TAG = "Constant";
	public final static String RPNAME_ATTR = "name";
	public final static String VALUE_ATTR = "value";
}
