/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.geometry.commons;

import ch.cern.totem.todac.commons.utils.progressbar.IProgressStatus;

public enum Status implements IProgressStatus {
	START(0.0f), // 0%
	PARSING_XML(0.05f), // 5%
	FETCHING_TIMESTAMPS(0.55f), // 55%
	UPLOADING(0.6f), // 60%
	DONE(1.0f); // 100%

	private final float progress;

	Status(float percentage) {
		this.progress = percentage;
	}

	@Override
	public float get() {
		return progress;
	}
}
