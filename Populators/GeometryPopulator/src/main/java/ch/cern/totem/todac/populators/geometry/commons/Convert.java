/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.geometry.commons;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 Scheme of conversion RomanPot names to int values recognised by DB.
 Specification's author is Jan Kaspar

 RP_147_Left_Det_Dist_0 --> 000
 RP_147_Left_Det_Dist_1 --> 001
 RP_147_Left_Det_Dist_0 --> 000
 RP_147_Left_Det_Dist_1 --> 001
 RP_147_Left_Det_Dist_2 --> 002
 RP_147_Left_Det_Dist_3 --> 003
 RP_147_Left_Det_Dist_4 --> 004
 RP_147_Left_Det_Dist_5 --> 005

 RP_147_Right_Det_Dist_0 --> 100
 RP_147_Right_Det_Dist_1 --> 101
 RP_147_Right_Det_Dist_2 --> 102
 RP_147_Right_Det_Dist_3 --> 103
 RP_147_Right_Det_Dist_4 --> 104
 RP_147_Right_Det_Dist_5 --> 105

 RP_220_Left_Det_Dist_0 --> 020
 RP_220_Left_Det_Dist_1 --> 021
 RP_220_Left_Det_Dist_2 --> 022
 RP_220_Left_Det_Dist_3 --> 023
 RP_220_Left_Det_Dist_4 --> 024
 RP_220_Left_Det_Dist_5 --> 025

 RP_220_Right_Det_Dist_0 --> 120
 RP_220_Right_Det_Dist_1 --> 121
 RP_220_Right_Det_Dist_2 --> 122
 RP_220_Right_Det_Dist_3 --> 123
 RP_220_Right_Det_Dist_4 --> 124
 RP_220_Right_Det_Dist_5 --> 125
 */

public class Convert {
	private final static Logger logger = Logger.getLogger(Convert.class);

	protected final static String RPNAME_SEPARATOR = "_";

    public static String DbIntToRPName(String dbId) throws InvalidArgumentException {
        StringBuilder sb = new StringBuilder();
        sb.append("RP");
        sb.append(RPNAME_SEPARATOR);
        sb.append(getRPId(dbId));
        sb.append(RPNAME_SEPARATOR);
        sb.append(getSide(dbId));
        sb.append(RPNAME_SEPARATOR);
        sb.append("Det");
        sb.append(RPNAME_SEPARATOR);
        sb.append("Dist");
        sb.append(RPNAME_SEPARATOR);
        sb.append(getRPNo(dbId));

        return sb.toString();
    }

    protected static String getSide(String dbId) throws InvalidArgumentException {
        char firstDigit = dbId.charAt(0);
        if(firstDigit == '1')
            return "Right";
        else if(firstDigit == '0')
            return "Left";
        else
            throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, String.valueOf(dbId));
    }

    protected static String getRPId(String dbId) throws InvalidArgumentException {
        char secondDigit = dbId.charAt(1);
        if(secondDigit == '2')
            return "220";
        else if(secondDigit == '0')
            return "147";
        else
            throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, String.valueOf(dbId));
    }

    protected static String getRPNo(String dbId) throws InvalidArgumentException {
        int thirdDigit = Integer.parseInt(dbId.substring(2));
        if(thirdDigit <= 5)
            return String.valueOf(thirdDigit);
        else
            throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, String.valueOf(dbId));
    }

	// TODO refactor
	public static String RPNameToDBInt(String name) throws InvalidArgumentException {
		int res;
		String[] splitted = name.split(RPNAME_SEPARATOR);

		if (splitted[2].equalsIgnoreCase("Right"))
			res = 100;
		else if (splitted[2].equalsIgnoreCase("Left"))
			res = 0;
		else
			throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, name);

		if (splitted[1].equalsIgnoreCase("220"))
			res += 20;
		else if (splitted[1].equalsIgnoreCase("147"))
			res += 0;
		else
			throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, name);

		int rpNo = Integer.valueOf(splitted[5]);
		if (rpNo >= 0 && rpNo <= 5)
			res += rpNo;
		else
			throw new InvalidArgumentException(ExceptionMessages.RP_NAME_INVALID, name);

		// left padding
		String result = "000" + res;
		result = result.substring(result.length() - 3);

		logger.trace("Converted Roman Pot name " + name + " to " + result);
		return result;
	}

	// 40*mm --> 40.0
	public static double trimDouble(String strDouble) {
		Matcher m = Pattern.compile("-?\\d+\\.{0,1}\\d*").matcher(strDouble);
		m.find();
		double value = Double.valueOf(m.group());

		logger.trace("Converted " + strDouble + " to double value " + value);
		return value;
	}

	protected final static int LABEL_MAX_PARENT_DIRECTORIES = 1;
	protected final static String WINDOWS_SEPARATOR = "\\";
	protected final static String UNIX_SEPARATOR = "/";

	// label = name of parent directory + xml file or absolute path to the xml
	public static String filenameToLabel(String xmlFilepath, int maxLabelLength) {
		logger.debug("Generating geometry label for file: " + xmlFilepath);

		String[] parts = xmlFilepath.replace(WINDOWS_SEPARATOR, UNIX_SEPARATOR).split(UNIX_SEPARATOR);
		int partsSize = parts.length;

		String label;
		try {
			label = parts[partsSize - 1];
			for (int i = 2; i <= LABEL_MAX_PARENT_DIRECTORIES + 1; ++i) {
				if (partsSize >= i) {
					String parent = parts[partsSize - i];
					if ((label.length() + parent.length()) < maxLabelLength) {
						label = parent + UNIX_SEPARATOR + label;
					}
				}
			}
		} catch (Exception e) {
			logger.error("Unknown error while creating label for geometry file " + xmlFilepath, e);
			return xmlFilepath;
		}

		logger.trace("Converted " + xmlFilepath + " to label " + label);
		logger.debug(label);
		return label;
	}
}
