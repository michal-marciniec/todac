/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.populators.geometry.commons;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;

public class Parser {
	private final static Logger logger = Logger.getLogger(Parser.class);

	public Document createXmlDocument(String xmlPath) throws IOException {
		try {
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document xmlDocument = docBuilder.parse(new File(xmlPath));
			return xmlDocument;
		} catch (SAXException e) {
			throw new IOException(e);
		} catch (ParserConfigurationException e) {
			throw new IOException(e);
		}
	}

	public Map<String, Double> parseXmlGeometry(Document xmlGeometryDocument) throws InvalidArgumentException {
		Map<String, Double> geometryData = new HashMap<String, Double>();

		NodeList nodes = xmlGeometryDocument.getElementsByTagName(Constants.MAIN_TAG);
		for (int s = 0; s < nodes.getLength(); s++) {
			Node node = nodes.item(s);

			String name = readAndConvertRPName(node);
			double value = readAndConvertValue(node);

			logger.trace("Read: " + name + "\t" + value);
			geometryData.put(name, value);
		}
		return geometryData;
	}

	protected String readAndConvertRPName(Node node) throws InvalidArgumentException {
		String attribute = node.getAttributes().getNamedItem(Constants.RPNAME_ATTR).getNodeValue();

		return Convert.RPNameToDBInt(attribute);
	}

	protected double readAndConvertValue(Node node) {
		String attribute = node.getAttributes().getNamedItem(Constants.VALUE_ATTR).getNodeValue();

		return Convert.trimDouble(attribute);
	}
}
