TOTEM Offline Database Access Console

https://cds.cern.ch/record/1645029/

Technologies: 

- Java 7

- Maven 3

- Tomcat 7

- Vaadin 7

- Log4j

- args4j

- CERN Advanced Storage Manager

- Oracle 11g