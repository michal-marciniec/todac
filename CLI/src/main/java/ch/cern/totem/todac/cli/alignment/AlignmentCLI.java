/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.alignment;

import java.util.List;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.alignment.AlignmentPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

/**
 * 
 * @author Kamil Mielnik
 */
public class AlignmentCLI implements CLI {

	private final static Logger logger = Logger.getLogger(AlignmentCLI.class);
	protected final String CLI_HELP = App.APP_NAME + " alignment <path> <start_run> [<end_run>] [-h] [-R] [-filelist] [-label <label>]";
	protected final static int MAXIMUM_LABEL_LENGTH = 50;
	
	protected DatabaseAccessProvider databaseAccessProvider;
	protected CmdLineParser cmdLineParser;
	protected String[] arguments;
	protected AlignmentPopulator alignmentPopulator = null;
	protected List<String> filepaths = null;
	
	@Argument(index=0, required=true, metaVar="<path>", usage="Path to an XML file in a proper format")
	protected String path;

	@Argument(index=1, required=true, metaVar="<startRun>", usage="Beginning TOTEM Run number")
	public int startRun;

	@Argument(index=2, required=false, metaVar="<endRun>", usage="Ending TOTEM Run number")
	protected int endRun = Integer.MIN_VALUE;

	@Option(name = "-h", usage = "Prints help")
	protected boolean help = false;
	
	@Option(name = "-R", usage = "Allows to recursively traverse directory specified in <path> in order to find XML files and process them")
	protected boolean recursive = false;

	@Option(name = "-filelist", usage = "Allows to specify multiple files for processing. Filepaths will be read from text file specified in <path>")
	protected boolean filelist = false;
	
	@Option(name="-label", required=false, metaVar="<label>", usage="Allows to specify database label explicitly")
	protected String label = null;

	
	public AlignmentCLI(DatabaseAccessProvider databaseAccessProvider, String[] arguments) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.arguments = arguments;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing arguments for " + AlignmentCLI.class.getName());
		
		try {
			cmdLineParser = new CmdLineParser(this);
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(arguments);
		} catch (CmdLineException e) {
			if(help) return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		if (endRun == Integer.MIN_VALUE) endRun = startRun;
		if(recursive && filelist) throw new CommandLineParsingException(ExceptionMessages.CLI_R_FILELIST);
		if((recursive || filelist) && label != null) throw new CommandLineParsingException(ExceptionMessages.CLI_R_FILELIST_LABEL);
		return !help;
	}
	
	@Override
	public void initialize() throws TodacException {
		logger.debug("Initializing " + AlignmentCLI.class.getName());
		
		RomanPotManager romanPotManager = null;
		try{
			romanPotManager = databaseAccessProvider.getRomanPotManager();
		}
		catch(TudasException e){
			throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
		alignmentPopulator = new AlignmentPopulator(romanPotManager);
		
		logger.debug(AlignmentCLI.class.getName() + " initialized");
	}

	@Override
	public void verify() throws TodacException {
		Validator.validateAccess(path);
		Validator.validateNonNegative(startRun);
		Validator.validateNonNegative(endRun);
		Validator.validateInterval(startRun, endRun);
		if(label != null) Validator.validateStringLength(label, MAXIMUM_LABEL_LENGTH);
		
		if(recursive){
			filepaths = Traverse.traverse(path);
		}
		else if(filelist){
			filepaths = SourceFileReader.read(path);
		}
		if(filepaths != null){
			for(String filepath : filepaths){
				Validator.validateAccess(filepath);
				Validator.validateIsFile(filepath);
			}
		}
	}

	@Override
	public void run() throws TodacException {
		if(label != null){
			alignmentPopulator.setLabel(label);
		}
		
		if(recursive || filelist){
			for(String filepath : filepaths){
				alignmentPopulator.saveAlignments(filepath, startRun, endRun);
			}
		}
		else if(filelist){
			for(String filepath : filepaths){
				alignmentPopulator.saveAlignments(filepath, startRun, endRun);
			}
		}
		else{
			alignmentPopulator.saveAlignments(path, startRun, endRun);
		}
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return alignmentPopulator;
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}
}
