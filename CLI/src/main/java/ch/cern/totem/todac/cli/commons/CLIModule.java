/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli.commons;

public enum CLIModule {
	ALIGNMENT, GEOMETRY, LUMINOSITY, OPTICS, RUNINFO, LHCLOGGING;

	public static CLIModule ignoreCaseValueOf(String moduleName) throws IllegalArgumentException {
		String upperCaseModuleName = moduleName.toUpperCase();
		return CLIModule.valueOf(upperCaseModuleName);
	}

	@Override
	public String toString() {
		return this.name().toLowerCase();
	}
}
