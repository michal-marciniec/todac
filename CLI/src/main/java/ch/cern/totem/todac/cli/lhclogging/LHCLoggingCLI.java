/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.lhclogging;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.lhclogging.LHCLoggingPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;

public class LHCLoggingCLI implements CLI {

	private final static Logger logger = Logger.getLogger(LHCLoggingCLI.class);
	protected final String OPTICS_CLI_HELP = App.APP_NAME + " lhclogging <start_run> [<end_run>] [-h] [-variable <variable>] [-set <path>]";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected CmdLineParser cmdLineParser;
	protected String[] arguments;
	protected LHCLoggingPopulator lhcLoggingPopulator;

	public LHCLoggingCLI(DatabaseAccessProvider databaseAccessProvider, String[] arguments) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.arguments = arguments;
	}

	@Argument(index = 0, required = true, metaVar = "<start_run>", usage = "Beginning TOTEM Run number")
	public int startRun;

	@Argument(index = 1, required = false, metaVar = "<end_run>", usage = "Ending TOTEM Run number")
	protected int endRun = Integer.MIN_VALUE;

	@Option(name = "-h", usage = "Prints help")
	protected boolean help = false;

	@Option(name = "-variable", required = false, metaVar = "<variable>", usage = "Allows to specify a single variable that will be acquired from LHC Logging Database")
	protected String variable = null;
	
	@Option(name = "-set", metaVar = "<path>", usage = "Allows to specify file containing set of variables that will be acquired from LHC Logging Database")
	protected String variableSetPath = null;

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing arguments for " + LHCLoggingCLI.class.getName());
		try {
			cmdLineParser = new CmdLineParser(this);
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(arguments);
		} catch (CmdLineException e) {
			if(help) return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		
		if(variableSetPath != null && variable != null){
			throw new CommandLineParsingException(ExceptionMessages.CLI_VARIABLE_SET);
		}
		
		if (endRun == Integer.MIN_VALUE)
			endRun = startRun;
		
		return !help;
	}
	
	@Override
	public void initialize() throws TodacException {
		lhcLoggingPopulator = new LHCLoggingPopulator(databaseAccessProvider);
		lhcLoggingPopulator.initialize();
	}

	@Override
	public void verify() throws TodacException {
		if (variableSetPath != null) {
			Validator.validateIsFile(variableSetPath);
			Validator.validateAccess(variableSetPath);
		}
		Validator.validateNonNegative(startRun);
		Validator.validateNonNegative(endRun);
		Validator.validateInterval(startRun, endRun);
	}

	@Override
	public void run() throws TodacException {
		if(variableSetPath != null) {
			lhcLoggingPopulator.populateByRun(startRun, endRun, variableSetPath);
		}
		else if (variable != null) {
			lhcLoggingPopulator.populateByRun(variable, startRun, endRun);
		} else {
			lhcLoggingPopulator.populateByRun(startRun, endRun);
		}
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return lhcLoggingPopulator;
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", OPTICS_CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}

}
