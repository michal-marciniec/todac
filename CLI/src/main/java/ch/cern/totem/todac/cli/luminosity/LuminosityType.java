/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.luminosity;

public enum LuminosityType {
	ByLs, ByLsXing
}
