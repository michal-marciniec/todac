/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.alignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;

public class SourceFileReader {

	private final static Logger logger = Logger.getLogger(SourceFileReader.class);
	
	public static List<String> read(String filepath) throws TodacException {
		logger.debug("Reading file: " + filepath);
		List<String> filepaths = new LinkedList<String>();
		
		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(filepath));
			while ((line = br.readLine()) != null) {
				if(!line.isEmpty()){
					if(filepaths.contains(line)){
						logger.warn("Duplicated entry: " + line);
					}
					filepaths.add(line);
				}
			}
		} catch (Exception e) {
			throw new TodacException(ExceptionMessages.FILE_ACCESS, e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e2) {
					throw new TodacException(ExceptionMessages.FILE_ACCESS, e2);
				}
			}
		}
		logger.debug("File: " + filepath + " read");
		return filepaths;
	}
}
