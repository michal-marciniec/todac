/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.ProgressBar;

public class App {

	public final static String APP_NAME = "todac";

	private static Logger logger = Logger.getLogger(App.class);

	private static void setUpLogger() {
		try {
			InputStream propertiesStream = App.class.getResourceAsStream("/log4j.properties");
			Properties prop = new Properties();
			prop.load(propertiesStream);
			PropertyConfigurator.configure(prop);
		} catch (Exception e) {
			logger.warn("Error while setting up logger.", e);
		}
	}

	static {
		setUpLogger();
	}

	public static void main(String[] args) {
		CLI cli = null;
		ProgressBar progressBar = null;
		try {
			logger.debug("TODAC CLI application started");

			cli = new CoreCLI(args);
			cli.initialize();
			progressBar = new ProgressBar(cli.getMeasurableObject());
			progressBar.start();
			cli.run();

			logger.debug("TODAC CLI application finished");
		} catch (CommandLineParsingException e) {
			cli.printHelp();
		} catch (TodacException e) {
			Out.println(e);
		} finally {
			progressBar.stop();
		}
	}
}
