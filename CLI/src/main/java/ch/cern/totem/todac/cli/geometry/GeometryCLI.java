/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli.geometry;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.exceptions.ValidatorException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.geometry.GeometryPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;

public class GeometryCLI implements CLI {
	
	private final static Logger logger = Logger.getLogger(GeometryCLI.class);
	protected final String GEOMETRY_CLI_HELP = App.APP_NAME + " geometry <path> <start_run> [<end_run>] [-h] [-label <label>]";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected GeometryPopulator geometryPopulator;
	protected CmdLineParser cmdLineParser;

	protected String[] args;

	@Option(name = "-h", usage = "Prints help")
	protected boolean help = false;

	@Option(name = "-label", required = false, metaVar = "<label>", usage = "Allows to specify database label explicitly\nNote: <label> must not contain white spaces\nDefault value: a label based on <path>")
	protected String label = null;

	@Argument(index = 0, required = true, metaVar = "<path>", usage = "Path to an XML file in a proper format (see the documentation for allowed formats of geometry XMLs)")
	protected String xmlFilepath;

	@Argument(index = 1, required = true, metaVar = "<start_run>", usage = "Beginning TOTEM Run number")
	public int startRun;

	@Argument(index = 2, required = false, metaVar = "<end_run>", usage = "Ending TOTEM Run number\nDefault value: <start_run>")
	protected int endRun = Integer.MIN_VALUE;

	public GeometryCLI(DatabaseAccessProvider databaseAccessProvider, String[] args) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.args = args;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing command line arguments");
		
		cmdLineParser = new CmdLineParser(this);
		parseCommandLine();
		try {
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			if(help) return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		if (endRun == Integer.MIN_VALUE)
			endRun = startRun;

		return !help;
	}

	@Override
	public void initialize() throws DatabaseException {
		logger.debug("Initialization");

		logger.debug("GeometryPopulator initialization");
		geometryPopulator = new GeometryPopulator(databaseAccessProvider);
		geometryPopulator.initialize();
	}

	protected void parseCommandLine() throws CommandLineParsingException {
		
	}

	@Override
	public void run() throws TodacException {
		runGeometryPopulator();
	}

	protected void runGeometryPopulator() throws TodacException {
		if (label == null)
			geometryPopulator.parseAndPopulate(xmlFilepath, startRun, endRun);
		else
			geometryPopulator.parseAndPopulate(xmlFilepath, startRun, endRun, label);
	}

	@Override
	public void verify() throws ValidatorException {
		Validator.validateAccess(xmlFilepath);
		Validator.validateIsFile(xmlFilepath);
		Validator.validateNonNegative(startRun);
		Validator.validateNonNegative(endRun);
		Validator.validateInterval(startRun, endRun);
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", GEOMETRY_CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return geometryPopulator;
	}

}
