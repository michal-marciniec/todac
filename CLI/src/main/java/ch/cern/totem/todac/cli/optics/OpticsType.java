package ch.cern.totem.todac.cli.optics;

public enum OpticsType {
	BETASTAR, CURRENTS, STRENGTH, ALL;

	public static OpticsType ignoreCaseValueOf(String str) {
		return OpticsType.valueOf(str.toUpperCase());
	}
}
