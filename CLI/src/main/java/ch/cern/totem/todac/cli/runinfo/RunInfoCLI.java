/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.runinfo;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.runinfo.RunInfoPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class RunInfoCLI implements CLI {
	
	private final static Logger logger = Logger.getLogger(RunInfoCLI.class);
	protected final String CLI_HELP = App.APP_NAME + " runinfo [-h] <run_number> [-description <description>]";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected CmdLineParser cmdLineParser;
	protected String[] arguments;
	protected RunInfoPopulator runInfoPopulator;

	@Argument(index = 0, required = true, metaVar = "<run_number>", usage = "TOTEM Run number")
	protected int runNumber;
	
	@Option(name = "-h", aliases = "-help", usage = "Prints help")
	protected boolean help = false;
	
	@Option(name = "-description", required = false, metaVar = "<description>", usage = "Allows to specify run description explicitly")
	protected String description = null;

	public RunInfoCLI(DatabaseAccessProvider databaseAccessProvider, String[] arguments) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.arguments = arguments;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing arguments for " + RunInfoCLI.class.getName());
		cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(arguments);
		} catch (CmdLineException e) {
			if(help) return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		return !help;
	}

	@Override
	public void initialize() throws TodacException {
		RunInformationManager runInformationManager = null;
		try {
			runInformationManager = databaseAccessProvider.getRunInformationManager();
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
		runInfoPopulator = new RunInfoPopulator(runInformationManager);
		runInfoPopulator.initialize();
	}

	@Override
	public void verify() throws TodacException {
		Validator.validatePositive(runNumber);
	}

	@Override
	public void run() throws TodacException {
		if (description == null) {
			runInfoPopulator.saveRunInfo(runNumber);
		} else {
			runInfoPopulator.saveRunInfo(runNumber, description);
		}
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return runInfoPopulator;
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}

}
