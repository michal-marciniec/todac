/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.luminosity;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.luminosity.LuminosityPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.LuminosityManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class LuminosityCLI implements CLI {

	private final static Logger logger = Logger.getLogger(LuminosityCLI.class);
	protected final String CLI_HELP = App.APP_NAME + " luminosity <path> -type <type> [-h] [-label <label>] [-lsLengthSec <lsLengthSec>]";
	protected final static int MAXIMUM_LABEL_LENGTH = 50;
	
	protected DatabaseAccessProvider databaseAccessProvider;
	protected CmdLineParser cmdLineParser;
	protected String[] arguments;
	protected LuminosityPopulator luminosityPopulator;

	@Argument(index=0, required=true, metaVar="<path>", usage="Path to a CSV file in a proper format")
	protected String path;
	
	@Option(name="-type", required=true, metaVar="<type>", usage="Type of luminosity stored in <path>")
	protected LuminosityType type = null;
	
	@Option(name = "-h", aliases = "-help", usage = "Prints help")
	protected boolean help = false;
	
	@Option(name="-label", required=false, metaVar="<label>", usage="Allows to specify database label explicitly")
	protected String label = null;
	
	@Option(name="-lsLengthSec", aliases="-lslengthsec", required=false, metaVar="<lsLengthSec>", usage="Allows to specify custom lsLengthSec value")
	protected double lsLengthSec = Double.MAX_VALUE;
	
	public LuminosityCLI(DatabaseAccessProvider databaseAccessProvider, String[] arguments) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.arguments = arguments;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing arguments for " + LuminosityCLI.class.getName());
		try {
			cmdLineParser = new CmdLineParser(this);
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(arguments);
		} catch (CmdLineException e) {
			if(help) return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		return !help;
	}
	
	@Override
	public void initialize() throws CommandLineParsingException, TodacException {	
		LuminosityManager luminosityManager = null;
		try {
			luminosityManager = databaseAccessProvider.getLuminosityManager();
		} catch (TudasException e) {
			throw new TodacException(ExceptionMessages.TUDAS_GET_MANAGER, e);
		}
		luminosityPopulator = new LuminosityPopulator(luminosityManager);
	}

	@Override
	public void verify() throws TodacException {
		Validator.validateAccess(path);
		Validator.validateIsFile(path);
		Validator.validatePositive(lsLengthSec);
		if(label != null) Validator.validateStringLength(label, MAXIMUM_LABEL_LENGTH);
	}

	@Override
	public void run() throws TodacException {
		if(lsLengthSec != Double.MAX_VALUE) luminosityPopulator.setLsLengthSec(lsLengthSec);
		if(label != null) luminosityPopulator.setLabel(label);
		
		switch(type){
			case ByLs:
				luminosityPopulator.saveLuminosityByLs(path);
				break;
			case ByLsXing:
				luminosityPopulator.saveLuminosityByLsXing(path);
				break;
			default:
				break;
		}
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return luminosityPopulator;
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}
}
