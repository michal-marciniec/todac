/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli;

import java.util.Arrays;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.cli.alignment.AlignmentCLI;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.cli.commons.CLIModule;
import ch.cern.totem.todac.cli.geometry.GeometryCLI;
import ch.cern.totem.todac.cli.lhclogging.LHCLoggingCLI;
import ch.cern.totem.todac.cli.luminosity.LuminosityCLI;
import ch.cern.totem.todac.cli.optics.OpticsCLI;
import ch.cern.totem.todac.cli.runinfo.RunInfoCLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.utils.progressbar.ProgressBar;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class CoreCLI implements CLI {

	private final static Logger logger = Logger.getLogger(CoreCLI.class);
	public final static String DOCUMENTATION_WEBPAGE = "https://twiki.cern.ch/twiki/bin/view/TOTEM/CompDBTodacTotemOfflineDatabaseAccessConsole";
	protected final String CORE_CLI_USAGE = App.APP_NAME + " <measurement> [<measurement_parameters>]";
	protected final String CORE_CLI_DESCRIPTION = "\t<measurement> := {alignment,geometry,luminosity,lhclogging,runinfo,optics}\n\t[<measurement_parameters>] - measurement-specific parameters";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected boolean initialized = false;

	protected CLIModule cliModule;
	protected String[] args;

	public CoreCLI(String[] args) {
		this.databaseAccessProvider = null;
		this.args = args;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		return true;
	}

	@Override
	public void initialize() throws CommandLineParsingException {
		logger.debug("Parsing command line arguments");
		try {
			cliModule = CLIModule.ignoreCaseValueOf(args[0]);
		} catch (Exception e) {
			logger.info("Command line arguments parsing error.");
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS);
		}
		databaseAccessProvider = DatabaseAccessProvider.getInstance();
	}

	@Override
	public void run() throws TodacException {
		logger.info("Starting " + cliModule + " CLI");
		String[] moduleArgs = Arrays.copyOfRange(args, 1, args.length);

		CLI cli = null;
		ProgressBar progressBar = null;
		try {
			cli = getModuleCLI(moduleArgs);
			if(!cli.parseArguments()){
				cli.printHelp();
				printDocumentation();
			}
			else{
				cli.verify();
				initialized = true;
				databaseAccessProvider.initialize();
				cli.initialize();
				
				//progressBar = new ProgressBar(cli.getMeasurableObject());
				//progressBar.start();
				cli.run();
			}
		} catch (CommandLineParsingException e) {
			Out.println(e);
			cli.printHelp();
			printDocumentation();
		} catch(TudasException e){
			throw new TodacException(ExceptionMessages.TUDAS_UNKNOWN, e);
		}
		finally{
			if(progressBar != null){
				progressBar.stop();
			}
			if (databaseAccessProvider != null && initialized) {
				try {
					databaseAccessProvider.close();
				} catch (TudasException e) {
					Out.println(e);
				}
			}
		}
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", "\t" + CORE_CLI_USAGE, "Parameters:", CORE_CLI_DESCRIPTION);
		printDocumentation();
	}
	
	private void printDocumentation() {
		Out.println("", "*** Documentation ***", "\t" + DOCUMENTATION_WEBPAGE);
	}

	protected CLI getModuleCLI(String[] args) throws TodacException {
		CLI cli = null;
		switch (cliModule) {
			case ALIGNMENT:
				cli = new AlignmentCLI(databaseAccessProvider, args);
				break;
			case GEOMETRY:
				cli = new GeometryCLI(databaseAccessProvider, args);
				break;
			case LUMINOSITY:
				cli = new LuminosityCLI(databaseAccessProvider, args);
				break;
			case OPTICS:
				cli = new OpticsCLI(databaseAccessProvider, args);
				break;
			case RUNINFO:
				cli = new RunInfoCLI(databaseAccessProvider, args);
				break;
			case LHCLOGGING:
				cli = new LHCLoggingCLI(databaseAccessProvider, args);
				break;
			default:
				throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS);
		}
		return cli;
	}

	@Override
	public void verify() throws TodacException {

	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return null;
	}

}
