/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.alignment;

public enum AlignmentType {
	Description, SequenceDescription, Any
}
