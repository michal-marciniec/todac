/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.alignment;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;


public class Traverse {

	private final static Logger logger = Logger.getLogger(Traverse.class);
	
	public static List<String> traverse(String directory) {
		logger.debug("Traversing location: " + directory);
		
		List<String> results = new LinkedList<String>();
		File current = new File(directory);

		if (current.isFile()) {
			logger.debug("Current traverse node: " + current.getAbsolutePath() + " is a file");
			if (matchFilename(current.getName())) {
				results.add(current.getAbsolutePath());
			}
		}
		else {
			logger.debug("Current traverse node: " + current.getAbsolutePath() + " is a directory");
			File[] children = current.listFiles();
			for (File child : children) {
				results.addAll(traverse(child.getAbsolutePath()));
			}
		}

		logger.debug("Finished traversing location: " + directory);
		return results;
	}

	protected static boolean matchFilename(String filename) {
		return filename.matches(".*\\.xml$");
	}
	
}
