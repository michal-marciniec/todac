/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.cli.commons;

import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;

public interface CLI {	
	static final int CONSOLE_WIDTH = Integer.MAX_VALUE;
	
	boolean parseArguments() throws CommandLineParsingException;
	void initialize() throws TodacException;
	void verify() throws TodacException;
	void run() throws TodacException;
	MeasurableProgress getMeasurableObject();
	void printHelp();
}
