/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli.optics;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionDef;
import org.kohsuke.args4j.spi.OptionHandler;
import org.kohsuke.args4j.spi.Parameters;
import org.kohsuke.args4j.spi.Setter;

@SuppressWarnings("rawtypes")
public class OpticsTypeHandler extends OptionHandler {

	@SuppressWarnings("unchecked")
	public OpticsTypeHandler(CmdLineParser parser, OptionDef option, Setter setter) {
		super(parser, option, setter);
	}

	@Override
	public String getDefaultMetaVariable() {
		return "measurementType";
	}

	@SuppressWarnings("unchecked")
	@Override
	public int parseArguments(Parameters param) throws CmdLineException {
		try {
			String module = param.getParameter(0).toUpperCase();
			setter.addValue(OpticsType.ignoreCaseValueOf(module));
			return 1;
		} catch (Exception e) {
			return 0;
		}
	}

}
