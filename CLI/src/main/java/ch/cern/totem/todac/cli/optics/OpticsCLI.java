/**
 * @author Michal Marciniec
 * michal.marciniec@gmail.com
 */

package ch.cern.totem.todac.cli.optics;

import org.apache.log4j.Logger;
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import ch.cern.totem.todac.cli.App;
import ch.cern.totem.todac.cli.commons.CLI;
import ch.cern.totem.todac.commons.exceptions.CommandLineParsingException;
import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.exceptions.ValidatorException;
import ch.cern.totem.todac.commons.utils.Out;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.commons.validator.Validator;
import ch.cern.totem.todac.populators.optics.OpticsPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;

public class OpticsCLI implements CLI {
	private final static Logger logger = Logger.getLogger(OpticsCLI.class);
	protected final String OPTICS_CLI_HELP = App.APP_NAME + " optics <path> <start_run> [<end_run>] [-type <type>] [-h]";

	protected DatabaseAccessProvider databaseAccessProvider;
	protected OpticsPopulator opticsPopulator;
	protected CmdLineParser cmdLineParser;

	protected String[] args;

	@Option(name = "-h", usage = "Prints help")
	protected boolean help = false;

	@Option(name = "-type", required = false, metaVar = "<type>", usage = "Type of optics stored in files specified in <path>\n<type> must be one of the following: betastar, strength, currents, all\nDefault value: all (see the documentation)", handler = OpticsTypeHandler.class)
	protected OpticsType type = OpticsType.ALL;

	@Argument(index = 0, required = true, metaVar = "<path>", usage = "Path to an CSV file in a proper format (see the documentation for allowed formats of optics CSVs)")
	protected String path;

	@Argument(index = 1, required = true, metaVar = "<start_run>", usage = "Beginning TOTEM Run number")
	public int startRun;

	@Argument(index = 2, required = false, metaVar = "<end_run>", usage = "Ending TOTEM Run number\nDefault value: <start_run>")
	protected int endRun = Integer.MIN_VALUE;

	public OpticsCLI(DatabaseAccessProvider databaseAccessProvider, String[] args) {
		this.databaseAccessProvider = databaseAccessProvider;
		this.args = args;
	}

	@Override
	public boolean parseArguments() throws CommandLineParsingException {
		logger.debug("Parsing command line arguments");

		cmdLineParser = new CmdLineParser(this);
		try {
			cmdLineParser.setUsageWidth(CONSOLE_WIDTH);
			cmdLineParser.parseArgument(args);
		} catch (CmdLineException e) {
			if (help)
				return false;
			logger.info("Command line arguments parsing error.", e);
			throw new CommandLineParsingException(ExceptionMessages.CLI_WRONG_ARGUMENTS, e);
		}
		if (endRun == Integer.MIN_VALUE)
			endRun = startRun;
		return !help;
	}

	@Override
	public void initialize() throws DatabaseException {
		logger.debug("Initialization");

		opticsPopulator = new OpticsPopulator(databaseAccessProvider);
		opticsPopulator.initialize();
	}

	protected void parseCommandLine() throws CommandLineParsingException {

	}

	@Override
	public void verify() throws ValidatorException {
		if (!help) {
			if (type != OpticsType.ALL)
				Validator.validateIsFile(path);
			else {
				OpticsPopulator.validateOpticsFound(path);
			}
			Validator.validateAccess(path);
			Validator.validateNonNegative(startRun);
			Validator.validateNonNegative(endRun);
			Validator.validateInterval(startRun, endRun);
		}
	}

	@Override
	public void run() throws TodacException {
		if (help) {
			printHelp();
			return;
		} else {
			runOpticsPopulator();
		}
	}

	protected void runOpticsPopulator() throws TodacException {
		switch (type) {
			case ALL:
				opticsPopulator.parseAndPopulateOptics(path, startRun, endRun);
				break;
			case BETASTAR:
				opticsPopulator.parseAndPopulateBetaStar(path);
				break;
			case CURRENTS:
				opticsPopulator.parseAndPopulateCurrent(path, startRun, endRun);
				break;
			case STRENGTH:
				opticsPopulator.parseAndPopulateMagnetStrength(path, startRun, endRun);
				break;
			default:
				printHelp();
				break;
		}
	}

	@Override
	public MeasurableProgress getMeasurableObject() {
		return opticsPopulator;
	}

	@Override
	public void printHelp() {
		Out.println("*** Usage ***", OPTICS_CLI_HELP);
		cmdLineParser.printUsage(Out.stream);
		Out.println();
	}
}
