package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.lhclogging.LHCLoggingBrowser;
import ch.cern.totem.todac.web.components.lhclogging.LHCLoggingPopulateForm;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class LHCLoggingView extends TodacView {

	public static final TodacURL URL = new TodacURL("lhclogging", "LHC Logging Database");

	public LHCLoggingView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new LHCLoggingPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new LHCLoggingBrowser();
	}

}
