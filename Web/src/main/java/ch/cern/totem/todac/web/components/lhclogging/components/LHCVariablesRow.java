package ch.cern.totem.todac.web.components.lhclogging.components;


import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.LHCLoggingPopulator;
import ch.cern.totem.todac.web.components.forms.derivable.FormRowLayout;
import ch.cern.totem.todac.web.utils.Notificator;

import java.util.HashSet;
import java.util.Set;

public class LHCVariablesRow extends FormRowLayout {

    protected static final Set<String> DEFAULT_VARIABLES = fetchDefaultSet();
    protected LHCVariablesTableBuilder tableBuilder;
    protected LHCVariablesControlPanel controlPanel;

    public LHCVariablesRow() {
        initComponents();
        this.addLeftComponent(controlPanel);
        this.addRightComponent(tableBuilder.getTable());
        reset();
    }

    protected void initComponents() {
        tableBuilder = new LHCVariablesTableBuilder();
        controlPanel = new LHCVariablesControlPanel(tableBuilder, DEFAULT_VARIABLES);
    }

    @Override
    public void reset() {
        tableBuilder.clear();
        tableBuilder.add(DEFAULT_VARIABLES);
        tableBuilder.sort();
        controlPanel.reset();
    }

    public Set<String> getVariables() {
        return tableBuilder.getVariables();
    }

    protected static Set<String> fetchDefaultSet() {
        try {
            final Set<String> defaultMeasurements = LHCLoggingPopulator.getDefaultMeasurements();
            return defaultMeasurements;
        } catch (TodacException e) {
            Notificator.showError(e);
            return new HashSet<>(0);
        }
    }
}
