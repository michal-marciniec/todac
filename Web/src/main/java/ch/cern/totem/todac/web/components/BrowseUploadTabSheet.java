package ch.cern.totem.todac.web.components;

import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;

@SuppressWarnings("serial")
public class BrowseUploadTabSheet extends TabSheet {

    private static final String BROWSE_TAB = "Browse";
    private static final String UPLOAD_TAB = "Upload";
	
	public BrowseUploadTabSheet(Component browseComponent, Component uploadComponent) {
		super();
		this.addTab(browseComponent, BROWSE_TAB);
		this.addTab(uploadComponent, UPLOAD_TAB);
	}

}
