package ch.cern.totem.todac.web.components.runinfo;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.TimestampRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.runinfo.RunInformationCheckParameters;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class RunInformationBrowserForm extends FormLayout {

	protected BrowserSwitcher browserSwitcher;
	
	protected TimestampRow timestampRow;
	protected SubmitRow submitRow;
	
	public RunInformationBrowserForm(BrowserSwitcher browserSwitcher) {
		this.browserSwitcher = browserSwitcher;
		this.initComponents();
	}
	
	protected void initComponents() {
		timestampRow = new TimestampRow();
		submitRow = new SubmitRow();
		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				browserSwitcher.switchToBrowser();
			}
		});
		
		addComponent(timestampRow);
		addComponent(submitRow);
	}
	
	public RunInformationCheckParameters getParameters() throws InvalidArgumentException {
		try{
			String timestamp = timestampRow.getValue();
			return new RunInformationCheckParameters(timestamp);
		}
		catch(Exception e){
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
