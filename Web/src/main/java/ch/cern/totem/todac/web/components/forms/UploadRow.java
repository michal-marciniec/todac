package ch.cern.totem.todac.web.components.forms;

import java.io.File;

import ch.cern.totem.todac.web.components.forms.derivable.FormRowLayout;
import ch.cern.totem.todac.web.utils.UploadReceiver;

import com.vaadin.ui.Label;
import com.vaadin.ui.Upload;

@SuppressWarnings("serial")
public class UploadRow extends FormRowLayout {
	
	protected static final String FILE = "File";
	
	protected Upload upload;
	protected UploadReceiver uploadReceiver;
	
	public UploadRow() {
		this.initComponents();
	}
	
	protected void initComponents() {
		upload = new Upload();
		upload.setButtonCaption(null); // IMPORTANT, see jdoc
		addLeftComponent(new Label(FILE));
		addRightComponent(upload);
	}
	
	public void setUploadReceiver(UploadReceiver receiver) {
		uploadReceiver = receiver;
		upload.setReceiver(receiver);
		upload.addSucceededListener(receiver);
	}
	
	public void submitUpload() {
		upload.submitUpload();
	}
	
	public File getFile() {
		return uploadReceiver.getFile();
	}

	@Override
	public void reset() {

	}
	
}
