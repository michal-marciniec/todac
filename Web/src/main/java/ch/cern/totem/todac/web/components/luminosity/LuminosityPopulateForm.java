package ch.cern.totem.todac.web.components.luminosity;

import java.io.File;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.forms.LabelRow;
import ch.cern.totem.todac.web.components.forms.LsLengthSecRow;
import ch.cern.totem.todac.web.components.forms.LuminosityTypeRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.UploadRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.luminosity.LuminosityDao;
import ch.cern.totem.todac.web.model.luminosity.LuminosityType;
import ch.cern.totem.todac.web.model.luminosity.LuminosityUploadParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.todac.web.utils.UploadReceiver;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

@SuppressWarnings("serial")
public class LuminosityPopulateForm extends FormLayout {

	protected LabelRow labelRow;
	protected LuminosityTypeRow luminosityTypeRow;
	protected LsLengthSecRow lsLengthSecRow;
	protected UploadRow uploadRow;
	protected SubmitRow submitRow;

	protected LuminosityDao luminosityDao;

	public LuminosityPopulateForm() {
		try {
			this.initComponents();
		} catch (TudasException e) {
			Notificator.showError(e);
		}
	}

	protected void initComponents() throws TudasException {
		luminosityDao = new LuminosityDao();

		labelRow = new LabelRow();
		luminosityTypeRow = new LuminosityTypeRow();
		lsLengthSecRow = new LsLengthSecRow();
		uploadRow = new UploadRow();
		submitRow = new SubmitRow();

		uploadRow.setUploadReceiver(new UploadReceiver(new SucceededListener() {
			public void uploadSucceeded(SucceededEvent event) {
				File file = uploadRow.getFile();

				try {
					luminosityDao.uploadLuminosity(getParameters());
					Notificator.showTrayMessage("Upload Successfull", file.getAbsolutePath());
				} catch (InvalidArgumentException e) {
					Notificator.showError(e);
				}
				file.delete();
				reset();
			}
		}));

		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				uploadRow.submitUpload();
			}
		});

		addComponent(luminosityTypeRow);
		addComponent(labelRow);
		addComponent(lsLengthSecRow);
		addComponent(uploadRow);
		addComponent(submitRow);
	}

	protected LuminosityUploadParameters getParameters() throws InvalidArgumentException {
		try {
			String filepath = uploadRow.getFile().getAbsolutePath();
			String label = labelRow.getValue();
			Double lsLengthSec = lsLengthSecRow.isChecked() ? Double.valueOf(lsLengthSecRow.getValue()) : null;
			LuminosityType type = luminosityTypeRow.getValue();
			return new LuminosityUploadParameters(filepath, label, lsLengthSec, type);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
