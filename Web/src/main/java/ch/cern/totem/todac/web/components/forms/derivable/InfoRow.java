package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
public class InfoRow extends FormRowLayout {

	protected Label label;

	public InfoRow(String caption) {
		this.initComponents(caption);
	}

	protected void initComponents(String caption) {
		label = new Label(caption);
		leftContent = new HorizontalLayout(label);
		leftContent.setStyleName(STYLE_FORM_LEFT_CONTENT);
		content = new HorizontalLayout(leftContent);
		content.setStyleName(STYLE_FORM_CONTENT);
		setCompositionRoot(content);
	}

	public void setValue(String value) {
		label.setValue(value);
	}

	@Override
	public void reset() {

	}

}
