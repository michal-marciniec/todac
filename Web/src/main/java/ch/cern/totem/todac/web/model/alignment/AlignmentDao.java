package ch.cern.totem.todac.web.model.alignment;

import java.util.Map;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.populators.alignment.AlignmentPopulator;
import ch.cern.totem.todac.web.model.populators.Populators;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.IntervalOfValidity;
import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;

public class AlignmentDao {

	private static Logger logger = Logger.getLogger(AlignmentDao.class);

	protected AlignmentPopulator alignmentPopulator;

	public AlignmentDao() throws TudasException {
		this.alignmentPopulator = Populators.getAlignmentPopulator();
	}

	public MeasurableProgress getMeasurableObject() {
		return alignmentPopulator;
	}

	public AlignmentData downloadAlignments(AlignmentDownloadParameters parameters) {
		logger.debug("Downloading alignments (" + parameters.getLabel() + ")");
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		try {
			databaseAccessProvider.initialize();
			RomanPotManager romanPotManager = databaseAccessProvider.getRomanPotManager();
			Map<IntervalOfValidity, RomanPotAlignment[]> alignments = null;
			if (parameters.getVersion() < 0) {
				alignments = romanPotManager.loadAlignmentsByRun(parameters.getStartRun(), parameters.getEndRun(), parameters.getLabel());
			} else {
				alignments = romanPotManager.loadAlignmentsByRun(parameters.getStartRun(), parameters.getEndRun(), parameters.getLabel(), parameters.getVersion());
			}
			return new AlignmentData(alignments, parameters.getLabel());
		} catch (TudasException e) {
			Notificator.showError(e);
		}
		logger.warn("No alignments found (" + parameters.getLabel() + ")");
		return new AlignmentData();
	}

	public void uploadAlignments(AlignmentUploadParameters parameters) {
		logger.debug("Uploading alignments (" + parameters.getLabel() + ")");
		try {
			alignmentPopulator.setLabel(parameters.getLabel());
			alignmentPopulator.saveAlignments(parameters.getFilepath(), parameters.getStartRun(), parameters.getEndRun());
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}

}
