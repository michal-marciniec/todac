package ch.cern.totem.todac.web.components.forms.derivable;

import ch.cern.totem.todac.web.components.forms.validators.NotEmptyValidator;

import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class InputRow extends FormRowLayout {

	protected TextField inputField;

	public InputRow(String caption) {
		this.initComponents(caption);
	}

	protected void initComponents(String caption) {
		inputField = new TextField();
		inputField.addValidator(new NotEmptyValidator());
		inputField.setImmediate(true);
		addLeftComponent(new Label(caption));
		addRightComponent(inputField);
	}

	public String getValue() {
		return inputField.getValue();
	}

	public void reset() {
		inputField.setValue("");
	}

}
