package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.InputRow;
import ch.cern.totem.todac.web.components.forms.validators.TimestampValidator;

@SuppressWarnings("serial")
public class TimestampRow extends InputRow {
	
	protected static final String CAPTION = "Timestamp"; // (MM/DD/YY hh:mm:ss)

	public TimestampRow() {
		super(CAPTION);
		this.inputField.addValidator(new TimestampValidator());
	}
	
}
