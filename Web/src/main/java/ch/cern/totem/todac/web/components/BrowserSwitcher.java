package ch.cern.totem.todac.web.components;

public interface BrowserSwitcher {

	void switchToBrowser();
	
	void switchToSearchBox();

}
