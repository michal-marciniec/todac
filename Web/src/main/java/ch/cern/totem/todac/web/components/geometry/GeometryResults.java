package ch.cern.totem.todac.web.components.geometry;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.BrowserButtonsRow;
import ch.cern.totem.todac.web.components.forms.ButtonsRow;
import ch.cern.totem.todac.web.model.geometry.GeometryData;
import ch.cern.totem.todac.web.utils.DownloadResourceProvider;
import ch.cern.totem.todac.web.utils.Downloader;
import ch.cern.totem.todac.web.utils.Notificator;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class GeometryResults extends VerticalLayout implements DownloadResourceProvider {

    private static final String DOWNLOAD_FILENAME = "RP_Dist_Beam_Cent.xml";
    protected BrowserSwitcher switcher;
    protected GeometryDataTable dataTable;
    protected Downloader downloader;
    protected ButtonsRow buttonsRow;

    public GeometryResults(GeometryData contents, BrowserSwitcher switcher) {
        this.switcher = switcher;

        initComponents();
        setContents(contents);
    }

    protected void initComponents() {
        dataTable = new GeometryDataTable();
        downloader = new Downloader(this);
        buttonsRow = new BrowserButtonsRow();
        buttonsRow.addLeftClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                switcher.switchToSearchBox();
            }
        });
        downloader.extend(buttonsRow.getRightButton());

        this.addComponents(dataTable, buttonsRow);
    }

    public GeometryData getContents() {
        return dataTable.getContents();
    }

    public void setContents(GeometryData contents) {
        dataTable.setContents(contents);
    }

    @Override
    public String getResource() {
        try {
            final String resource = getContents().parseCsv();
            return resource;
        } catch (InvalidArgumentException e) {
            Notificator.showError(e);
            return "";
        }
    }

    @Override
    public String getFilename() {
        return DOWNLOAD_FILENAME;
    }
}
