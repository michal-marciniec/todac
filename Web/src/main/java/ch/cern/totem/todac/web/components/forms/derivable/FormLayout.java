package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class FormLayout extends CustomComponent implements FormComponent {

	protected static final String STYLE_FORM = "form";

	protected VerticalLayout content;
	protected List<FormRowLayout> rows;

	public FormLayout(FormRowLayout... rowLayouts) {
		this.rows = new LinkedList<FormRowLayout>();
		this.initComponents(rowLayouts);
	}

	protected void initComponents(FormRowLayout... rowLayouts) {
		content = new VerticalLayout();
		for (FormRowLayout rowLayout : rowLayouts) {
			content.addComponent(rowLayout);
			rows.add(rowLayout);
		}
		content.setStyleName(STYLE_FORM);
		setCompositionRoot(content);
		setImmediate(true);
	}

	public void addComponent(FormRowLayout rowLayout) {
		rows.add(rowLayout);
		content.addComponent(rowLayout);
	}

    public void addComponents(FormRowLayout... rowLayouts) {
        for(FormRowLayout rowLayout : rowLayouts)
            addComponent(rowLayout);
    }

	@Override
	public void reset() {
		for (FormRowLayout row : rows) {
			row.reset();
		}
	}

}
