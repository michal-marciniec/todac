package ch.cern.totem.todac.web.model.luminosity;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import ch.cern.totem.todac.web.model.Constants;

public class LuminosityDownloadParameters {

	protected final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);

	protected String label;
	protected Long timestamp;
	protected LuminosityType type;
	protected Integer version;

	public LuminosityDownloadParameters(String label, String timestamp, LuminosityType type, Integer version) throws ParseException {
		this.label = label;
		this.timestamp = dateFormat.parse(timestamp).getTime();
		this.type = type;
		this.version = version;
	}

	public String getLabel() {
		return label;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public LuminosityType getType() {
		return type;
	}

	public Integer getVersion() {
		return version;
	}

}
