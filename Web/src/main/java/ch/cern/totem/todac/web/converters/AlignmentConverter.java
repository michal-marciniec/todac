package ch.cern.totem.todac.web.converters;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ch.cern.totem.todac.web.model.alignment.AlignmentData;
import ch.cern.totem.todac.web.model.alignment.AlignmentData.Type;
import ch.cern.totem.tudas.tudasUtil.structure.IntervalOfValidity;
import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;

public class AlignmentConverter {

	public static String convert(AlignmentData alignmentData) {
		Map<IntervalOfValidity, RomanPotAlignment[]> alignments = alignmentData.getData();

		if (alignments == null || alignments.size() <= 0) {
			return null;
		}
		StringBuilder documentBuilder = new StringBuilder();
		documentBuilder.append("<xml DocumentType=\"");
		documentBuilder.append(alignmentData.getType());
		documentBuilder.append("\">\n");
		for (IntervalOfValidity iov : alignments.keySet()) {
			RomanPotAlignment[] rpAlignments = alignments.get(iov);
			List<RomanPotAlignment> romanPotAlignments = new LinkedList<RomanPotAlignment>();
			for (int i = 0; i < rpAlignments.length; ++i) {
				romanPotAlignments.add(rpAlignments[i]);
			}
			if (alignmentData.getType() == Type.AlignmentSequenceDescription) {
				documentBuilder.append("\n\t<TimeInterval first=\"");
				documentBuilder.append(iov.startTime / 1000);
				documentBuilder.append("\" last=\"");
				documentBuilder.append(iov.endTime / 1000);
				documentBuilder.append("\">\n");
			}
			sort(romanPotAlignments);
			for (RomanPotAlignment romanPotAlignment : romanPotAlignments) {
				StringBuilder rpBuilder = new StringBuilder();

				int id = Integer.parseInt(romanPotAlignment.romanPotLabel);
				rpBuilder.append(id < 100 ? "\n\t<rp " : "\t<det");
				rpBuilder.append(" id=\"");
				rpBuilder.append(String.format("%4s", romanPotAlignment.romanPotLabel));
				rpBuilder.append("\"");
				for (String key : romanPotAlignment.alignmentValues.keySet()) {
					rpBuilder.append(" ");
					rpBuilder.append(String.format("%7s", key));
					rpBuilder.append("=\"");
					rpBuilder.append(String.format("%8s", romanPotAlignment.alignmentValues.get(key).toString()));
					rpBuilder.append("\"");
				}
				rpBuilder.append("/>\n");
				documentBuilder.append(rpBuilder.toString());
			}
			if (alignmentData.getType() == Type.AlignmentSequenceDescription) {
				documentBuilder.append("\t</TimeInterval>\n");
			}
		}
		documentBuilder.append("\n</xml>");
		return documentBuilder.toString();
	}

	public static boolean containsRomanPot(Map<IntervalOfValidity, RomanPotAlignment[]> alignments) {
		for (IntervalOfValidity iov : alignments.keySet()) {
			for (RomanPotAlignment rpAlignments : alignments.get(iov)) {
				Integer id = Integer.parseInt(rpAlignments.romanPotLabel);
				if (id < 100) {
					return true;
				}
			}
		}
		return false;
	}

	public static void sort(List<RomanPotAlignment> alignments) {
		final AlignmentComparator alignmentComparator = new AlignmentComparator();
		Collections.<RomanPotAlignment> sort(alignments, alignmentComparator);
	}

	static class AlignmentComparator implements Comparator<RomanPotAlignment> {
		@Override
		public int compare(RomanPotAlignment o1, RomanPotAlignment o2) {
			Integer id1 = Integer.parseInt(o1.romanPotLabel);
			Integer id2 = Integer.parseInt(o2.romanPotLabel);
			if (id1 < 100 && id2 >= 100) {
				id1 = 10 * id1 - 1;
			}
			if (id2 < 100 && id1 >= 100) {
				id2 = 10 * id2 - 1;
			}
			return (id1 == null || id2 == null) ? 0 : id1.compareTo(id2);
		}
	}

}
