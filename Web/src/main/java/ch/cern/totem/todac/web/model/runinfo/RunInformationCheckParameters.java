package ch.cern.totem.todac.web.model.runinfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import ch.cern.totem.todac.web.model.Constants;

public class RunInformationCheckParameters {

	protected final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);

	protected Long timestamp;

	public RunInformationCheckParameters(String timestamp) throws ParseException {
		this.timestamp = dateFormat.parse(timestamp).getTime();
	}

	public Long getTimestamp() {
		return timestamp;
	}
	
}
