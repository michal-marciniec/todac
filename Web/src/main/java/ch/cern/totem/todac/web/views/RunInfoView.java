package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.runinfo.FakeRunInformationPopulateForm;
import ch.cern.totem.todac.web.components.runinfo.RunInformationBrowser;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class RunInfoView extends TodacView {

	public static final TodacURL URL = new TodacURL("runinfo", "Run Information");

	public RunInfoView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new FakeRunInformationPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new RunInformationBrowser();
	}

}
