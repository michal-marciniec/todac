package ch.cern.totem.todac.web.components.lhclogging;

import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingData;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingDataEntry;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Table;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("serial")
public class LHCLoggingDataTable extends Table {

    private final static String COLUMN_VARIABLE = "Variable";
    private final static float COLUMN_VARIABLE_RATIO = 0.4f;
    private final static String COLUMN_STARTTIME = "Start time";
    private final static String COLUMN_ENDTIME = "End time";
    private final static float COLUMN_TIME_RATIO = 0.15f;
    private final static String COLUMN_VALUE = "Value";
    private final static float COLUMN_VALUE_RATIO = 0.1f;
    private final static String COLUMN_UNIT = "Unit";
    private final static float COLUMN_UNIT_RATIO = 0.05f;
    private final static String COLUMN_DESCRIPTION = "Description";
    private final static float COLUMN_DESCRIPTION_RATIO = 0.25f;
    private final static String WIDTH = "100%";
    private final static int PAGE_LENGTH = 25;
    protected LHCLoggingData contents;

    public LHCLoggingDataTable() {
        super();
        this.setImmediate(true);
        this.setSelectable(true);
        this.setMultiSelect(true);
        this.setWidth(WIDTH);
    }

    public LHCLoggingData getContents() {
        return contents;
    }

    public void setContents(LHCLoggingData contents) {
        this.contents = contents;

        final IndexedContainer container = new IndexedContainer();
        setUpContainer(container);

        for (LHCLoggingDataEntry entry : contents.getData()) {
            Item row = container.getItem(container.addItem());
            row.getItemProperty(COLUMN_VARIABLE).setValue(entry.getVariable());
            row.getItemProperty(COLUMN_STARTTIME).setValue(formatTimestamp(entry.getStartTimestamp()));
            row.getItemProperty(COLUMN_ENDTIME).setValue(formatTimestamp(entry.getEndTimestamp()));
            row.getItemProperty(COLUMN_VALUE).setValue(formatDouble(entry.getValue()));
            row.getItemProperty(COLUMN_UNIT).setValue(entry.getUnit());
            row.getItemProperty(COLUMN_DESCRIPTION).setValue(entry.getDescription());
        }

        this.setContainerDataSource(container);
        setUpColumns();
        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
        this.sort(new Object[]{COLUMN_VARIABLE, COLUMN_STARTTIME, COLUMN_ENDTIME}, new boolean[]{true, true, true});
    }

    protected void setUpContainer(IndexedContainer container) {
        container.addContainerProperty(COLUMN_VARIABLE, String.class, "");
        container.addContainerProperty(COLUMN_STARTTIME, String.class, "");
        container.addContainerProperty(COLUMN_ENDTIME, String.class, "");
        container.addContainerProperty(COLUMN_VALUE, String.class, "");
        container.addContainerProperty(COLUMN_UNIT, String.class, "");
        container.addContainerProperty(COLUMN_DESCRIPTION, String.class, "");
    }

    protected void setUpColumns() {
        this.setColumnHeaders(COLUMN_VARIABLE, COLUMN_STARTTIME, COLUMN_ENDTIME, COLUMN_VALUE, COLUMN_UNIT, COLUMN_DESCRIPTION);
        this.setVisibleColumns(COLUMN_VARIABLE, COLUMN_STARTTIME, COLUMN_ENDTIME, COLUMN_VALUE, COLUMN_UNIT, COLUMN_DESCRIPTION);
        this.setColumnExpandRatio(COLUMN_VARIABLE, COLUMN_VARIABLE_RATIO);
        this.setColumnExpandRatio(COLUMN_STARTTIME, COLUMN_TIME_RATIO);
        this.setColumnExpandRatio(COLUMN_ENDTIME, COLUMN_TIME_RATIO);
        this.setColumnExpandRatio(COLUMN_VALUE, COLUMN_VALUE_RATIO);
        this.setColumnExpandRatio(COLUMN_UNIT, COLUMN_UNIT_RATIO);
        this.setColumnExpandRatio(COLUMN_DESCRIPTION, COLUMN_DESCRIPTION_RATIO);
    }

    protected String formatTimestamp(long timestamp) {
        Date date = new Date(timestamp);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        final String formattedTimestamp = format.format(date);
        return formattedTimestamp;
    }

    protected String formatDouble(double doubleVal) {
        return String.valueOf(doubleVal);
    }

}