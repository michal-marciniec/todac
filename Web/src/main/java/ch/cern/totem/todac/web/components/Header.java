package ch.cern.totem.todac.web.components;

import ch.cern.totem.todac.web.MyVaadinUI;
import ch.cern.totem.todac.web.navigation.Dispatcher;
import com.vaadin.event.LayoutEvents;
import com.vaadin.ui.*;

@SuppressWarnings("serial")
public class Header extends CustomComponent {

	protected static final String STYLE_USER_BAR = "userbar-horizontallayout";
	protected static final String STYLE_RIGHT_PANEL = "userbar-rightpanel";
	protected static final String STYLE_USER_LABEL = "user-label";
	protected static final String STYLE_TITLE = "userbar-title";

	protected static final String APP_NAME = "TODAC";

	protected HorizontalLayout content;
	protected Label userLabel;

	public Header() {
		this.initializeComponents();
	}
	
	protected void initializeComponents() {
		final Dispatcher dispatcher = ((MyVaadinUI) UI.getCurrent()).getDispatcher();
		
		userLabel = new Label();
		userLabel.setStyleName(STYLE_USER_LABEL);

		HorizontalLayout rightPanel = new HorizontalLayout(userLabel);
		rightPanel.setStyleName(STYLE_RIGHT_PANEL);

        Label titleLabel = new Label(APP_NAME);
		titleLabel.setStyleName(STYLE_TITLE);
		
        HorizontalLayout leftPanel = new HorizontalLayout(titleLabel);
        leftPanel.addLayoutClickListener(new LayoutEvents.LayoutClickListener() {
            public void layoutClick(LayoutEvents.LayoutClickEvent event) {
                dispatcher.navigateToHome();
            }
        });

		content = new HorizontalLayout(leftPanel, rightPanel);
		content.setStyleName(STYLE_USER_BAR);
		setCompositionRoot(content);
	}

}
