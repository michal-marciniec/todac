package ch.cern.totem.todac.web.components.geometry;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.geometry.GeometryDao;
import ch.cern.totem.todac.web.model.geometry.GeometryData;
import ch.cern.totem.todac.web.model.geometry.GeometryParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class GeometryBrowser extends CustomComponent implements BrowserSwitcher {

    protected GeometryBrowserForm browserForm;

    public GeometryBrowser() {
        initComponents();
        switchToSearchBox();
    }

    protected void initComponents() {
        browserForm = new GeometryBrowserForm(this);
    }

    @Override
    public void switchToBrowser() {
        try {
            GeometryParameters query = browserForm.getParameters();
            GeometryData tableContent = getData(query);
            this.setCompositionRoot(new GeometryResults(tableContent, this));
        } catch (TodacException e) {
            Notificator.showError(e);
        } catch (TudasException e) {
            Notificator.showError(e);
        }
    }

    @Override
    public void switchToSearchBox() {
        this.setCompositionRoot(browserForm);
    }

    protected GeometryData getData(GeometryParameters query) throws TudasException {
        GeometryDao geometryDao = new GeometryDao();
        GeometryData geometry = geometryDao.fetch(query);

        return geometry;
    }
}
