package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;
import ch.cern.totem.todac.web.components.forms.validators.PositiveNumberValidator;

@SuppressWarnings("serial")
public class LsLengthSecRow extends OptionalInputRow {

	protected static final String CAPTION = "LsLengthSec";
	
	public LsLengthSecRow() {
		super(CAPTION);
		this.inputField.addValidator(new PositiveNumberValidator());
	}

}
