package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;
import ch.cern.totem.todac.web.components.forms.validators.NonNegativeNumberValidator;


@SuppressWarnings("serial")
public class VersionRow extends OptionalInputRow {

	protected static final String CAPTION = "Version";

	public VersionRow() {
		super(CAPTION);
		this.inputField.addValidator(new NonNegativeNumberValidator());
	}

}
