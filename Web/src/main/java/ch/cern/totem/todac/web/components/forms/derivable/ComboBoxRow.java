package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Label;

@SuppressWarnings("serial")
public class ComboBoxRow extends FormRowLayout {

	protected ComboBox comboBox;

	public ComboBoxRow(String caption) {
		this.initComponents(caption);
	}

	protected void initComponents(String caption) {
		comboBox = new ComboBox();
		comboBox.setFilteringMode(FilteringMode.STARTSWITH);
		comboBox.setImmediate(true);
		comboBox.setNullSelectionAllowed(false);
		addLeftComponent(new Label(caption));
		addRightComponent(comboBox);
	}

	public void addOption(String option) {
		comboBox.addItem(option);
	}

	public String getValue() {
		return comboBox.getValue().toString();
	}

	@Override
	public void reset() {
		comboBox.select(null);
	}

}
