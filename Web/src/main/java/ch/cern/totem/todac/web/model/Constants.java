package ch.cern.totem.todac.web.model;

public class Constants {
	
	public static final String DATE_FORMAT = "MM/dd/yy HH:mm:ss";
	public static final Double LS_LENGTH_SEC = 23.310779056686375;
	
}
