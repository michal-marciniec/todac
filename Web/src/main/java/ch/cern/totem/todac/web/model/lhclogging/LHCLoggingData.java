package ch.cern.totem.todac.web.model.lhclogging;

import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LHCLoggingData {

    List<LHCLoggingDataEntry> data;

    public LHCLoggingData() {
        this.data = new LinkedList<>();
    }

    public List<LHCLoggingDataEntry> getData() {
        return data;
    }

    public void add(String variable, Map<GeneralMeasurementIOV, GeneralMeasurement[]> measurements) {
        for (Map.Entry<GeneralMeasurementIOV, GeneralMeasurement[]> entry : measurements.entrySet()) {
            GeneralMeasurementIOV iov = entry.getKey();
            for (GeneralMeasurement gm : entry.getValue())
                data.add(new LHCLoggingDataEntry(variable, iov.startTime, iov.endTime, gm.value, gm.unit, gm.description));
        }
    }
}
