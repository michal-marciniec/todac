package ch.cern.totem.todac.web.model.lhclogging;

public class LHCLoggingDataEntry {
    protected String variable;
    protected long startTimestamp;
    protected long endTimestamp;
    protected double value;
    protected String unit;
    protected String description;

    public LHCLoggingDataEntry(String variable, long startTimestamp, long endTimestamp, double value, String unit, String description) {

        this.variable = variable;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.value = value;
        this.unit = unit;
        this.description = description;
    }

    public String getVariable() {
        return variable;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public double getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }

    public String getDescription() {
        return description;
    }
}
