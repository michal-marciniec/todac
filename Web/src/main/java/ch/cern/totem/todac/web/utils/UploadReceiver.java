package ch.cern.totem.todac.web.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.TodacException;

import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

@SuppressWarnings("serial")
public class UploadReceiver implements Receiver, SucceededListener {

	private static Logger logger = Logger.getLogger(UploadReceiver.class);

	protected static final String LOCATION = "uploads";
	protected static final int MAX_TRIES = 5;

	protected File file;
	protected SucceededListener succeededListener;

	public UploadReceiver(SucceededListener succeededListener) {
		this.succeededListener = succeededListener;
	}

	public OutputStream receiveUpload(String filename, String mimeType) {
		FileOutputStream fos = null;
		try {
			int tries = 0;
			do {
				String path = getRandomPath(filename);
				file = new File(path);
				++tries;
			} while (file.exists() && tries < MAX_TRIES);
			if (tries == MAX_TRIES) {
				logger.error("Unable to create file with random name");
			} else {
				file.createNewFile();
				fos = new FileOutputStream(file);
			}
		} catch (FileNotFoundException e) {
			logger.error("Unable to create stream with temporary upload file", e);
			Notificator.showError(new TodacException(ExceptionMessages.SERVER_ERROR, e));
		} catch (IOException e) {
			logger.error("Unable to access temporary upload file", e);
			Notificator.showError(new TodacException(ExceptionMessages.SERVER_ERROR, e));
		}
		return fos;
	}

	public void uploadSucceeded(SucceededEvent event) {
		succeededListener.uploadSucceeded(event);
	}

	public File getFile() {
		return file;
	}

	protected static String getRandomPath(String filename) {
		final Random random = new Random();

		File directory = new File(LOCATION);
		if (!directory.exists()) {
			directory.mkdir();
		}

		StringBuilder builder = new StringBuilder();
		builder.append(directory.getAbsolutePath());
		builder.append(File.separator);
		builder.append(String.valueOf(Math.round(random.nextFloat() * Integer.MAX_VALUE)));
		builder.append(filename);
		return builder.toString();
	}

}
