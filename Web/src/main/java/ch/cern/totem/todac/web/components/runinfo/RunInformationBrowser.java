package ch.cern.totem.todac.web.components.runinfo;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.runinfo.RunInformationDao;
import ch.cern.totem.todac.web.model.runinfo.RunInformationData;
import ch.cern.totem.todac.web.model.runinfo.RunInformationCheckParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class RunInformationBrowser extends CustomComponent implements BrowserSwitcher {

	protected Table dataTable;
	protected RunInformationBrowserForm browserForm;
	protected RunInformationResults display;
	protected VerticalLayout layout;

	public RunInformationBrowser() {
		try {
			this.initComponents();
			this.switchToSearchBox();
		} catch (TudasException e) {
			Notificator.showError(e);
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}

	protected void initComponents() throws TudasException, TodacException {
		RunInformationDao runInformationDao = new RunInformationDao();
		RunInformationData data = runInformationDao.downloadAllRunsDetailed();
		dataTable = new RunInformationDataTable(data);
		browserForm = new RunInformationBrowserForm(this);
		display = new RunInformationResults(this, data);
		
		layout = new VerticalLayout(dataTable, browserForm, display);
		setCompositionRoot(layout);
	}

	@Override
	public void switchToBrowser() {
		try {
			RunInformationCheckParameters parameters = browserForm.getParameters();
			display.search(parameters);
			browserForm.setVisible(false);
			display.setVisible(true);
		} catch (InvalidArgumentException e) {
			Notificator.showError(e);
		}
	}

	@Override
	public void switchToSearchBox() {
		browserForm.setVisible(true);
		display.setVisible(false);
	}

}
