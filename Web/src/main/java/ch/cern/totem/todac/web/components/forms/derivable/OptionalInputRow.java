package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.TextField;

@SuppressWarnings("serial")
public class OptionalInputRow extends FormRowLayout {

	protected static final boolean DEFAULT_VALUE = false;
	
    protected CheckBox checkBox;
    protected TextField inputField;

    public OptionalInputRow(String caption) {
        this.initComponents(caption);
    }

    protected void initComponents(String caption) {
        checkBox = new CheckBox();
        checkBox.setCaption(caption);
        checkBox.setValue(DEFAULT_VALUE);
        checkBox.addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                setSelection(checkBox.getValue());
            }
        });

        inputField = new TextField();
        inputField.setEnabled(DEFAULT_VALUE);
        inputField.setImmediate(true);
        addLeftComponent(checkBox);
        addRightComponent(inputField);
    }

    public String getValue() {
        return inputField.getValue();
    }

    public boolean isChecked() {
        return checkBox.getValue();
    }

    public void setSelection(boolean value) {
        checkBox.setValue(value);
        inputField.setEnabled(checkBox.getValue());
        inputField.setImmediate(checkBox.getValue());
    }
    
    public void reset() {
        setSelection(DEFAULT_VALUE);
        inputField.setValue("");
    }

}
