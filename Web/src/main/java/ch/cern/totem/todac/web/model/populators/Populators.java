package ch.cern.totem.todac.web.model.populators;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.alignment.AlignmentPopulator;
import ch.cern.totem.todac.populators.geometry.GeometryPopulator;
import ch.cern.totem.todac.populators.lhclogging.LHCLoggingPopulator;
import ch.cern.totem.todac.populators.luminosity.LuminosityPopulator;
import ch.cern.totem.todac.populators.optics.OpticsPopulator;
import ch.cern.totem.todac.populators.runinfo.RunInfoPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.LuminosityManager;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class Populators {
	
	protected static DatabaseAccessProvider getDatabaseAccessProvider() throws TudasException {
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		databaseAccessProvider.initialize();
		return databaseAccessProvider;
	}

	public static AlignmentPopulator getAlignmentPopulator() throws TudasException {
		RomanPotManager romanPotManager = getDatabaseAccessProvider().getRomanPotManager();
		return new AlignmentPopulator(romanPotManager);
	}
	
	public static LuminosityPopulator getLuminosityPopulator() throws TudasException {
		LuminosityManager luminosityManager = getDatabaseAccessProvider().getLuminosityManager();
		return new LuminosityPopulator(luminosityManager);
	}
	
	public static RunInfoPopulator getRunInfoPopulator() throws TudasException, TodacException {
		RunInformationManager runInformationManager = getDatabaseAccessProvider().getRunInformationManager();
		RunInfoPopulator populator = new RunInfoPopulator(runInformationManager);
		//populator.initialize();
		return populator;
	}
	
	public static LHCLoggingPopulator getLhcLoggingPopulator() throws TudasException {
		return new LHCLoggingPopulator(getDatabaseAccessProvider());
		//initialize?
	}
	
	public static GeometryPopulator getGeometryPopulator() throws TudasException {
		return new GeometryPopulator(getDatabaseAccessProvider());
		//initialize?
	}
	
	public static OpticsPopulator getOpticsPopulator() throws TudasException {
		return new OpticsPopulator(getDatabaseAccessProvider());
		//initialize?
	}
	
}
