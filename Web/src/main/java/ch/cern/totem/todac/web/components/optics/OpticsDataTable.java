package ch.cern.totem.todac.web.components.optics;

import ch.cern.totem.todac.web.model.optics.OpticsData;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Table;

import java.util.Map;

@SuppressWarnings("serial")
public class OpticsDataTable extends Table {

    private final static int PAGE_LENGTH = 25;
    private final static String COLUMN_ID = "ID";
    private final static String COLUMN_VALUE = "Value";
    protected OpticsData contents;

    public OpticsDataTable() {
        super();
        this.setImmediate(true);
        this.setSelectable(true);
        this.setMultiSelect(true);
    }

    public OpticsData getContents() {
        return contents;
    }

    public void setContents(OpticsData contents) {
        this.contents = contents;

        final IndexedContainer container = new IndexedContainer();
        container.addContainerProperty(COLUMN_ID, String.class, "");
        container.addContainerProperty(COLUMN_VALUE, String.class, "");

        for (Map.Entry<String, Double> entry : contents.getMap().entrySet()) {
            Item row = container.getItem(container.addItem());
            row.getItemProperty(COLUMN_ID).setValue(entry.getKey());
            String formattedValue = entry.getValue().toString();
            row.getItemProperty(COLUMN_VALUE).setValue(formattedValue);
        }

        this.setContainerDataSource(container);
        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
        setUpColumns();
    }

    protected void setUpColumns() {
        this.setColumnHeaders(COLUMN_ID, COLUMN_VALUE);
        this.setVisibleColumns(COLUMN_ID, COLUMN_VALUE);
    }
}