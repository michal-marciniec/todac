package ch.cern.totem.todac.web.components.optics;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.optics.OpticsDao;
import ch.cern.totem.todac.web.model.optics.OpticsData;
import ch.cern.totem.todac.web.model.optics.OpticsParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class OpticsBrowser extends CustomComponent implements BrowserSwitcher {

    protected OpticsBrowserForm searchBox;

    public OpticsBrowser() {
        initComponents();
        switchToSearchBox();
    }

    protected void initComponents() {
        this.searchBox = new OpticsBrowserForm(this);
    }

    @Override
    public void switchToBrowser() {
        OpticsData contents = null;
        try {
            OpticsParameters query = searchBox.getParameters();
            contents = new OpticsDao().fetch(query);
        } catch (TudasException e) {
            Notificator.showError(e);
        } catch (TodacException e) {
            Notificator.showError(e);
        }

        this.setCompositionRoot(new OpticsResults(contents, this));
    }

    @Override
    public void switchToSearchBox() {
        this.setCompositionRoot(searchBox);
    }
}
