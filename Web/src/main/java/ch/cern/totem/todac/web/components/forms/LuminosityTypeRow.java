package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.FormRowLayout;
import ch.cern.totem.todac.web.model.luminosity.LuminosityType;

import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;

@SuppressWarnings("serial")
public class LuminosityTypeRow extends FormRowLayout {

	protected static final String CAPTION = "Luminosity type";
	
	protected OptionGroup luminosityTypeGroup;
	protected Object defaultSelection;
	
	public LuminosityTypeRow() {
		this.initComponents();
	}
	
	protected void initComponents() {
		luminosityTypeGroup = new OptionGroup();
		Object idLs = luminosityTypeGroup.addItem();
		luminosityTypeGroup.setItemCaption(idLs, LuminosityType.LuminosityByLs.toString());
		defaultSelection = idLs;
		luminosityTypeGroup.select(defaultSelection);
		Object idLsXing = luminosityTypeGroup.addItem();
		luminosityTypeGroup.setItemCaption(idLsXing, LuminosityType.LuminosityByLsXing.toString());
		
		addLeftComponent(new Label(CAPTION));
		addRightComponent(luminosityTypeGroup);
	}
	
	public LuminosityType getValue() {
		Object itemId = luminosityTypeGroup.getValue();
		String itemCaption = luminosityTypeGroup.getItemCaption(itemId);
		return LuminosityType.valueOf(itemCaption.toString());
	}
	
	public void reset() {
		luminosityTypeGroup.select(defaultSelection);
	}
		
	
}
