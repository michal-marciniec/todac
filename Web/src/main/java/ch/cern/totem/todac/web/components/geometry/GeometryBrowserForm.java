package ch.cern.totem.todac.web.components.geometry;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.LabelRow;
import ch.cern.totem.todac.web.components.forms.RunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.VersionRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.forms.derivable.InputRow;
import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;
import ch.cern.totem.todac.web.model.geometry.GeometryParameters;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class GeometryBrowserForm extends FormLayout {

	protected BrowserSwitcher switcher;
	protected RunSelectRow runNumberRow;
	protected InputRow labelRow;
	protected OptionalInputRow versionRow;
	protected SubmitRow submitRow;

	public GeometryBrowserForm(BrowserSwitcher switcher) {
        this.switcher = switcher;
        initComponents();
		this.addComponents(runNumberRow, labelRow, versionRow, submitRow);
	}

	protected void initComponents() {
		this.runNumberRow = new RunSelectRow();
		this.labelRow = new LabelRow();
		this.versionRow = new VersionRow();
		this.submitRow = new SubmitRow();
		this.submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		this.submitRow.addRightClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				switcher.switchToBrowser();
			}
		});
	}

	protected GeometryParameters getParameters() throws InvalidArgumentException {
		try {
			Integer runNumber = Integer.parseInt(runNumberRow.getValue());
			String label = labelRow.getValue();
			if (versionRow.isChecked()) {
				int version = Integer.parseInt(versionRow.getValue());
				return new GeometryParameters(runNumber, label, version);
			} else
				return new GeometryParameters(runNumber, label);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}
}
