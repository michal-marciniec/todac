package ch.cern.totem.todac.web.converters;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;

import java.util.Map;

public class GeometryConverter {

    protected static final String XML_DOCUMENT_TAG = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    protected static final String DDDEFINITION_START_TAG = "<DDDefinition xmlns=\"http://www.cern.ch/cms/DDL\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.cern.ch/cms/DDL ../../DDLSchema/DDLSchema.xsd\">";
    protected static final String DDDEFINITION_END_TAG = "</DDDefinition>";
    protected static final String CONSTANTS_SECTION_START_TAG = "<ConstantsSection label=\"RP_Dist_Beam_Cent.xml\">";
    protected static final String CONSTANTS_SECTION_END_TAG = "</ConstantsSection>";


    public static String convert(Map<String, Double> geometry) throws InvalidArgumentException {
		StringBuilder documentBuilder = new StringBuilder();

		documentBuilder.append(XML_DOCUMENT_TAG).append("\n");
		documentBuilder.append(DDDEFINITION_START_TAG).append("\n\t");
		documentBuilder.append(CONSTANTS_SECTION_START_TAG);

		for (Map.Entry<String, Double> entry : geometry.entrySet()) {
            documentBuilder.append("\n\t\t").append("<Constant name=").append("\"" + entry.getKey() + "\" ").append("value=\"" + entry.getValue() + "\" />");
        }

        documentBuilder.append("\n\t");
        documentBuilder.append(CONSTANTS_SECTION_END_TAG).append("\n");
		documentBuilder.append(DDDEFINITION_END_TAG);
		return documentBuilder.toString();
	}

}
