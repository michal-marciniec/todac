package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.InputRow;
import ch.cern.totem.todac.web.components.forms.validators.IntegerValidator;
import ch.cern.totem.todac.web.components.forms.validators.PositiveNumberValidator;

@SuppressWarnings("serial")
public class RunRow extends InputRow {

	protected static final String CAPTION = "Run number";

	public RunRow() {
		super(CAPTION);
		this.inputField.addValidator(new IntegerValidator());
		this.inputField.addValidator(new PositiveNumberValidator());
	}

}
