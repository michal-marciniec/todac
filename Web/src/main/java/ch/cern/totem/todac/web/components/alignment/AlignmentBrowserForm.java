package ch.cern.totem.todac.web.components.alignment;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.EndRunSelectRow;
import ch.cern.totem.todac.web.components.forms.LabelRow;
import ch.cern.totem.todac.web.components.forms.StartRunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.VersionRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.alignment.AlignmentDownloadParameters;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class AlignmentBrowserForm extends FormLayout {

	protected BrowserSwitcher browserSwitcher;

	protected StartRunSelectRow startRunRow;
	protected EndRunSelectRow endRunRow;
	protected LabelRow labelRow;
	protected VersionRow versionRow;
	protected SubmitRow submitRow;

	public AlignmentBrowserForm(BrowserSwitcher browserSwitcher) {
		this.browserSwitcher = browserSwitcher;
		this.initComponents();
	}

	protected void initComponents() {
		startRunRow = new StartRunSelectRow();
		endRunRow = new EndRunSelectRow();
		labelRow = new LabelRow();
		versionRow = new VersionRow();
		submitRow = new SubmitRow();
		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				browserSwitcher.switchToBrowser();
			}
		});

		addComponent(startRunRow);
		addComponent(endRunRow);
		addComponent(labelRow);
		addComponent(versionRow);
		addComponent(submitRow);
	}

	public AlignmentDownloadParameters getParameters() throws InvalidArgumentException {
		try {
			Integer startRun = Integer.parseInt(startRunRow.getValue());
			Integer endRun = Integer.parseInt(endRunRow.isChecked() ? endRunRow.getValue() : startRunRow.getValue());
			String label = labelRow.getValue();
			Integer version = versionRow.isChecked() ? Integer.parseInt(versionRow.getValue()) : -1;
			return new AlignmentDownloadParameters(startRun, endRun, label, version);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
