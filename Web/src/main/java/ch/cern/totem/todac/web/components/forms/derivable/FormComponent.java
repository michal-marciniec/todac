package ch.cern.totem.todac.web.components.forms.derivable;

public interface FormComponent {
	void reset();
}
