package ch.cern.totem.todac.web.utils;

public interface DownloadResourceProvider {
    String getResource();
    String getFilename();
}