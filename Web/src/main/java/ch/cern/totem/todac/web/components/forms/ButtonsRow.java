package ch.cern.totem.todac.web.components.forms;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;

import ch.cern.totem.todac.web.components.forms.derivable.FormRowLayout;

@SuppressWarnings("serial")
public class ButtonsRow extends FormRowLayout {

	protected static final String STYLE_LEFT_CONTENT = "form-left-content-button";

	protected Button leftButton;
	protected Button rightButton;

	public ButtonsRow(String leftButtonCaption, String rightButtonCaption) {
		this.initComponents(leftButtonCaption, rightButtonCaption);
	}

	protected void initComponents(String leftButtonCaption, String rightButtonCaption) {
		if (leftButtonCaption != null) {
			leftButton = new Button(leftButtonCaption);
			addLeftComponent(leftButton);
		}
		if (rightButtonCaption != null) {
			rightButton = new Button(rightButtonCaption);
			addRightComponent(rightButton);
		}
		leftContent.setStyleName(STYLE_LEFT_CONTENT);
	}

	public void addLeftClickListener(ClickListener clickListener) {
		leftButton.addClickListener(clickListener);
	}

	public void addRightClickListener(ClickListener clickListener) {
		rightButton.addClickListener(clickListener);
	}

	public Button getLeftButton() {
		return leftButton;
	}

	public Button getRightButton() {
		return rightButton;
	}

	@Override
	public void reset() {

	}

}
