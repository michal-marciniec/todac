package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;


@SuppressWarnings("serial")
public class OptionalLabelRow extends OptionalInputRow {

	protected static final String CAPTION = "Label";
			
	public OptionalLabelRow() {
		super(CAPTION);
	}

}
