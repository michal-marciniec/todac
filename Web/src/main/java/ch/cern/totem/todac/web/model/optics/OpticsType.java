package ch.cern.totem.todac.web.model.optics;

public enum OpticsType {

    BETA_STAR("Beta star"), CURRENTS("Currents"), MAGNET_STRENGTH("Magnet strength");

    private String typeName;

    private OpticsType(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return typeName;
    }
}
