package ch.cern.totem.todac.web.components.luminosity;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsData;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsXingData;
import ch.cern.totem.todac.web.model.luminosity.LuminosityDao;
import ch.cern.totem.todac.web.model.luminosity.LuminosityDownloadParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class LuminosityBrowser extends CustomComponent implements BrowserSwitcher {

	protected LuminosityBrowserForm browserForm;

	public LuminosityBrowser() {
		this.initComponents();
		this.switchToSearchBox();
	}

	protected void initComponents() {
		browserForm = new LuminosityBrowserForm(this);
	}

	@Override
	public void switchToBrowser() {
		try {
			LuminosityDownloadParameters parameters = browserForm.getParameters();
			LuminosityDao luminosityDao = new LuminosityDao();
			switch (parameters.getType()) {
				case LuminosityByLs:
					LuminosityByLsData luminosityByLsData = luminosityDao.downloadLuminosityByLs(parameters);
					LuminosityByLsResults displayByLs = new LuminosityByLsResults(this, luminosityByLsData);
					setCompositionRoot(displayByLs);
					break;
				case LuminosityByLsXing:
					LuminosityByLsXingData luminosityByLsXingData = luminosityDao.downloadLuminosityByLsXing(parameters);
					LuminosityByLsXingResults displayByLsXing = new LuminosityByLsXingResults(this, luminosityByLsXingData);
					setCompositionRoot(displayByLsXing);
					break;
			}
		} catch (InvalidArgumentException e) {
			Notificator.showError(e);
		} catch (TudasException e) {
			Notificator.showError(e);
		}
	}

	@Override
	public void switchToSearchBox() {
		setCompositionRoot(browserForm);
	}

}
