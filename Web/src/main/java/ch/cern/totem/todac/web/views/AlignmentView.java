package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.alignment.AlignmentBrowser;
import ch.cern.totem.todac.web.components.alignment.AlignmentPopulateForm;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class AlignmentView extends TodacView {

	public static final TodacURL URL = new TodacURL("alignment", "Alignments");

	public AlignmentView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new AlignmentPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new AlignmentBrowser();
	}

}
