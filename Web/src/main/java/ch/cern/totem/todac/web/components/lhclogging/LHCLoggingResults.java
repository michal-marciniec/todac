package ch.cern.totem.todac.web.components.lhclogging;

import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.BrowserButtonsRow;
import ch.cern.totem.todac.web.components.forms.ButtonsRow;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingData;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingDataEntry;
import ch.cern.totem.todac.web.utils.DownloadResourceProvider;
import ch.cern.totem.todac.web.utils.Downloader;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

public class LHCLoggingResults extends VerticalLayout implements DownloadResourceProvider {

    private static final String DOWNLOAD_FILENAME = "variables.csv";
    protected BrowserSwitcher switcher;
    protected LHCLoggingDataTable dataTable;
    protected ButtonsRow buttonsRow;
    protected Downloader downloader;

    public LHCLoggingResults(LHCLoggingData contents, BrowserSwitcher switcher) {
        super();
        this.switcher = switcher;
        initComponents();
        this.addComponents(dataTable, buttonsRow);
        setContents(contents);
    }

    protected void initComponents() {
        dataTable = new LHCLoggingDataTable();
        buttonsRow = new BrowserButtonsRow();
        buttonsRow.addLeftClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                switcher.switchToSearchBox();
            }
        });
        downloader = new Downloader(this);
        downloader.extend(buttonsRow.getRightButton());
    }

    public LHCLoggingData getContents() {
        return dataTable.getContents();
    }

    public void setContents(LHCLoggingData contents) {
        dataTable.setContents(contents);
    }

    @Override
    public String getResource() {
        StringBuilder builder = new StringBuilder();
        LHCLoggingData contents = getContents();
        for (LHCLoggingDataEntry entry : contents.getData())
            builder.append(entry.getVariable()).append(",").append(entry.getStartTimestamp()).append(",").append(entry.getEndTimestamp())
                    .append(",").append(entry.getValue()).append(",").append(entry.getUnit()).append(",").append(entry.getDescription()).append("\n");
        return builder.toString();
    }

    @Override
    public String getFilename() {
        return DOWNLOAD_FILENAME;
    }

}
