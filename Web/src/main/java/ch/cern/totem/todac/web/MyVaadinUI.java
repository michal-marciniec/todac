package ch.cern.totem.todac.web;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.NavigationMenu;
import ch.cern.totem.todac.web.navigation.Dispatcher;
import ch.cern.totem.todac.web.views.*;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.annotation.WebServlet;
import java.io.InputStream;
import java.util.Properties;

@Theme("mytheme")
@SuppressWarnings("serial")
public class MyVaadinUI extends UI {
	
	private static Logger logger = Logger.getLogger(MyVaadinUI.class);
	private static void setUpLogger() {
		try {
			InputStream propertiesStream = MyVaadinUI.class.getResourceAsStream("/log4j.properties");
			Properties prop = new Properties();
			prop.load(propertiesStream);
			PropertyConfigurator.configure(prop);
		} catch (Exception e) {
			logger.warn("Error while setting up logger.", e);
		}
	}
	
    static {
        try {
        	setUpLogger();
            DatabaseAccessProvider.getInstance().initialize();
            logger.info("TUDAS connection established");
        } catch (Exception e) {
            logger.error("Initialization error", e);
        }
    }

    protected Dispatcher dispatcher;

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = MyVaadinUI.class, widgetset = "ch.cern.totem.todac.web.AppWidgetSet")
    public static class Servlet extends VaadinServlet {
    }
    
    @Override
    protected void init(VaadinRequest request) {
    	getPage().setTitle("TODAC");
    	
    	final Navigator navigator = new Navigator(this, this);
        dispatcher = new Dispatcher(navigator);

        navigator.addView("", new HomeView(new NavigationComponent(NavigationMenu.build())));
        navigator.addView(HomeView.URL, new HomeView(new NavigationComponent(NavigationMenu.build())));
        navigator.addView(AlignmentView.URL.getUrl(), new AlignmentView(NavigationComponent.build()));
        navigator.addView(GeometryView.URL.getUrl(), new GeometryView(NavigationComponent.build()));
        navigator.addView(LHCLoggingView.URL.getUrl(), new LHCLoggingView(NavigationComponent.build()));
        navigator.addView(LuminosityView.URL.getUrl(), new LuminosityView(NavigationComponent.build()));
        navigator.addView(OpticsView.URL.getUrl(), new OpticsView(NavigationComponent.build()));
        navigator.addView(RunInfoView.URL.getUrl(), new RunInfoView(NavigationComponent.build()));
    }

    public Dispatcher getDispatcher() {
        return dispatcher;
    }

}
