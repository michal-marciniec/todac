package ch.cern.totem.todac.web.components.forms;

import com.vaadin.ui.Button;

@SuppressWarnings("serial")
public class SubmitRow extends ButtonsRow {

	private final static String LEFT_CAPTION = "Clear";
	private final static String RIGHT_CAPTION = "Ok";

	protected Button button;

	public SubmitRow() {
		super(LEFT_CAPTION, RIGHT_CAPTION);
	}

	@Override
	public void reset() {

	}

}
