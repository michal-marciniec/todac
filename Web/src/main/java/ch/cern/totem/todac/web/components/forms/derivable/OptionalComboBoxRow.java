package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;

@SuppressWarnings("serial")
public class OptionalComboBoxRow extends FormRowLayout {

	protected static final boolean DEFAULT_VALUE = false;

	protected CheckBox checkBox;
	protected ComboBox comboBox;

	public OptionalComboBoxRow(String caption) {
		this.initComponents(caption);
	}

	protected void initComponents(String caption) {
		checkBox = new CheckBox();
		checkBox.setCaption(caption);
		checkBox.setValue(DEFAULT_VALUE);
		checkBox.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				setSelection(checkBox.getValue());
			}
		});

		comboBox = new ComboBox();
		comboBox.setEnabled(DEFAULT_VALUE);
		comboBox.setFilteringMode(FilteringMode.STARTSWITH);
		comboBox.setImmediate(true);
		comboBox.setNullSelectionAllowed(false);

		addLeftComponent(checkBox);
		addRightComponent(comboBox);
	}

	public void addOption(String option) {
		comboBox.addItem(option);
	}

	public String getValue() {
		return comboBox.getValue().toString();
	}

	public boolean isChecked() {
		return checkBox.getValue();
	}

	public void setSelection(boolean value) {
		checkBox.setValue(value);
		comboBox.setEnabled(checkBox.getValue());
	}

	public void reset() {
		comboBox.select(null);
		setSelection(DEFAULT_VALUE);
	}
	
}
