package ch.cern.totem.todac.web.components.runinfo;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.todac.web.model.runinfo.RunInformationData;
import ch.cern.totem.tudas.tudasUtil.structure.RunInformation;

import com.vaadin.ui.Table;

@SuppressWarnings("serial")
public class RunInformationDataTable extends Table {

    protected static final int PAGE_LENGTH = 25;
	protected static final String[] COLUMNS = { "Run number", "First event", "Last event", "Number of events", "Description" };
	
	protected RunInformationData runInformation;
	protected int id;

	public RunInformationDataTable(RunInformationData runInformation) {
		this.runInformation = runInformation;
		this.id = 0;
		this.initComponents();
	}

	protected void initComponents() {
		setImmediate(true);
		setSelectable(true);
		setMultiSelect(true);

		addContainerProperty(COLUMNS[0], Integer.class, -1);
		addContainerProperty(COLUMNS[1], String.class, "");
		addContainerProperty(COLUMNS[2], String.class, "");
		addContainerProperty(COLUMNS[3], Long.class, -1L);
		addContainerProperty(COLUMNS[4], String.class, "");
		
		final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		for (RunInformation runInfo : runInformation.getRunInformation()) {
			Object[] values = new Object[COLUMNS.length];
			values[0] = runInfo.runNumber;
			values[1] = dateFormat.format(new Timestamp(runInfo.firstEventTime));
			values[2] = dateFormat.format(new Timestamp(runInfo.lastEventTime));
			values[3] = runInfo.numberOfEvents;
			values[4] = runInfo.description;
			addItem(values, getNewId());
		}

        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
	}
	
	protected Integer getNewId() {
		return new Integer(id++);
	}

}
