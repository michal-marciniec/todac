package ch.cern.totem.todac.web.model.runinfo;

import java.util.Arrays;
import java.util.Comparator;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.populators.runinfo.RunInfoPopulator;
import ch.cern.totem.todac.web.model.populators.Populators;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.RunInformation;

public class RunInformationDao {
	
	private static Logger logger = Logger.getLogger(RunInformationDao.class);
	
	protected RunInfoPopulator runInfoPopulator;
	
	public RunInformationDao() throws TudasException, TodacException {
		this.runInfoPopulator = Populators.getRunInfoPopulator();
	}
	
	public MeasurableProgress getMeasurableObject() {
		return runInfoPopulator;
	} 

	public int[] downloadAllRuns() {
		logger.debug("Downloading run list");
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		try {
			databaseAccessProvider.initialize();
			RunInformationManager runInformationManager = databaseAccessProvider.getRunInformationManager();
			int[] runs = runInformationManager.listAllRuns();
			Arrays.sort(runs);
			return runs;
		} catch (TudasException e) {
			Notificator.showError(e);
		}
		logger.debug("No runs found");
		return null;
	}

	public RunInformationData downloadAllRunsDetailed() {
		logger.debug("Downloading detailed run list");
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		try {
			databaseAccessProvider.initialize();
			RunInformationManager runInformationManager = databaseAccessProvider.getRunInformationManager();
			RunInformation[] runInformation = runInformationManager.listAllRunsDetailed();
			final Comparator<RunInformation> runInformationComparator = new RunInformationComparator();
			Arrays.sort(runInformation, runInformationComparator);
			return new RunInformationData(runInformation);
		} catch (TudasException e) {
			Notificator.showError(e);
		}
		logger.debug("No runs found");
		return null;
	}
	
	public void uploadRunInfo(RunInformationUploadParameters parameters){
		logger.debug("Uploading run info (" + parameters.getRunNumber() + ")");
		try {
			runInfoPopulator.saveRunInfo(parameters.getRunNumber(), parameters.getDescription());
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}
	
	static class RunInformationComparator implements Comparator<RunInformation> {
		@Override
		public int compare(RunInformation arg0, RunInformation arg1) {
			Integer runNumber0 = new Integer(arg0.runNumber);
			Integer runNumber1 = new Integer(arg1.runNumber);
			return runNumber0.compareTo(runNumber1);
		}
	}

}
