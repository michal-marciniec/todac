package ch.cern.totem.todac.web.model.luminosity;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.progressbar.MeasurableProgress;
import ch.cern.totem.todac.populators.luminosity.LuminosityPopulator;
import ch.cern.totem.todac.web.converters.LuminosityConverter;
import ch.cern.totem.todac.web.model.populators.Populators;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.LuminosityManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;

public class LuminosityDao {

	private static Logger logger = Logger.getLogger(LuminosityDao.class);

	protected LuminosityPopulator luminosityPopulator;

	public LuminosityDao() throws TudasException {
		this.luminosityPopulator = Populators.getLuminosityPopulator();
	}

	public MeasurableProgress getMeasurableObject() {
		return luminosityPopulator;
	}

	public LuminosityByLsData downloadLuminosityByLs(LuminosityDownloadParameters parameters) {
		logger.debug("Downloading LuminosityByLs (" + parameters.getLabel() + ")");
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		try {
			databaseAccessProvider.initialize();
			LuminosityManager luminosityManager = databaseAccessProvider.getLuminosityManager();
			LuminosityByLs[] luminosity = null;
			if (parameters.getVersion() < 0) {
				luminosity = luminosityManager.loadLuminosityByLs(parameters.getTimestamp(), parameters.getLabel());
			} else {
				luminosity = luminosityManager.loadLuminosityByLs(parameters.getTimestamp(), parameters.getLabel(), parameters.getVersion());
			}
			LuminosityConverter.sort(luminosity);
			return new LuminosityByLsData(luminosity, parameters.getLabel());
		} catch (TudasException e) {
			Notificator.showError(e);
		}
		logger.warn("No LuminosityByLs found (" + parameters.getLabel() + ")");
		return new LuminosityByLsData();
	}

	public LuminosityByLsXingData downloadLuminosityByLsXing(LuminosityDownloadParameters parameters) {
		logger.debug("Downloading LuminosityByLsXing (" + parameters.getLabel() + ")");
		DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
		try {
			databaseAccessProvider.initialize();
			LuminosityManager luminosityManager = databaseAccessProvider.getLuminosityManager();
			LuminosityByLsXing[] luminosity = null;
			if (parameters.getVersion() < 0) {
				luminosity = luminosityManager.loadLuminosityByLsXing(parameters.getTimestamp(), parameters.getLabel());
			} else {
				luminosity = luminosityManager.loadLuminosityByLsXing(parameters.getTimestamp(), parameters.getLabel(), parameters.getVersion());
			}
			LuminosityConverter.sort(luminosity);
			return new LuminosityByLsXingData(luminosity, parameters.getLabel());
		} catch (TudasException e) {
			Notificator.showError(e);
		}
		logger.warn("No LuminosityByLsXing found (" + parameters.getLabel() + ")");
		return new LuminosityByLsXingData();
	}

	public void uploadLuminosity(LuminosityUploadParameters parameters) {
		logger.debug("Uploading " + parameters.getType() + "(" + parameters.getLabel() + ")");
		try {
			luminosityPopulator.setLabel(parameters.getLabel());
			luminosityPopulator.setLsLengthSec(parameters.getlsLengthSec());
			switch (parameters.getType()) {
				case LuminosityByLs:
					luminosityPopulator.saveLuminosityByLs(parameters.getFilepath());
					break;
				case LuminosityByLsXing:
					luminosityPopulator.saveLuminosityByLsXing(parameters.getFilepath());
					break;
			}
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}

}
