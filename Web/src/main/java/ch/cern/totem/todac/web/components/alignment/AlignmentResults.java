package ch.cern.totem.todac.web.components.alignment;

import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.BrowserButtonsRow;
import ch.cern.totem.todac.web.model.alignment.AlignmentData;
import ch.cern.totem.todac.web.utils.DownloadResourceProvider;
import ch.cern.totem.todac.web.utils.Downloader;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class AlignmentResults extends VerticalLayout implements DownloadResourceProvider {

    protected BrowserSwitcher browserSwitcher;
    protected Downloader downloader;
    protected AlignmentDataTable dataTable;
    protected BrowserButtonsRow buttonsRow;
    protected String filename;

    public AlignmentResults(BrowserSwitcher browserSwitcher, AlignmentData alignments) {
        this.browserSwitcher = browserSwitcher;
        this.initComponents(alignments);
    }

    protected void initComponents(AlignmentData alignments) {
        dataTable = new AlignmentDataTable(alignments);

        buttonsRow = new BrowserButtonsRow();
        buttonsRow.addLeftClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                browserSwitcher.switchToSearchBox();
            }
        });
        setFilename(alignments.getLabel());

        downloader = new Downloader(this);
        downloader.extend(buttonsRow.getRightButton());

        addComponent(dataTable);
        addComponent(buttonsRow);
    }

    @Override
    public String getResource() {
        String contents = dataTable.getContents().getFile();
        return contents;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    protected void setFilename(String label) {
        filename = label + ".xml";
    }
}
