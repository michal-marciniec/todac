package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.HomeBox;
import ch.cern.totem.todac.web.components.NavigationComponent;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class HomeView extends CustomComponent implements View {

	public static final String URL = "home";

	protected NavigationComponent navigationView;
	protected CustomComponent home;

	public HomeView(NavigationComponent navigationView) {
		this.navigationView = navigationView;
		this.home = null;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (home == null) {
			home = new HomeBox();
			navigationView.setContent(home);
		}
		setCompositionRoot(navigationView);
	}

}
