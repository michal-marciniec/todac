package ch.cern.totem.todac.web.model.runinfo;

import ch.cern.totem.tudas.tudasUtil.structure.RunInformation;

public class RunInformationData {

	protected RunInformation[] runInformation;
	
	public RunInformationData(RunInformation[] runInformation){
		this.runInformation = runInformation;
	}
	
	public RunInformation[] getRunInformation() {
		return runInformation;
	}
	
}
