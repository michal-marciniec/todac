package ch.cern.totem.todac.web.components.lhclogging.components;

import com.vaadin.data.Item;
import com.vaadin.ui.Table;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class LHCVariablesTableBuilder {

    protected static final String COLUMN_NAME = "Requested Variables";
    protected static final String WIDTH = "300px";
    protected static final int PAGE_LENGTH = 15;
    protected Table table;

    public LHCVariablesTableBuilder() {
        super();
        initComponents();
    }

    public Table getTable() {
        return table;
    }

    protected void initComponents() {
        this.table = new Table();
        this.table.setImmediate(true);
        this.table.setMultiSelect(true);
        this.table.setSelectable(true);
        this.table.setFooterVisible(false);
        this.table.addContainerProperty(COLUMN_NAME, String.class, "");
        this.table.setVisibleColumns(COLUMN_NAME);
        this.table.setColumnHeaders(COLUMN_NAME);
        this.table.setPageLength(PAGE_LENGTH);
        this.table.setWidth(WIDTH);
    }

    public void add(String variable) {
        if(variable == null || variable.isEmpty())
            return;

        if (!table.containsId(variable)) {
            Item newItem = table.addItemAfter(null, variable);
            newItem.getItemProperty(COLUMN_NAME).setValue(variable);
        }
    }

    public void add(Collection<String> variables) {
        for (String var : variables)
            add(var);
    }

    public void edit(String oldValue, String newValue) {
        remove(oldValue);
        add(newValue);
    }

    public boolean remove(String variable) {
        return table.removeItem(variable);
    }

    public void removeSelected() {
        Iterable selectedRows = (Iterable) table.getValue();
        for (Object row : selectedRows) {
            table.removeItem(row);
        }
    }

    public boolean clear() {
        return table.removeAllItems();
    }

    public void sort() {
        table.sort(new Object[]{COLUMN_NAME}, new boolean[]{true});
    }

    public Set<String> getVariables() {
        Set<String> variables = new HashSet<>(table.size());
        for (Iterator i = table.getItemIds().iterator(); i.hasNext(); ) {
            Object id = i.next();
            Item item = table.getItem(id);
            String var = item.getItemProperty(COLUMN_NAME).getValue().toString();
            variables.add(var);
        }
        return variables;
    }
}
