package ch.cern.totem.todac.web.utils;

import org.apache.log4j.Logger;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.server.Page;
import com.vaadin.ui.Notification;

public class Notificator {

	private static Logger logger = Logger.getLogger(Notificator.class);

	public static void showError(TodacException e) {
		logger.error(e);
        e.printStackTrace();
		new Notification(e.getMessage(), "\n" + e.getSolution(), Notification.Type.ERROR_MESSAGE).show(Page.getCurrent());
	}

	public static void showError(TudasException e) {
		logger.error(e);
        e.printStackTrace();
		new Notification(e.getReason(), "\n" + e.getSolution(), Notification.Type.ERROR_MESSAGE).show(Page.getCurrent());
	}

	public static void showTrayMessage(String title, String caption) {
		logger.debug(title + ": " + caption);
		new Notification(title, caption, Notification.Type.TRAY_NOTIFICATION).show(Page.getCurrent());
	}

}
