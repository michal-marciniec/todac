package ch.cern.totem.todac.web.components.luminosity;

import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.BrowserButtonsRow;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsData;
import ch.cern.totem.todac.web.utils.DownloadResourceProvider;
import ch.cern.totem.todac.web.utils.Downloader;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class LuminosityByLsResults extends VerticalLayout implements DownloadResourceProvider {

    protected BrowserSwitcher browserSwitcher;
    protected Downloader downloader;
    protected LuminosityByLsDataTable dataTable;
    protected BrowserButtonsRow buttonsRow;
    protected String filename;

    public LuminosityByLsResults(BrowserSwitcher browserSwitcher, LuminosityByLsData luminosity) {
        this.browserSwitcher = browserSwitcher;
        this.initComponents(luminosity);
    }

    protected void initComponents(LuminosityByLsData luminosity) {
        dataTable = new LuminosityByLsDataTable(luminosity);
        setFilename(luminosity.getLabel());

        buttonsRow = new BrowserButtonsRow();
        buttonsRow.addLeftClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                browserSwitcher.switchToSearchBox();
            }
        });

        downloader = new Downloader(this);
        downloader.extend(buttonsRow.getRightButton());

        addComponent(dataTable);
        addComponent(buttonsRow);
    }

    protected void setFilename(String label) {
        filename = label + ".csv";
    }

    @Override
    public String getResource() {
        String contents = dataTable.getContents().getFile();
        return contents;
    }

    @Override
    public String getFilename() {
        return filename;
    }
}
