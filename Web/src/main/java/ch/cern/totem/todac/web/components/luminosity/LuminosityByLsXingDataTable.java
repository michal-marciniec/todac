package ch.cern.totem.todac.web.components.luminosity;

import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsXingData;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;
import com.vaadin.ui.Table;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@SuppressWarnings("serial")
public class LuminosityByLsXingDataTable extends Table {

    protected final static int PAGE_LENGTH = 25;
	protected static final String[] COLUMNS = { "Run", "Fill", "UTC Time", "Delivered (/ub)", "Recorded (/ub)", "LsLengthSec" }; // "[bx,Hz/ub]"

	protected LuminosityByLsXingData data;
	protected int id;

	public LuminosityByLsXingDataTable(LuminosityByLsXingData data) {
		this.data = data;
		this.id = 0;
		this.initComponents();
	}

	protected void initComponents() {
		setImmediate(true);
		setSelectable(true);
		setMultiSelect(true);

		// initialize table
		addContainerProperty(COLUMNS[0], Integer.class, 0);
		addContainerProperty(COLUMNS[1], Integer.class, 0);
		addContainerProperty(COLUMNS[2], String.class, "");
		addContainerProperty(COLUMNS[3], String.class, "");
		addContainerProperty(COLUMNS[4], String.class, "");
		addContainerProperty(COLUMNS[5], String.class, "");

		final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		for (LuminosityByLsXing luminosity : data.getLuminosityByLsXing()) {
			Object[] values = new Object[COLUMNS.length];
			values[0] = luminosity.cmsRun;
			values[1] = luminosity.fill;
			values[2] = dateFormat.format(new Timestamp(luminosity.timestamp));
			values[3] = Double.toString(luminosity.delivered);
			values[4] = Double.toString(luminosity.recorded);
			values[5] = Double.toString(luminosity.lslengthsec);
			addItem(values, getNewId());
		}

        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
	}

	protected Integer getNewId() {
		return new Integer(id++);
	}

    public LuminosityByLsXingData getContents() {
        return data;
    }

}
