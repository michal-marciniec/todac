package ch.cern.totem.todac.web.components.luminosity;

import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsData;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;
import com.vaadin.ui.Table;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@SuppressWarnings("serial")
public class LuminosityByLsDataTable extends Table {

    protected final static int PAGE_LENGTH = 25;
	protected static final String[] COLUMNS = { "Run", "Fill", "LS", "UTC Time", "Beam Status", "E (GeV)", "Delivered (/ub", "Recorded (/ub)", "Average PU", "LsLengthSec" };

	protected LuminosityByLsData data;
	protected int id;

	public LuminosityByLsDataTable(LuminosityByLsData data) {
		this.data = data;
		this.id = 0;
		this.initComponents();
	}

	protected void initComponents() {
		setImmediate(true);
		setSelectable(true);
		setMultiSelect(true);

		// initialize table
		addContainerProperty(COLUMNS[0], Integer.class, 0);
		addContainerProperty(COLUMNS[1], Integer.class, 0);
		addContainerProperty(COLUMNS[2], String.class, "");
		addContainerProperty(COLUMNS[3], String.class, "");
		addContainerProperty(COLUMNS[4], String.class, "");
		addContainerProperty(COLUMNS[5], String.class, "");
		addContainerProperty(COLUMNS[6], String.class, "");
		addContainerProperty(COLUMNS[7], String.class, "");
		addContainerProperty(COLUMNS[8], String.class, "");
		addContainerProperty(COLUMNS[9], String.class, "");

		final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		for (LuminosityByLs luminosity : data.getLuminosityByLs()) {
			Object[] values = new Object[COLUMNS.length];
			values[0] = luminosity.cmsRun;
			values[1] = luminosity.fill;
			values[2] = luminosity.ls;
			values[3] = dateFormat.format(new Timestamp(luminosity.timestamp));
			values[4] = luminosity.beamStatus;
			values[5] = Double.toString(luminosity.energy);
			values[6] = Double.toString(luminosity.delivered);
			values[7] = Double.toString(luminosity.recorded);
			values[8] = Double.toString(luminosity.avgPU);
			values[9] = Double.toString(luminosity.lslengthsec);
			addItem(values, getNewId());
		}

        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
	}

	protected Integer getNewId() {
		return new Integer(id++);
	}

    public LuminosityByLsData getContents() {
        return data;
    }

}
