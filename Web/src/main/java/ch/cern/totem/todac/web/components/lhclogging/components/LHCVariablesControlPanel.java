package ch.cern.totem.todac.web.components.lhclogging.components;

import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

import java.util.*;

public class LHCVariablesControlPanel extends VerticalLayout {

    protected static final String INPUT_IGNORE_REGEX = "[ ,\t\n]+";
    protected static final String CAPTION_ADD = "Add variables";
    protected static final String CAPTION_ADD_DEFAULTS = "Load defaults ";
    protected static final String CAPTION_REMOVE = "Remove";
    protected static final String CAPTION_CLEAR = "Clear";
    protected static final String CAPTION_SORT = "Sort";
    protected final LHCVariablesTableBuilder tableBuilder;
    protected final Set<String> defaultVariableSet;
    protected TextArea textField;
    protected Button addButton;
    protected Button addDefaultsButton;
    protected Button removeButton;
    protected Button clearButton;
    protected Button sortButton;

    public LHCVariablesControlPanel(LHCVariablesTableBuilder tableBuilder, Set<String> defaultVariables) {
        this.tableBuilder = tableBuilder;
        this.defaultVariableSet = defaultVariables;
        initComponents();
    }

    protected void initComponents() {
        this.textField = new TextArea();
        this.addButton = new Button(CAPTION_ADD);
        this.addButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                List<String> userVariables = parseInputVariables();
                tableBuilder.add(userVariables);
            }
        });
        Layout addVariableLayout = new VerticalLayout(textField, addButton);
        addVariableLayout.setStyleName("inputlayout");

        this.addDefaultsButton = new Button(CAPTION_ADD_DEFAULTS);
        this.addDefaultsButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                List<String> sorted = new LinkedList<>(defaultVariableSet);
                Collections.sort(sorted);
                tableBuilder.add(sorted);
            }
        });
        this.removeButton = new Button(CAPTION_REMOVE);
        this.removeButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                tableBuilder.removeSelected();
            }
        });

        this.sortButton = new Button(CAPTION_SORT);
        this.sortButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                tableBuilder.sort();
            }
        });

        this.clearButton = new Button(CAPTION_CLEAR);
        this.clearButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                tableBuilder.clear();
            }
        });

        this.setStyleName("controlpanel");
        addComponents(addVariableLayout, addDefaultsButton, sortButton, removeButton, clearButton);
    }

    protected List<String> parseInputVariables() {
        String text = textField.getValue();
        text = text.trim();
        String[] variables = text.split(INPUT_IGNORE_REGEX);
        return Arrays.asList(variables);
    }

    public void reset() {
        textField.setValue("");
    }

}
