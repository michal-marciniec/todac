package ch.cern.totem.todac.web.components.forms.derivable;

import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;

@SuppressWarnings("serial")
public abstract class FormRowLayout extends CustomComponent implements FormComponent {

	protected static final String STYLE_FORM_LEFT_CONTENT = "form-left-content";
	protected static final String STYLE_FORM_RIGHT_CONTENT = "form-right-content";
	protected static final String STYLE_FORM_CONTENT = "form-content";
	
	protected HorizontalLayout content;
	protected HorizontalLayout leftContent;
	protected HorizontalLayout rightContent;
	
	public FormRowLayout(){
		this.initComponents();
	}
	
	private void initComponents() {
		leftContent = new HorizontalLayout();
		leftContent.setStyleName(STYLE_FORM_LEFT_CONTENT);
		rightContent = new HorizontalLayout();
		rightContent.setStyleName(STYLE_FORM_RIGHT_CONTENT);
		content = new HorizontalLayout(leftContent, rightContent);
		content.setStyleName(STYLE_FORM_CONTENT);
		setCompositionRoot(content);
		setImmediate(true);
	}
	
	public void addLeftComponent(Component component){
		leftContent.addComponent(component);
	}
	
	public void addRightComponent(Component component){
		rightContent.addComponent(component);
	}

}
