package ch.cern.totem.todac.web.components.alignment;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.alignment.AlignmentDao;
import ch.cern.totem.todac.web.model.alignment.AlignmentData;
import ch.cern.totem.todac.web.model.alignment.AlignmentDownloadParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class AlignmentBrowser extends CustomComponent implements BrowserSwitcher {

	protected AlignmentBrowserForm browserForm;

	public AlignmentBrowser() {
		this.initComponents();
		this.switchToSearchBox();
	}

	protected void initComponents() {
		browserForm = new AlignmentBrowserForm(this);
	}

	@Override
	public void switchToBrowser() {
		try {
			AlignmentDownloadParameters parameters = browserForm.getParameters();
			AlignmentDao alignmentDao = new AlignmentDao();
			AlignmentData data = alignmentDao.downloadAlignments(parameters);
			AlignmentResults display = new AlignmentResults(this, data);
			setCompositionRoot(display);
		} catch (InvalidArgumentException e) {
			Notificator.showError(e);
		} catch (TudasException e) {
			Notificator.showError(e);
		}
	}

	@Override
	public void switchToSearchBox() {
		setCompositionRoot(browserForm);
	}

}
