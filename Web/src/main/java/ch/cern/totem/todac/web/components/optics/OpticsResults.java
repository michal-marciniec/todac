package ch.cern.totem.todac.web.components.optics;

import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.BrowserButtonsRow;
import ch.cern.totem.todac.web.components.forms.ButtonsRow;
import ch.cern.totem.todac.web.model.optics.OpticsData;
import ch.cern.totem.todac.web.utils.DownloadResourceProvider;
import ch.cern.totem.todac.web.utils.Downloader;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class OpticsResults extends VerticalLayout implements DownloadResourceProvider {

    private static final String DOWNLOAD_FILENAME = "optics.csv";
    protected BrowserSwitcher switcher;
    protected OpticsDataTable dataTable;
    protected ButtonsRow buttonsRow;
    protected Downloader downloader;

    public OpticsResults(OpticsData contents, BrowserSwitcher switcher) {
        super();
        this.switcher = switcher;

        initComponents();
        setContents(contents);
        this.addComponents(dataTable, buttonsRow);
    }

    protected void initComponents() {
        this.dataTable = new OpticsDataTable();

        buttonsRow = new BrowserButtonsRow();
        buttonsRow.addLeftClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                switcher.switchToSearchBox();
            }
        });

        this.downloader = new Downloader(this);
        this.downloader.extend(buttonsRow.getRightButton());
    }

    public OpticsData getContents() {
        return dataTable.getContents();
    }

    public void setContents(OpticsData contents) {
        dataTable.setContents(contents);
    }

    @Override
    public String getResource() {
        final String resource = getContents().parseCsv();
        return resource;
    }

    @Override
    public String getFilename() {
        return DOWNLOAD_FILENAME;
    }

}
