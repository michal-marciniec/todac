package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.geometry.GeometryBrowser;
import ch.cern.totem.todac.web.components.geometry.GeometryPopulateForm;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class GeometryView extends TodacView {

	public static final TodacURL URL = new TodacURL("geometry", "Geometry");

	public GeometryView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new GeometryPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new GeometryBrowser();
	}

}
