package ch.cern.totem.todac.web.model.optics;

import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.commons.utils.TimestampFetcher;
import ch.cern.totem.todac.populators.optics.OpticsPopulator;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.OpticsManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import java.util.Map;
import java.util.TreeMap;

public class OpticsDao {

    public void populate(String filepath, OpticsType type, int startRun, int endRun) throws TudasException, TodacException {
        DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
        databaseAccessProvider.initialize();
        OpticsPopulator opticsPopulator = new OpticsPopulator(databaseAccessProvider);
        opticsPopulator.initialize();

        int version = -1;
        switch (type) {
            case BETA_STAR:
                version = opticsPopulator.parseAndPopulateBetaStar(filepath);
                break;
            case CURRENTS:
                version = opticsPopulator.parseAndPopulateCurrent(filepath, startRun, endRun);
                break;
            case MAGNET_STRENGTH:
                version = opticsPopulator.parseAndPopulateMagnetStrength(filepath, startRun, endRun);
                break;
        }

        Notificator.showTrayMessage("Success", "Database populated with specified optics data. Version: " + version);
    }

    public OpticsData fetch(OpticsParameters query) throws TudasException, DatabaseException {
        Map<String, Double> opticsMap = new TreeMap<>();
        DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
        databaseAccessProvider.initialize();
        TimestampFetcher timestampFetcher = new TimestampFetcher(databaseAccessProvider.getRunInformationManager());
        long timestamp = timestampFetcher.getRunStartTimestamp(query.getRunNumber());

        final OpticsManager opticsManager = databaseAccessProvider.getOpticsManager();
        switch (query.getType()) {
            case BETA_STAR:
                double bsValue = opticsManager.loadBetaStar(timestamp, query.getVersion());
                opticsMap.put(query.getType().toString(), bsValue);
                break;
            case CURRENTS:
                Map<String, Double> currents = opticsManager.loadCurrents(timestamp, query.getVersion());
                opticsMap.putAll(currents);
                break;
            case MAGNET_STRENGTH:
                Map<String, Double> magnetStrength = opticsManager.loadMagnetStrength(timestamp, query.getVersion());
                opticsMap.putAll(magnetStrength);
                break;
        }

        return new OpticsData(opticsMap);
    }
}
