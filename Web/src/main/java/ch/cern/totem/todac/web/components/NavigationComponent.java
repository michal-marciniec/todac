package ch.cern.totem.todac.web.components;

import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class NavigationComponent extends VerticalLayout {
	
    protected Header header;
    protected Component navigationMenu;
    protected HorizontalLayout mainContent;
    
    public NavigationComponent(Component navigationMenu) {
    	this.navigationMenu = navigationMenu;
    	this.initComponents();
    }
    
    protected void initComponents() {
        header = new Header();
        addComponent(header);
        mainContent = new HorizontalLayout(navigationMenu);
        addComponent(mainContent);
        setExpandRatio(header, 0);
        setExpandRatio(mainContent, 1);
    	mainContent.setSizeFull();
    }
    
    public void setContent(Component component) {
    	mainContent.removeAllComponents();
    	mainContent.addComponent(navigationMenu);
    	mainContent.addComponent(component);
    	mainContent.setExpandRatio(navigationMenu, 0);
    	mainContent.setExpandRatio(component, 1);
    	//component.setSizeFull();
	}
    
    public static NavigationComponent build() {
    	NavigationMenu navigationMenu = NavigationMenu.build();
    	NavigationComponent navigationComponent = new NavigationComponent(navigationMenu);
    	return navigationComponent;
    }
    
}
