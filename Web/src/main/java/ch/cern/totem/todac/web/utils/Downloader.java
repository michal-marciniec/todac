package ch.cern.totem.todac.web.utils;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@SuppressWarnings("serial")
public class Downloader extends FileDownloader {

    private final DownloadResourceProvider resourceProvider;

    public Downloader(DownloadResourceProvider resourceProvider) {
        super(new StreamResource(null, ""));
        this.resourceProvider = resourceProvider;
    }

    @Override
    public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path) throws IOException {
        final StreamResource resource = createResource(resourceProvider.getResource(), resourceProvider.getFilename());
        setFileDownloadResource(resource);

        return super.handleConnectorRequest(request, response, path);
    }

	protected static StreamResource createResource(final String contents, String filename) {
        return new StreamResource(new StreamResource.StreamSource() {
            @Override
            public InputStream getStream() {
                return new ByteArrayInputStream(contents.getBytes());
            }
        }, filename);
    }
	
}