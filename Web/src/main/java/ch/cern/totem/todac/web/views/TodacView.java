package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.BrowseUploadTabSheet;
import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public abstract class TodacView extends CustomComponent implements View {

	protected TodacURL url;
	protected NavigationComponent navigationComponent;
	protected CustomComponent browser;
	protected CustomComponent populateForm;
	protected BrowseUploadTabSheet tabSheet;

	public TodacView(NavigationComponent navigationComponent, TodacURL url) {
		this.navigationComponent = navigationComponent;
		this.url = url;
		this.browser = null;
		this.populateForm = null;
		this.tabSheet = null;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (tabSheet == null) {
			browser = createBrowser();
			populateForm = createPopulateForm();
			tabSheet = new BrowseUploadTabSheet(browser, populateForm);
			navigationComponent.setContent(tabSheet);
			//navigationComponent.setSizeFull();
		}
		setCompositionRoot(navigationComponent);
		//setSizeFull();
	}

	public TodacURL getTodacURL() {
		return url;
	}

	// factory method pattern
	protected abstract CustomComponent createPopulateForm();

	// factory method pattern
	protected abstract CustomComponent createBrowser();

}
