package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.derivable.ComboBoxRow;
import ch.cern.totem.todac.web.model.runinfo.RunInformationDao;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

@SuppressWarnings("serial")
public class RunSelectRow extends ComboBoxRow {

    protected static final String CAPTION = "Run number";

    public RunSelectRow() {
        this(CAPTION);
    }

    public RunSelectRow(String caption) {
        super(caption);
        try {
			this.initComponents();
		} catch (TudasException e) {
			Notificator.showError(e);
		} catch (TodacException e) {
			Notificator.showError(e);
		}
    }

    protected void initComponents() throws TudasException, TodacException {
        RunInformationDao runInformationDao = new RunInformationDao();
        int[] runs = runInformationDao.downloadAllRuns();
        for (int run : runs) {
            addOption(String.valueOf(run));
        }

        comboBox.setTextInputAllowed(false);
    }
}
