package ch.cern.totem.todac.web.navigation;

import ch.cern.totem.todac.web.views.HomeView;
import com.vaadin.navigator.Navigator;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Dispatcher implements Serializable {

	private Navigator navigator;

	public Dispatcher(Navigator navigator) {
		this.navigator = navigator;
	}

	/***
	 * Redirects to specified url.
	 * 
	 * @param url
	 *            Url to redirect to
	 */
	public void navigateTo(String url) {
		navigator.navigateTo(url);
	}

	
    /***
     * Redirects to home subpage.
     *
     */
    public void navigateToHome() {
        navigateTo(HomeView.URL);
    }

}
