package ch.cern.totem.todac.web.converters;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;

import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsData;
import ch.cern.totem.todac.web.model.luminosity.LuminosityByLsXingData;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;

public class LuminosityConverter {
	
	public static String convert(LuminosityByLsData data) {
		StringBuilder documentBuilder = new StringBuilder();
		documentBuilder.append("Run:Fill,LS,UTCTime,Beam Status,E(GeV),Delivered(/ub),Recorded(/ub),avgPU\n");
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		for (LuminosityByLs luminosity : data.getLuminosityByLs()) {
			documentBuilder.append(luminosity.cmsRun);
			documentBuilder.append(":");
			documentBuilder.append(luminosity.fill);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.ls);
			documentBuilder.append(",");
			documentBuilder.append(dateFormat.format(new Timestamp(luminosity.timestamp)));
			documentBuilder.append(",");
			documentBuilder.append(luminosity.beamStatus);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.energy);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.delivered);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.recorded);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.avgPU);
			documentBuilder.append("\n");
		}
		return documentBuilder.toString();
	}
	
	public static String convert(LuminosityByLsXingData data) {
		StringBuilder documentBuilder = new StringBuilder();
		documentBuilder.append("run:fill,ls,UTCTime,delivered(/ub),recorded(/ub),\"[bx,Hz/ub]\"\n");
		SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		for (LuminosityByLsXing luminosity : data.getLuminosityByLsXing()) {
			documentBuilder.append(luminosity.cmsRun);
			documentBuilder.append(":");
			documentBuilder.append(luminosity.fill);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.ls);
			documentBuilder.append(",");
			documentBuilder.append(dateFormat.format(new Timestamp(luminosity.timestamp)));
			documentBuilder.append(",");
			documentBuilder.append(luminosity.delivered);
			documentBuilder.append(",");
			documentBuilder.append(luminosity.recorded);
			documentBuilder.append(",");
			for (double value : luminosity.luminosityValues) {
				documentBuilder.append(value);
				documentBuilder.append(",");
			}
			documentBuilder.append("\n");
		}
		return documentBuilder.toString();
	}
	
	public static void sort(LuminosityByLs[] luminosity) {
		final LuminosityByLsComparator comparator = new LuminosityByLsComparator();
		Arrays.sort(luminosity, comparator);
	}
	
	public static void sort(LuminosityByLsXing[] luminosity) {
		final LuminosityByLsXingComparator comparator = new LuminosityByLsXingComparator();
		Arrays.sort(luminosity, comparator);
	}
	
	static class LuminosityByLsComparator implements Comparator<LuminosityByLs> {
		@Override
		public int compare(LuminosityByLs o1, LuminosityByLs o2) {
			Long t1 = o1.timestamp;
			Long t2 = o2.timestamp;
			return (o1 == null || o2 == null) ? 0 : t1.compareTo(t2);
		}
	}
	
	static class LuminosityByLsXingComparator implements Comparator<LuminosityByLsXing> {
		@Override
		public int compare(LuminosityByLsXing o1, LuminosityByLsXing o2) {
			Long t1 = o1.timestamp;
			Long t2 = o2.timestamp;
			return (o1 == null || o2 == null) ? 0 : t1.compareTo(t2);
		}
	}

}
