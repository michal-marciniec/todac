package ch.cern.totem.todac.web.model.geometry;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.geometry.GeometryPopulator;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import java.util.Map;

public class GeometryDao {

    public void populate(String filepath, GeometryParameters query) throws TodacException, TudasException {
        DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
        databaseAccessProvider.initialize();
        GeometryPopulator geometryPopulator = new GeometryPopulator(databaseAccessProvider);
        geometryPopulator.initialize();

        int geometryVersion;
        if (query.getLabel() == null) {
            geometryVersion = geometryPopulator.parseAndPopulate(filepath, query.getRunNumber(), query.getEndRun());
        } else {
            geometryVersion = geometryPopulator.parseAndPopulate(filepath, query.getRunNumber(), query.getEndRun(), query.getLabel());
        }

        Notificator.showTrayMessage("Success", "Database populated with specified geometry data. Version: " + geometryVersion);
    }

    public GeometryData fetch(GeometryParameters query) throws TudasException {
        DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
        databaseAccessProvider.initialize();

        RunInformationManager runInfoManager = databaseAccessProvider.getRunInformationManager();
        long startTimestamp = runInfoManager.loadRunStartTime(query.getRunNumber());

        RomanPotManager rpManager = databaseAccessProvider.getRomanPotManager();
        Map<String, Double> geometryMap = rpManager.loadGeometry(startTimestamp, query.getLabel(), query.getVersion());

        return new GeometryData(geometryMap);
    }
}
