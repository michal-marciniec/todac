package ch.cern.totem.todac.web.components.forms.validators;

import ch.cern.totem.todac.web.model.Constants;

import com.vaadin.data.validator.RegexpValidator;

@SuppressWarnings("serial")
public class TimestampValidator extends RegexpValidator {
	// MM/dd/yy HH:mm:ss
	public TimestampValidator() {
		super("[\\s]*" + "(([0][1-9])|([1][0-2]))" + "/" + "(([0-2][0-9])|([3][0-1]))" + "/" + "[0-9]{2}" + "[\\s]+" + "(([0-1][0-9])|[2][0-3])" + "[:]"
				+ "[0-5][0-9]" + "[:]" + "[0-5][0-9]" + "[\\s]*", "Expected timestamp format is: " + Constants.DATE_FORMAT);
	}

}
