package ch.cern.totem.todac.web.model.luminosity;

import ch.cern.totem.todac.web.model.Constants;

public class LuminosityUploadParameters {

	protected String filepath;
	protected String label;
	protected Double lsLengthSec;
	protected LuminosityType type;

	public LuminosityUploadParameters(String filepath, String label, Double lsLengthSec, LuminosityType type) {
		this.filepath = filepath;
		this.label = label;
		this.lsLengthSec = lsLengthSec == null ? Constants.LS_LENGTH_SEC : lsLengthSec;
		this.type = type;
	}

	public String getFilepath() {
		return filepath;
	}

	public String getLabel() {
		return label;
	}

	public Double getlsLengthSec() {
		return lsLengthSec;
	}

	public LuminosityType getType() {
		return type;
	}

}
