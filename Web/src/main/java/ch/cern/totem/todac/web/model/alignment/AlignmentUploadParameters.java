package ch.cern.totem.todac.web.model.alignment;

public class AlignmentUploadParameters {

	protected Integer startRun; 
	protected Integer endRun; 
	protected String label;
	protected String filepath;

	public AlignmentUploadParameters(Integer startRun, Integer endRun, String label, String filepath) {
		super();
		this.startRun = startRun;
		this.endRun = endRun;
		this.label = label;
		this.filepath = filepath;
	}

	public Integer getStartRun() {
		return startRun;
	}
	
	public Integer getEndRun() {
		return endRun;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getFilepath() {
		return filepath;
	}
	
}
