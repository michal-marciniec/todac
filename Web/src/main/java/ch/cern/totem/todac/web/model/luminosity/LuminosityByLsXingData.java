package ch.cern.totem.todac.web.model.luminosity;

import ch.cern.totem.todac.web.converters.LuminosityConverter;
import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLsXing;

public class LuminosityByLsXingData {
	
	protected LuminosityByLsXing[] luminosityByLsXing;
	protected String label;
	protected Double lsLengthSec;

	public LuminosityByLsXingData() {
		this.luminosityByLsXing = new LuminosityByLsXing[0];
		this.label = "";
		this.lsLengthSec = Constants.LS_LENGTH_SEC;
	}
	
	public LuminosityByLsXingData(LuminosityByLsXing[] luminosityByLsXing, String label) {
		this.luminosityByLsXing = luminosityByLsXing;
		this.label = label;
		this.lsLengthSec = Constants.LS_LENGTH_SEC;
	}

	public String getFile() {
		return LuminosityConverter.convert(this);
	}
	
	public String getLabel() {
		return label;
	}
	
	public LuminosityByLsXing[] getLuminosityByLsXing() {
		return luminosityByLsXing;
	}

	public Double getLsLengthSec() {
		return lsLengthSec;
	}

	public void setLsLengthSec(Double lsLengthSec) {
		this.lsLengthSec = lsLengthSec;
	}
	
}
