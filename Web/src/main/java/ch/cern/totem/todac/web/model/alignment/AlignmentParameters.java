package ch.cern.totem.todac.web.model.alignment;

public class AlignmentParameters {

	protected int startRun; 
	protected int endRun; 
	protected String label;

	public AlignmentParameters(int startRun, int endRun, String label) {
		this.startRun = startRun;
		this.endRun = endRun;
		this.label = label;
	}
	
	public int getStartRun() {
		return startRun;
	}
	public void setStartRun(int startRun) {
		this.startRun = startRun;
	}
	public int getEndRun() {
		return endRun;
	}
	public void setEndRun(int endRun) {
		this.endRun = endRun;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
}
