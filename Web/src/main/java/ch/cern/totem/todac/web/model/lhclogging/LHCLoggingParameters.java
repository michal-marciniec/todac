package ch.cern.totem.todac.web.model.lhclogging;

import java.util.Set;

public class LHCLoggingParameters {

    protected int startRun;
    protected int endRun;
    protected Set<String> variables;

    public LHCLoggingParameters(int startRun, int endRun, Set<String> variables) {
        this.startRun = startRun;
        this.endRun = endRun;
        this.variables = variables;
    }

    public int getEndRun() {
        return endRun;
    }

    public int getStartRun() {
        return startRun;
    }

    public Set<String> getVariables() {
        return variables;
    }
}
