package ch.cern.totem.todac.web.components.geometry;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.*;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;
import ch.cern.totem.todac.web.model.geometry.GeometryDao;
import ch.cern.totem.todac.web.model.geometry.GeometryParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.todac.web.utils.UploadReceiver;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Upload;

@SuppressWarnings("serial")
public class GeometryPopulateForm extends FormLayout {

    protected GeometryDao geometryDao;
    protected StartRunSelectRow startRunRow;
    protected EndRunSelectRow endRunRow;
    protected OptionalInputRow labelRow;
    protected UploadRow uploadRow;
    protected SubmitRow submitRow;

    public GeometryPopulateForm() {
        initComponents();
        this.reset();
        addComponents(startRunRow, endRunRow, labelRow, uploadRow, submitRow);
    }

    protected void initComponents() {
        this.geometryDao = new GeometryDao();
        this.startRunRow = new StartRunSelectRow();
        this.endRunRow = new EndRunSelectRow();
        this.labelRow = new OptionalLabelRow();
        this.uploadRow = new UploadRow();
        this.uploadRow.setUploadReceiver(getUploadReceiver());
        this.submitRow = new SubmitRow();
        this.submitRow.addLeftClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                reset();
            }
        });
        this.submitRow.addRightClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                uploadRow.submitUpload();
            }
        });
    }

    protected UploadReceiver getUploadReceiver() {
        return new UploadReceiver(new Upload.SucceededListener() {
            @Override
            public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
                try {
                    String uploadedFilePath = uploadRow.getFile().getAbsolutePath();
                    GeometryParameters queryParameters = getParameters();
                    geometryDao.populate(uploadedFilePath, queryParameters);
                } catch (TodacException e) {
                    Notificator.showError(e);
                } catch (TudasException e) {
                    Notificator.showError(e);
                }
                uploadRow.getFile().delete();
                reset();
            }
        });
    }

    protected GeometryParameters getParameters() throws InvalidArgumentException {
        try {
            int startRun = Integer.parseInt(startRunRow.getValue());
            int endRun = Integer.parseInt((endRunRow.isChecked() ? endRunRow.getValue() : startRunRow.getValue()));
            String label = labelRow.isChecked() ? labelRow.getValue() : null;

            GeometryParameters parameters = new GeometryParameters(startRun, endRun, label);
            return parameters;
        } catch (Exception e) {
            throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
        }
    }
}
