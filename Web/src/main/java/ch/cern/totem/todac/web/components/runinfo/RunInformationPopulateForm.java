package ch.cern.totem.todac.web.components.runinfo;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.DescriptionRow;
import ch.cern.totem.todac.web.components.forms.RunRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.runinfo.RunInformationDao;
import ch.cern.totem.todac.web.model.runinfo.RunInformationUploadParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class RunInformationPopulateForm extends FormLayout {

	protected RunRow runRow;
	protected DescriptionRow descriptionRow;
	protected SubmitRow submitRow;

	protected RunInformationDao runInformationDao;

	public RunInformationPopulateForm() {
		try {
			this.initComponents();
		} catch (TudasException e) {
			Notificator.showError(e);
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}

	protected void initComponents() throws TudasException, TodacException {
		runInformationDao = new RunInformationDao();

		runRow = new RunRow();
		descriptionRow = new DescriptionRow();
		submitRow = new SubmitRow();

		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				try {
					runInformationDao.uploadRunInfo(getParameters());
				} catch (InvalidArgumentException e) {
					Notificator.showError(e);
				}

				reset();
			}
		});

		addComponent(runRow);
		addComponent(descriptionRow);
		addComponent(submitRow);
	}

	protected RunInformationUploadParameters getParameters() throws InvalidArgumentException {
		try {
			int runNumber = Integer.parseInt(runRow.getValue());
			String description = descriptionRow.getValue();
			return new RunInformationUploadParameters(runNumber, description);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
