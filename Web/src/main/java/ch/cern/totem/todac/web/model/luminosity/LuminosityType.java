package ch.cern.totem.todac.web.model.luminosity;

public enum LuminosityType {
	LuminosityByLs, LuminosityByLsXing
}