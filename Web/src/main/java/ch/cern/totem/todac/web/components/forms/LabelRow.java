package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.InputRow;


@SuppressWarnings("serial")
public class LabelRow extends InputRow {

	protected static final String CAPTION = "Label";

	public LabelRow() {
		super(CAPTION);
	}

}
