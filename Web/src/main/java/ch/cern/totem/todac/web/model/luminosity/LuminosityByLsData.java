package ch.cern.totem.todac.web.model.luminosity;

import ch.cern.totem.todac.web.converters.LuminosityConverter;
import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.tudas.tudasUtil.structure.LuminosityByLs;

public class LuminosityByLsData {

	protected LuminosityByLs[] luminosityByLs;
	protected String label;
	protected Double lsLengthSec;

	public LuminosityByLsData() {
		this.luminosityByLs = new LuminosityByLs[0];
		this.label = "";
		this.lsLengthSec = Constants.LS_LENGTH_SEC;
	}
	
	public LuminosityByLsData(LuminosityByLs[] luminosityByLs, String label) {
		this.luminosityByLs = luminosityByLs;
		this.label = label;
		this.lsLengthSec = Constants.LS_LENGTH_SEC;
	}

	public String getFile() {
		return LuminosityConverter.convert(this);
	}
	
	public String getLabel() {
		return label;
	}
	
	public LuminosityByLs[] getLuminosityByLs() {
		return luminosityByLs;
	}

	public Double getLsLengthSec() {
		return lsLengthSec;
	}

	public void setLsLengthSec(Double lsLengthSec) {
		this.lsLengthSec = lsLengthSec;
	}

}
