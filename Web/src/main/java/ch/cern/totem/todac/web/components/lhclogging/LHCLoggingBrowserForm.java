package ch.cern.totem.todac.web.components.lhclogging;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.EndRunSelectRow;
import ch.cern.totem.todac.web.components.forms.StartRunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.lhclogging.components.LHCVariablesRow;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingParameters;
import com.vaadin.ui.Button;

import java.util.Set;

@SuppressWarnings("serial")
public class LHCLoggingBrowserForm extends FormLayout {

    protected BrowserSwitcher switcher;
    protected StartRunSelectRow startRunRow;
    protected EndRunSelectRow endRunRow;
    protected LHCVariablesRow lhcVariablesRow;
    protected SubmitRow submitRow;

    public LHCLoggingBrowserForm(BrowserSwitcher switcher) {
        this.switcher = switcher;
        initComponents();
        this.addComponents(startRunRow, endRunRow, lhcVariablesRow, submitRow);
    }

    protected void initComponents() {
        this.startRunRow = new StartRunSelectRow();
        this.endRunRow = new EndRunSelectRow();
        this.lhcVariablesRow = new LHCVariablesRow();
        this.submitRow = new SubmitRow();
        this.submitRow.addRightClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                switcher.switchToBrowser();
            }
        });
        this.submitRow.addLeftClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                reset();
            }
        });
    }

    public LHCLoggingParameters getParameters() throws InvalidArgumentException {
        try {
            int startRun = Integer.parseInt(startRunRow.getValue());
            int endRun = Integer.parseInt((endRunRow.isChecked() ? endRunRow.getValue() : startRunRow.getValue()));
            Set<String> variables = lhcVariablesRow.getVariables();

            LHCLoggingParameters parameters = new LHCLoggingParameters(startRun, endRun, variables);
            return parameters;
        } catch (Exception e) {
            throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);

        }
    }
}
