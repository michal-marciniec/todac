package ch.cern.totem.todac.web.model.runinfo;

public class RunInformationUploadParameters {

	protected Integer runNumber;
	protected String description;

	public RunInformationUploadParameters(Integer runNumber, String description) {
		super();
		this.runNumber = runNumber;
		this.description = description;
	}

	public Integer getRunNumber() {
		return runNumber;
	}

	public String getDescription() {
		return description;
	}

}
