package ch.cern.totem.todac.web.components.optics;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.EndRunSelectRow;
import ch.cern.totem.todac.web.components.forms.OpticsTypeRow;
import ch.cern.totem.todac.web.components.forms.StartRunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.UploadRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.optics.OpticsDao;
import ch.cern.totem.todac.web.model.optics.OpticsType;
import ch.cern.totem.todac.web.utils.UploadReceiver;

import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.data.Property;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Upload;

@SuppressWarnings("serial")
public class OpticsPopulateForm extends FormLayout {

	protected StartRunSelectRow startRunRow;
	protected EndRunSelectRow endRunRow;
	protected UploadRow uploadRow;
	protected SubmitRow submitRow;
	protected OpticsTypeRow opticsTypeRow;

	public OpticsPopulateForm() {
		initComponents();
        addComponents(opticsTypeRow, startRunRow, endRunRow, uploadRow, submitRow);
	}

	protected void initComponents() {
		startRunRow = new StartRunSelectRow();
		endRunRow = new EndRunSelectRow();
		uploadRow = new UploadRow();
		uploadRow.setUploadReceiver(getUploadReceiver());
		submitRow = new SubmitRow();
		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				uploadRow.submitUpload();
			}
		});
		opticsTypeRow = new OpticsTypeRow();
		opticsTypeRow.addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
				OpticsType type = opticsTypeRow.getValue();
				if (type == null)
					return;

				switch (type) {
					case BETA_STAR:
						setRunRowsEnabled(false);
						break;
					case CURRENTS:
					case MAGNET_STRENGTH:
						setRunRowsEnabled(true);
						break;
				}
			}
		});
		opticsTypeRow.reset();
	}

	protected void upload(String opticsFilePath) throws TodacException, TudasException {
		OpticsType type = opticsTypeRow.getValue();
		if (type == OpticsType.BETA_STAR) {
			new OpticsDao().populate(opticsFilePath, type, 0, 0);
		} else {
			int startRun = Integer.parseInt(startRunRow.getValue());
			int endRun = startRun;
			if (endRunRow.isChecked())
				endRun = Integer.parseInt(endRunRow.getValue());

			new OpticsDao().populate(opticsFilePath, type, startRun, endRun);
		}

	}

	protected UploadReceiver getUploadReceiver() {
		return new UploadReceiver(new Upload.SucceededListener() {
			@Override
			public void uploadSucceeded(Upload.SucceededEvent succeededEvent) {
                try {
                    String uploadedFilePath = uploadRow.getFile().getAbsolutePath();
                    upload(uploadedFilePath);
                } catch (TodacException e) {
                    e.printStackTrace();
                } catch (TudasException e) {
                    e.printStackTrace();
                }

                uploadRow.getFile().delete();
			}
		});
	}

	protected void setRunRowsEnabled(boolean isEnabled) {
		this.startRunRow.setEnabled(isEnabled);
		this.endRunRow.setEnabled(isEnabled);
	}
}
