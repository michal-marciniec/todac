package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.luminosity.LuminosityBrowser;
import ch.cern.totem.todac.web.components.luminosity.LuminosityPopulateForm;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class LuminosityView extends TodacView {

	public static final TodacURL URL = new TodacURL("luminosity", "Luminosity");

	public LuminosityView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new LuminosityPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new LuminosityBrowser();
	}

}
