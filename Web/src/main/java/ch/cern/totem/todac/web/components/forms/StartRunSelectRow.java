package ch.cern.totem.todac.web.components.forms;

@SuppressWarnings("serial")
public class StartRunSelectRow extends RunSelectRow {

	protected static final String CAPTION = "Start run";

    public StartRunSelectRow() {
        super(CAPTION);
    }

}
