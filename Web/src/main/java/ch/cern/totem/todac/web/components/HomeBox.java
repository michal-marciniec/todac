package ch.cern.totem.todac.web.components;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class HomeBox extends CustomComponent {

	protected static final String STYLE_NAME = "home";
	protected static final String STYLE_HUGE_NAME = "label-huge";
	protected static final String STYLE_LARGE_NAME = "label-large";
	protected static final String STYLE_LINK_LARGE_NAME = "link-large";

	protected VerticalLayout content;

	public HomeBox() {
		this.initComponents();
	}

	protected void initComponents() {
		Label header = new Label("Welcome to TODAC - TOTEM Offline Database Access Console.");
		header.setStyleName(STYLE_HUGE_NAME);

		Label linkLabel = new Label("Documentation is available at TOTEM TWiki:");
		linkLabel.setStyleName(STYLE_LARGE_NAME);
		Link link = new Link("here", new ExternalResource("https://twiki.cern.ch/twiki/bin/view/TOTEM/CompDBTodacTotemOfflineDatabaseAccessConsole"));
		link.setStyleName(STYLE_LINK_LARGE_NAME);
		HorizontalLayout linkRow = new HorizontalLayout(linkLabel, link);

		content = new VerticalLayout(header, linkRow);
		content.setStyleName(STYLE_NAME);
		setCompositionRoot(content);
	}

}
