package ch.cern.totem.todac.web.components.forms.validators;

import com.vaadin.data.validator.AbstractStringValidator;

@SuppressWarnings("serial")
public class NotEmptyValidator extends AbstractStringValidator {

	public NotEmptyValidator() {
		super("Value cannot be empty");
	}

	@Override
	protected boolean isValidValue(String value) {
		return value != null && !value.isEmpty();
	}

}
