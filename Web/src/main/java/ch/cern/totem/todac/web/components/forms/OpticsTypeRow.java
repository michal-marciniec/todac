package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.FormRowLayout;
import ch.cern.totem.todac.web.model.optics.OpticsType;

import com.vaadin.data.Property;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;

@SuppressWarnings("serial")
public class OpticsTypeRow extends FormRowLayout {

    protected static final String CAPTION = "Optics type";
    protected final OpticsType DEFAULT_VALUE = OpticsType.BETA_STAR;

    protected OptionGroup optionGroup;

    public OpticsTypeRow() {
        initComponents();

        addLeftComponent(new Label(CAPTION));
        addRightComponent(optionGroup);
    }

    public void addValueChangeListener(Property.ValueChangeListener listener) {
        this.optionGroup.addValueChangeListener(listener);
    }

    protected void initComponents() {
        this.optionGroup = new OptionGroup();
        this.optionGroup.setImmediate(true);
        this.optionGroup.setMultiSelect(false);
        this.optionGroup.setNullSelectionAllowed(false);

        this.optionGroup.addItem(OpticsType.BETA_STAR);
        this.optionGroup.addItem(OpticsType.CURRENTS);
        this.optionGroup.addItem(OpticsType.MAGNET_STRENGTH);
        this.optionGroup.select(DEFAULT_VALUE);
    }

    public OpticsType getValue() {
        return (OpticsType) optionGroup.getValue();
    }

    public void reset() {
        optionGroup.unselect(optionGroup.getValue());
        optionGroup.select(DEFAULT_VALUE);
    }
}
