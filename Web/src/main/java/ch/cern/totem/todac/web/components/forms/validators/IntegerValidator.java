package ch.cern.totem.todac.web.components.forms.validators;

import com.vaadin.data.validator.AbstractStringValidator;

@SuppressWarnings("serial")
public class IntegerValidator extends AbstractStringValidator {

	public IntegerValidator() {
		super("Value has to be an integer");
	}

	@Override
	protected boolean isValidValue(String value) {
		try {
			Integer.parseInt(value);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}

}
