package ch.cern.totem.todac.web.components.alignment;

import java.io.File;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.forms.EndRunSelectRow;
import ch.cern.totem.todac.web.components.forms.LabelRow;
import ch.cern.totem.todac.web.components.forms.StartRunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.UploadRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.alignment.AlignmentDao;
import ch.cern.totem.todac.web.model.alignment.AlignmentUploadParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.todac.web.utils.UploadReceiver;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

@SuppressWarnings("serial")
public class AlignmentPopulateForm extends FormLayout {

	protected static final String BUTTON = "Populate";

	protected StartRunSelectRow startRunRow;
	protected EndRunSelectRow endRunRow;
	protected UploadRow uploadRow;
	protected LabelRow labelRow;
	protected SubmitRow submitRow;

	protected AlignmentDao alignmentDao;

	public AlignmentPopulateForm() {
		try {
			this.initComponents();
		} catch (TudasException e) {
			Notificator.showError(e);
		}
	}

	protected void initComponents() throws TudasException {
		alignmentDao = new AlignmentDao();

		startRunRow = new StartRunSelectRow();
		endRunRow = new EndRunSelectRow();
		labelRow = new LabelRow();
		uploadRow = new UploadRow();
		submitRow = new SubmitRow();

		uploadRow.setUploadReceiver(new UploadReceiver(new SucceededListener() {
			public void uploadSucceeded(SucceededEvent event) {
				File file = uploadRow.getFile();

				try {
					alignmentDao.uploadAlignments(getParameters());
					Notificator.showTrayMessage("Upload Successfull", file.getAbsolutePath());
				} catch (InvalidArgumentException e) {
					Notificator.showError(e);
				}

				file.delete();
				reset();
			}
		}));

		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				uploadRow.submitUpload();
			}
		});

		addComponent(startRunRow);
		addComponent(endRunRow);
		addComponent(labelRow);
		addComponent(uploadRow);
		addComponent(submitRow);

		reset();
	}

	protected AlignmentUploadParameters getParameters() throws InvalidArgumentException {
		try {
			Integer startRun = Integer.parseInt(startRunRow.getValue());
			Integer endRun = Integer.parseInt((endRunRow.isChecked() ? endRunRow.getValue() : startRunRow.getValue()));
			String label = labelRow.getValue();
			String filepath = uploadRow.getFile().getAbsolutePath();
			return new AlignmentUploadParameters(startRun, endRun, label, filepath);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
