package ch.cern.totem.todac.web.model.geometry;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.populators.geometry.commons.Convert;
import ch.cern.totem.todac.web.converters.GeometryConverter;

import java.util.Map;
import java.util.TreeMap;

public class GeometryData {

    private final Map<String, Double> map;

    public GeometryData(Map<String, Double> geometry) {
        this.map = new TreeMap<>();
        putAll(geometry);
    }

    private void putAll(Map<String, Double> geometry) {
        try {
            for (Map.Entry<String, Double> entry : geometry.entrySet()) {
                String rpName = Convert.DbIntToRPName(entry.getKey());
                map.put(rpName, entry.getValue());
            }
        } catch (InvalidArgumentException e) {
            map.clear();
            map.putAll(geometry);
        }
    }

    public Map<String, Double> getMap() {
        return new TreeMap<>(map);
    }

    public String parseCsv() throws InvalidArgumentException {
        return GeometryConverter.convert(map);
    }
}
