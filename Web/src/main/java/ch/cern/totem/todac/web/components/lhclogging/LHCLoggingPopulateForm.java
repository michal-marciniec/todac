package ch.cern.totem.todac.web.components.lhclogging;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.EndRunSelectRow;
import ch.cern.totem.todac.web.components.forms.StartRunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.lhclogging.components.LHCVariablesRow;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingDao;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import java.util.Set;

@SuppressWarnings("serial")
public class LHCLoggingPopulateForm extends FormLayout {

    protected StartRunSelectRow startRunRow;
    protected EndRunSelectRow endRunRow;
    protected LHCVariablesRow variablesRow;
    protected SubmitRow submitRow;

    public LHCLoggingPopulateForm() {
        initComponents();
        this.addComponents(startRunRow, endRunRow, variablesRow, submitRow);
    }

    protected void initComponents() {
        startRunRow = new StartRunSelectRow();
        endRunRow = new EndRunSelectRow();
        variablesRow = new LHCVariablesRow();
        submitRow = new SubmitRow();
        submitRow.addLeftClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                reset();
            }
        });
        submitRow.addRightClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                populate();
            }
        });
    }

    protected void populate() {
        try {
            LHCLoggingParameters params = getParameters();
            LHCLoggingDao lhcLoggingDao = new LHCLoggingDao();
            lhcLoggingDao.populate(params.getVariables(), params.getStartRun(), params.getEndRun());
            this.reset();
        } catch (TodacException e) {
            Notificator.showError(e);
        } catch (TudasException e) {
            Notificator.showError(e);
        }
    }

    protected LHCLoggingParameters getParameters() throws InvalidArgumentException {
        try {
            int startRun = Integer.parseInt(startRunRow.getValue());
            int endRun = Integer.parseInt(endRunRow.isChecked() ? endRunRow.getValue() : startRunRow.getValue());
            Set<String> variables = variablesRow.getVariables();

            return new LHCLoggingParameters(startRun, endRun, variables);
        } catch (Exception e) {
            throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
        }
    }

}
