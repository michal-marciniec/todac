package ch.cern.totem.todac.web.model.alignment;

import java.util.HashMap;
import java.util.Map;

import ch.cern.totem.todac.web.converters.AlignmentConverter;
import ch.cern.totem.tudas.tudasUtil.structure.IntervalOfValidity;
import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;

public class AlignmentData {

	protected Map<IntervalOfValidity, RomanPotAlignment[]> alignments;
	protected String label;
	protected Type type;

	public AlignmentData() {
		this.alignments = new HashMap<IntervalOfValidity, RomanPotAlignment[]>();
		this.label = "";
		this.type = Type.AlignmentDescription;
	}
	
	public AlignmentData(Map<IntervalOfValidity, RomanPotAlignment[]> alignments, String label) {
		this.alignments = alignments;
		this.label = label;
		this.type = AlignmentConverter.containsRomanPot(this.alignments) ? Type.AlignmentDescription : Type.AlignmentSequenceDescription;
	}

	public Map<IntervalOfValidity, RomanPotAlignment[]> getData() {
		return alignments;
	}

	public String getLabel() {
		return label;
	}

	public String getFile() {
		return AlignmentConverter.convert(this);
	}
	
	public Type getType() {
		return type;
	}
	
	public enum Type {
		AlignmentDescription, AlignmentSequenceDescription
	}

}
