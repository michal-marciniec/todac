package ch.cern.totem.todac.web.model.optics;

import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.populators.geometry.commons.Convert;

import java.util.Map;
import java.util.TreeMap;

public class OpticsData {

    private final Map<String, Double> map;

    public OpticsData(Map<String, Double> optics) {
        this.map = new TreeMap<>();
        putAll(optics);
    }

    private void putAll(Map<String, Double> geometry) {
        try {
            for (Map.Entry<String, Double> entry : geometry.entrySet()) {
                String rpName = Convert.DbIntToRPName(entry.getKey());
                map.put(rpName, entry.getValue());
            }
        } catch (InvalidArgumentException e) {
            map.clear();
            map.putAll(geometry);
        }
    }

    public Map<String, Double> getMap() {
        return new TreeMap<>(map);
    }

    public String parseCsv() {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, Double> entry : map.entrySet()) {
            builder.append(entry.getKey()).append(",").append(entry.getValue()).append("\n");
        }
        return builder.toString();
    }
}
