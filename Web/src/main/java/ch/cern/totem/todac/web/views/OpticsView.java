package ch.cern.totem.todac.web.views;

import ch.cern.totem.todac.web.components.NavigationComponent;
import ch.cern.totem.todac.web.components.optics.OpticsBrowser;
import ch.cern.totem.todac.web.components.optics.OpticsPopulateForm;
import ch.cern.totem.todac.web.navigation.TodacURL;

import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class OpticsView extends TodacView {

	public static final TodacURL URL = new TodacURL("optics", "Optics");

	public OpticsView(NavigationComponent navigationComponent) {
		super(navigationComponent, URL);
	}

	@Override
	protected CustomComponent createPopulateForm() {
		return new OpticsPopulateForm();
	}

	@Override
	protected CustomComponent createBrowser() {
		return new OpticsBrowser();
	}

}
