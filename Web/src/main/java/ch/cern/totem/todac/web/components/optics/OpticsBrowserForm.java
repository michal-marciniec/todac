package ch.cern.totem.todac.web.components.optics;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.OpticsTypeRow;
import ch.cern.totem.todac.web.components.forms.RunSelectRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.VersionRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.forms.derivable.OptionalInputRow;
import ch.cern.totem.todac.web.model.optics.OpticsParameters;
import ch.cern.totem.todac.web.model.optics.OpticsType;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class OpticsBrowserForm extends FormLayout {

    protected BrowserSwitcher switcher;
    protected OpticsTypeRow opticsTypeRow;
    protected RunSelectRow runNumberRow;
    protected OptionalInputRow versionRow;
    protected SubmitRow submitRow;

    public OpticsBrowserForm(BrowserSwitcher switcher) {
        this.switcher = switcher;
        initComponents();
        addComponents(opticsTypeRow, runNumberRow, versionRow, submitRow);
    }

    protected void initComponents() {
        opticsTypeRow = new OpticsTypeRow();
        runNumberRow = new RunSelectRow();
        versionRow = new VersionRow();
        submitRow = new SubmitRow();
        submitRow.addLeftClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                reset();
            }
        });
        submitRow.addRightClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                switcher.switchToBrowser();
            }
        });
    }

    public OpticsParameters getParameters() throws InvalidArgumentException {
        try {
            OpticsType type = opticsTypeRow.getValue();
            int runNumber = Integer.parseInt(runNumberRow.getValue());
            if (versionRow.isChecked()) {
                int version = Integer.parseInt(versionRow.getValue());
                return new OpticsParameters(type, runNumber, version);
            } else {
                return new OpticsParameters(type, runNumber);
            }
        } catch (Exception e) {
            throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
        }
    }
}
