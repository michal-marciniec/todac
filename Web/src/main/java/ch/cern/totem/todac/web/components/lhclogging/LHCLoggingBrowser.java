package ch.cern.totem.todac.web.components.lhclogging;

import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingDao;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingData;
import ch.cern.totem.todac.web.model.lhclogging.LHCLoggingParameters;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import com.vaadin.ui.CustomComponent;

@SuppressWarnings("serial")
public class LHCLoggingBrowser extends CustomComponent implements BrowserSwitcher {

    protected LHCLoggingBrowserForm searchBox;

    public LHCLoggingBrowser() {
        initComponents();
        switchToSearchBox();
    }

    protected void initComponents() {
        this.searchBox = new LHCLoggingBrowserForm(this);
    }

    @Override
    public void switchToBrowser() {
        try {
            LHCLoggingParameters query = searchBox.getParameters();
            LHCLoggingData tableContent = getData(query);
            this.setCompositionRoot(new LHCLoggingResults(tableContent, this));
        } catch (TodacException e) {
            Notificator.showError(e);
        } catch (TudasException e) {
            Notificator.showError(e);
        }
    }

    @Override
    public void switchToSearchBox() {
        this.setCompositionRoot(searchBox);
    }

    protected LHCLoggingData getData(LHCLoggingParameters query) throws TudasException, DatabaseException {
        LHCLoggingDao lhcLoggingDao = new LHCLoggingDao();
        LHCLoggingData data = lhcLoggingDao.fetch(query);

        return data;
    }
}
