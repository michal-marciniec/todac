package ch.cern.totem.todac.web.model.lhclogging;

import ch.cern.totem.todac.commons.exceptions.DatabaseException;
import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.lhclogging.LHCLoggingPopulator;
import ch.cern.totem.todac.web.model.populators.Populators;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurement;
import ch.cern.totem.tudas.tudasUtil.structure.GeneralMeasurementIOV;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Set;

public class LHCLoggingDao {

    private static Logger logger = Logger.getLogger(LHCLoggingDao.class);

    public void populate(Set<String> variables, int startRun, int endRun) throws TodacException, TudasException {
        TodacException failure = null;
        final LHCLoggingPopulator populator = Populators.getLhcLoggingPopulator();
        populator.initialize();
        for (String variable : variables) {
            try {
                populator.populateByRun(variable, startRun, endRun);
            } catch (TodacException e) {
                failure = e;
            }
        }
        if (failure != null) Notificator.showError(failure);
        else Notificator.showTrayMessage("Success", "");
    }

    public LHCLoggingData fetch(LHCLoggingParameters query) throws TudasException, DatabaseException {
        LHCLoggingData data = new LHCLoggingData();
        final LHCLoggingPopulator populator = Populators.getLhcLoggingPopulator();

        for (String variable : query.getVariables()) {
            try {
                final Map<GeneralMeasurementIOV, GeneralMeasurement[]> fetchedMeasurements = populator.fetch(variable, query.getStartRun(), query.getEndRun());
                data.add(variable, fetchedMeasurements);
            } catch (Exception e) {
                logger.warn(variable, e);
            }
        }
        return data;
    }
}
