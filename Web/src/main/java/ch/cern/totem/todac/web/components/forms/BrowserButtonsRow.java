package ch.cern.totem.todac.web.components.forms;

@SuppressWarnings("serial")
public class BrowserButtonsRow extends ButtonsRow {

    private final static String LEFT_CAPTION = "New search";
    private final static String RIGHT_CAPTION = "Download";

    public BrowserButtonsRow() {
        super(LEFT_CAPTION, RIGHT_CAPTION);
    }

	@Override
	public void reset() {

	}
}
