package ch.cern.totem.todac.web.navigation;

public class TodacURL {

	protected final String url;
	protected final String name;

	public TodacURL(String url, String name) {
		this.url = url;
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public String getName() {
		return name;
	}

}