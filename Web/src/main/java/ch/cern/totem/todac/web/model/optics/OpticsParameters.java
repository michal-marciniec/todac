package ch.cern.totem.todac.web.model.optics;

public class OpticsParameters {
    private OpticsType type;
    private int runNumber;
    private int version;

    public OpticsParameters(OpticsType type, int runNumber, int version) {
        this.type = type;
        this.runNumber = runNumber;
        this.version = version;
    }

    public OpticsParameters(OpticsType type, int runNumber) {
        this(type, runNumber, 0);
    }

    public OpticsType getType() {
        return type;
    }

    public int getRunNumber() {
        return runNumber;
    }

    public int getVersion() {
        return version;
    }
}
