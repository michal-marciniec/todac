package ch.cern.totem.todac.web.components.runinfo;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.forms.derivable.InfoRow;
import ch.cern.totem.todac.web.model.runinfo.RunInformationDao;
import ch.cern.totem.todac.web.utils.Notificator;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class FakeRunInformationPopulateForm extends FormLayout {

	protected static final String STYLE_NAME = "home";
	protected static final String STYLE_LARGE_NAME = "label-large";
	protected static final String STYLE_LINK_LARGE_NAME = "link-large";

	protected InfoRow infoRow;

	protected RunInformationDao runInformationDao;

	public FakeRunInformationPopulateForm() {
		try {
			this.initComponents();
		} catch (TudasException e) {
			Notificator.showError(e);
		} catch (TodacException e) {
			Notificator.showError(e);
		}
	}

	protected void initComponents() throws TudasException, TodacException {
		Link link = new Link("here", new ExternalResource("https://twiki.cern.ch/twiki/bin/view/TOTEM/CompDBTodacTotemOfflineDatabaseAccessConsole"));
		link.setStyleName(STYLE_LINK_LARGE_NAME);
		Label row1 = new Label("Due to CERN security restrictions, requested functionality is not available in Web application.");
		Label row2 = new Label("Please use TODAC Command-Line application.");
		Label row3 = new Label("Documentation is available");

		row1.setStyleName(STYLE_LARGE_NAME);
		row2.setStyleName(STYLE_LARGE_NAME);
		row3.setStyleName(STYLE_LARGE_NAME);

		content = new VerticalLayout(row1, row2, new HorizontalLayout(row3, link));
		content.setStyleName(STYLE_NAME);
		setCompositionRoot(content);
	}
}
