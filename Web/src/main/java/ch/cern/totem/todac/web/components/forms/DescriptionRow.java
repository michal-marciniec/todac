package ch.cern.totem.todac.web.components.forms;

import ch.cern.totem.todac.web.components.forms.derivable.InputRow;

@SuppressWarnings("serial")
public class DescriptionRow extends InputRow {

	protected static final String CAPTION = "Description";

	public DescriptionRow() {
		super(CAPTION);
	}

}