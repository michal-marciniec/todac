package ch.cern.totem.todac.web.components.alignment;

import ch.cern.totem.todac.web.converters.AlignmentConverter;
import ch.cern.totem.todac.web.model.Constants;
import ch.cern.totem.todac.web.model.alignment.AlignmentData;
import ch.cern.totem.todac.web.model.alignment.AlignmentData.Type;
import ch.cern.totem.tudas.tudasUtil.structure.IntervalOfValidity;
import ch.cern.totem.tudas.tudasUtil.structure.RomanPotAlignment;
import com.vaadin.ui.Table;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("serial")
public class AlignmentDataTable extends Table {

    protected final static int PAGE_LENGTH = 25;
	protected static final String COLUMN_ID = "id";
	protected static final String[] COLUMNS = { "sh_r", "sh_r_e", "sh_x", "sh_y", "sh_x_e", "sh_y_e", "rot_z", "rot_z_e", "sh_z", "sh_z_e" };
	protected static final String[] COLUMNS_SEQUENCE = { "sh_x", "sh_y", "rot_z" };
	protected static final String[] COLUMNS_IOV = { "IOV first", "IOV last" };

	protected AlignmentData alignments;
	protected int id;

	public AlignmentDataTable(AlignmentData alignments) {
		this.alignments = alignments;
		this.id = 0;
		this.initComponents();
	}

	protected void initComponents() {
		setImmediate(true);
        setSelectable(true);
        setMultiSelect(true);
		
		// initialize table
		String[] columns = alignments.getType() == Type.AlignmentDescription ? COLUMNS : COLUMNS_SEQUENCE;
		addContainerProperty(COLUMN_ID, String.class, 0);
		for (String column : columns) {
			addContainerProperty(column, Double.class, Double.NaN);
		}
		for(String column : COLUMNS_IOV) {
			addContainerProperty(column, String.class, 0);
		}
		
		final SimpleDateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		// initialize values
		for(IntervalOfValidity iov : alignments.getData().keySet()) {
			RomanPotAlignment[] rpAlignments = alignments.getData().get(iov);
			List<RomanPotAlignment> romanPotAlignments = new LinkedList<RomanPotAlignment>();
			for (int i = 0; i < rpAlignments.length; ++i) {
				romanPotAlignments.add(rpAlignments[i]);
			}
			AlignmentConverter.sort(romanPotAlignments);
			
			for(RomanPotAlignment rpAlignment : romanPotAlignments) {
				Object[] values = new Object[1 + columns.length + COLUMNS_IOV.length];
				values[0] = rpAlignment.romanPotLabel;
				for(int i = 0; i < columns.length; ++i){
					String column = columns[i];
					values[i + 1] = rpAlignment.alignmentValues.get(column);
				}
				values[values.length - 2] = dateFormat.format(new Timestamp(iov.startTime));
				values[values.length - 1] = dateFormat.format(new Timestamp(iov.endTime));
				addItem(values, getNewId());
			}
		}

        if (size() > PAGE_LENGTH) this.setPageLength(PAGE_LENGTH);
        else this.setPageLength(size());
	}
	
	protected Integer getNewId() {
		return new Integer(id++);
	}

    public AlignmentData getContents() {
        return alignments;
    }

}
