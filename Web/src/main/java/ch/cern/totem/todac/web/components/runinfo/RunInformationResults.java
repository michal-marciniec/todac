package ch.cern.totem.todac.web.components.runinfo;

import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.ButtonsRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.components.forms.derivable.InfoRow;
import ch.cern.totem.todac.web.model.runinfo.RunInformationCheckParameters;
import ch.cern.totem.todac.web.model.runinfo.RunInformationData;
import ch.cern.totem.tudas.tudasUtil.structure.RunInformation;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class RunInformationResults extends FormLayout {

	protected static final String NO_RESULTS = "Specified timestamp does not match any run timepan";
	protected static final String RESULT = "Specified timestamp matches run: ";

	protected BrowserSwitcher browserSwitcher;

	protected RunInformationData runInformation;
	protected InfoRow infoRow;
	protected ButtonsRow buttonsRow;

	public RunInformationResults(BrowserSwitcher browserSwitcher, RunInformationData runInformation) {
		this.browserSwitcher = browserSwitcher;
		this.runInformation = runInformation;
		this.initComponents();
	}

	protected void initComponents() {
		infoRow = new InfoRow(NO_RESULTS);

		buttonsRow = new ButtonsRow("New search", null);
		buttonsRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				browserSwitcher.switchToSearchBox();
			}
		});

		addComponent(infoRow);
		addComponent(buttonsRow);
	}

	public void search(RunInformationCheckParameters parameters) {
		int run = search(parameters.getTimestamp());
		infoRow.setValue(run < 0 ? NO_RESULTS : RESULT + run);
	}

	protected int search(long timestamp) {
		RunInformation[] runs = runInformation.getRunInformation();
		for (RunInformation run : runs) {
			if (timestamp >= run.firstEventTime && timestamp <= run.lastEventTime) {
				return run.runNumber;
			}
		}
		return Integer.MIN_VALUE;
	}

}
