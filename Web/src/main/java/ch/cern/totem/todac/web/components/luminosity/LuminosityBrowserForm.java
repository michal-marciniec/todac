package ch.cern.totem.todac.web.components.luminosity;

import ch.cern.totem.todac.commons.exceptions.ExceptionMessages;
import ch.cern.totem.todac.commons.exceptions.InvalidArgumentException;
import ch.cern.totem.todac.web.components.BrowserSwitcher;
import ch.cern.totem.todac.web.components.forms.LabelRow;
import ch.cern.totem.todac.web.components.forms.LuminosityTypeRow;
import ch.cern.totem.todac.web.components.forms.SubmitRow;
import ch.cern.totem.todac.web.components.forms.TimestampRow;
import ch.cern.totem.todac.web.components.forms.VersionRow;
import ch.cern.totem.todac.web.components.forms.derivable.FormLayout;
import ch.cern.totem.todac.web.model.luminosity.LuminosityDownloadParameters;
import ch.cern.totem.todac.web.model.luminosity.LuminosityType;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class LuminosityBrowserForm extends FormLayout {

	protected BrowserSwitcher browserSwitcher;

	protected LabelRow labelRow;
	protected TimestampRow timestampRow;
	protected LuminosityTypeRow luminosityTypeRow;
	protected VersionRow versionRow;
	protected SubmitRow submitRow;

	public LuminosityBrowserForm(BrowserSwitcher browserSwitcher) {
		this.browserSwitcher = browserSwitcher;
		this.initComponents();
	}

	protected void initComponents() {
		labelRow = new LabelRow();
		timestampRow = new TimestampRow();
		luminosityTypeRow = new LuminosityTypeRow();
		versionRow = new VersionRow();
		submitRow = new SubmitRow();
		submitRow.addLeftClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				reset();
			}
		});
		submitRow.addRightClickListener(new ClickListener() {
			public void buttonClick(ClickEvent event) {
				browserSwitcher.switchToBrowser();
			}
		});

		addComponent(luminosityTypeRow);
		addComponent(labelRow);
		addComponent(timestampRow);
		addComponent(versionRow);
		addComponent(submitRow);
	}

	public LuminosityDownloadParameters getParameters() throws InvalidArgumentException {
		try {
			String timestamp = timestampRow.getValue();
			String label = labelRow.getValue();
			LuminosityType type = luminosityTypeRow.getValue();
			Integer version = versionRow.isChecked() ? Integer.parseInt(versionRow.getValue()) : -1;
			return new LuminosityDownloadParameters(label, timestamp, type, version);
		} catch (Exception e) {
			throw new InvalidArgumentException(ExceptionMessages.INVALID_PARAMETERS);
		}
	}

}
