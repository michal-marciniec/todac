package ch.cern.totem.todac.web.components;

import ch.cern.totem.todac.web.MyVaadinUI;
import ch.cern.totem.todac.web.navigation.Dispatcher;
import ch.cern.totem.todac.web.navigation.TodacURL;
import ch.cern.totem.todac.web.views.AlignmentView;
import ch.cern.totem.todac.web.views.GeometryView;
import ch.cern.totem.todac.web.views.LHCLoggingView;
import ch.cern.totem.todac.web.views.LuminosityView;
import ch.cern.totem.todac.web.views.OpticsView;
import ch.cern.totem.todac.web.views.RunInfoView;

import com.vaadin.data.Property;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;

@SuppressWarnings("serial")
public class NavigationMenu extends Table {

	protected static final String STYLE_NAME = "menu-table";
	protected Dispatcher dispatcher;

	public NavigationMenu() {
		this.dispatcher = ((MyVaadinUI) UI.getCurrent()).getDispatcher();
		this.initComponents();
	}

	protected void initComponents() {
		setSelectable(true);
		setSizeUndefined();
		setMultiSelect(false);
		setImmediate(true);
		setNullSelectionAllowed(true);
		setColumnHeaderMode(Table.ColumnHeaderMode.HIDDEN);
		addContainerProperty("Menu", String.class, "");

		addValueChangeListener(new Property.ValueChangeListener() {
			@Override
			public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
				final Object selectedItemId = getValue();
				if (selectedItemId != null) {
					setValue(null);
					dispatcher.navigateTo(selectedItemId.toString());
				}
			}
		});

		setStyleName(STYLE_NAME);
		setPageLength(size());
	}

	public void add(TodacURL url) {
		addItem(new Object[] { url.getName() }, url.getUrl());
	}

	public static NavigationMenu build() {
		NavigationMenu navigationMenu = new NavigationMenu();
		navigationMenu.add(AlignmentView.URL);
		navigationMenu.add(GeometryView.URL);
		navigationMenu.add(LHCLoggingView.URL);
		navigationMenu.add(LuminosityView.URL);
		navigationMenu.add(OpticsView.URL);
		navigationMenu.add(RunInfoView.URL);
		return navigationMenu;
	}

}