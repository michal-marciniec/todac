package ch.cern.totem.todac.web.model.geometry;

public class GeometryParameters {
    private final int runNumber;
    private final int endRun;
    private final String label;
    private final int version;

    public GeometryParameters(int runNumber, int endRun, String label, int version) {
        this.runNumber = runNumber;
        this.endRun = endRun;
        this.label = label;
        this.version = version;
    }

    public GeometryParameters(int runNumber, String label, int version) {
        this(runNumber, runNumber, label, version);
    }

    public GeometryParameters(int runNumber, int endRun, String label) {
        this(runNumber, endRun, label, 0);
    }

    public GeometryParameters(int runNumber, String label) {
        this(runNumber, runNumber, label, 0);
    }

    public int getEndRun() {
        return endRun;
    }

    public Integer getRunNumber() {
        return runNumber;
    }

    public String getLabel() {
        return label;
    }

    public int getVersion() {
        return version;
    }
}
