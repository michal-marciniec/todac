package ch.cern.totem.todac.web.model.alignment;

public class AlignmentDownloadParameters {

	protected Integer startRun; 
	protected Integer endRun; 
	protected String label;
	protected Integer version;

	public AlignmentDownloadParameters(Integer startRun, Integer endRun, String label, Integer version) {
		this.startRun = startRun;
		this.endRun = endRun;
		this.label = label;
		this.version = version;
	}

	public Integer getStartRun() {
		return startRun;
	}
	
	public Integer getEndRun() {
		return endRun;
	}
	
	public String getLabel() {
		return label;
	}
	
	public Integer getVersion() {
		return version;
	}

}
