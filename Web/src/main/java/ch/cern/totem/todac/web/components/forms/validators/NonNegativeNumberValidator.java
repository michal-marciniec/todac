package ch.cern.totem.todac.web.components.forms.validators;

import com.vaadin.data.validator.AbstractStringValidator;

@SuppressWarnings("serial")
public class NonNegativeNumberValidator extends AbstractStringValidator {

	public NonNegativeNumberValidator() {
		super("Must be a non negative value");
	}

	@Override
	protected boolean isValidValue(String value) {
		if (value == null || value.isEmpty())
			return true;
		try {
			Double val = Double.parseDouble(value);
			return !(val < 0.0);
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
