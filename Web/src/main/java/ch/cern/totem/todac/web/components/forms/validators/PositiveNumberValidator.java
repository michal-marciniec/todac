package ch.cern.totem.todac.web.components.forms.validators;

import com.vaadin.data.validator.AbstractStringValidator;

@SuppressWarnings("serial")
public class PositiveNumberValidator extends AbstractStringValidator {

	public PositiveNumberValidator() {
		super("Must be a positivie value");
	}

	@Override
	protected boolean isValidValue(String value) {
		if (value == null || value.isEmpty())
			return true;
		try {
			Double val = Double.parseDouble(value);
			return val > 0.0;
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
