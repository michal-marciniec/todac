package ch.cern.totem.todac.examples;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.luminosity.LuminosityPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.LuminosityManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class LuminosityExample {

	public static void main(String[] args) {
		DatabaseAccessProvider serviceProvider = null;
		try {
			serviceProvider = DatabaseAccessProvider.getInstance();
			serviceProvider.initialize();
			LuminosityManager luminosityManager = serviceProvider.getLuminosityManager();
			LuminosityPopulator luminosityUploader = new LuminosityPopulator(luminosityManager);

			luminosityUploader
					.saveLuminosityByLs("C:\\Users\\Kronkiel\\Desktop\\CERN\\TotemData\\luminosity\\fill_2814_lumibyls.csv");
		} catch (TudasException e) {
			System.out.println(e);
		} catch (TodacException e) {
			System.out.println(e);
		} finally {
			try {
				serviceProvider.close();
			} catch (TudasException e) {
				System.out.println(e);
			}
		}
	}

}
