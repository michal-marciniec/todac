package ch.cern.totem.todac.examples;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.alignment.AlignmentPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RomanPotManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class AlignmentExample {

	public static void main(String[] args) {
		
		DatabaseAccessProvider serviceProvider = null;
		try {
			serviceProvider = DatabaseAccessProvider.getInstance();
			serviceProvider.initialize();
			RomanPotManager romanPotManager = serviceProvider.getRomanPotManager();
			AlignmentPopulator alignmentUploader = new AlignmentPopulator(romanPotManager);

			alignmentUploader
					.saveAlignments(
							"C:\\Users\\Kronkiel\\Desktop\\CERN\\TotemData\\alignment\\2012_07_07_1a",
							8318, 8318);

			alignmentUploader
					.saveAlignments(
							"C:\\Users\\Kronkiel\\Desktop\\CERN\\TotemData\\alignment\\2012_07_07_2",
							8333, 8341);

			alignmentUploader
					.saveAlignments(
							"C:\\Users\\Kronkiel\\Desktop\\CERN\\TotemData\\alignment\\2012_07_12-13",
							8367, 8372);
		} catch (TudasException e) {
			System.out.println(e);
		} catch (TodacException e) {
			System.out.println(e);
		} finally {
			if(serviceProvider != null){
				try {
					serviceProvider.close();
				} catch (TudasException e) {
					System.out.println(e);
				}
			}
		}
	}

}
