package ch.cern.totem.todac.examples.runInfo;

import java.sql.Timestamp;

import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class RunInfoTest {

	
	public static void main(String[] args) {
		
		DatabaseAccessProvider serviceProvider = null;
        try {
            serviceProvider = DatabaseAccessProvider.getInstance();
            serviceProvider.initialize();

            RunInformationManager runInfoManager = serviceProvider.getRunInformationManager();
            
            int[] runs = runInfoManager.listAllRuns();
            for(int runNo : runs){
            	long start = runInfoManager.loadRunStartTime(runNo);
                long end = runInfoManager.loadRunEndTime(runNo);

            	System.out.println("Run: " + runNo);
                System.out.println("\tStart: \t" + new Timestamp(start * 1000));
                System.out.println("\tEnd: \t" + new Timestamp(end * 1000));
                System.out.println();
            }
            
        } catch (TudasException e) {
        	e.printStackTrace();
        } finally {
            if (serviceProvider != null) {
                try {
                    serviceProvider.close();
                } catch (TudasException e) {
                	e.printStackTrace();
                }
            }
        }
	}

}
