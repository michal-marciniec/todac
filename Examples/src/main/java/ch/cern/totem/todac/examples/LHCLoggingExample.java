/**
 * @author Kamil Mielnik
 * kamil.adam.mielnik@gmail.com
 */

package ch.cern.totem.todac.examples;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ch.cern.totem.todac.populators.lhclogging.LHCLoggingPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class LHCLoggingExample {

	static {
		PropertyConfigurator.configure("log4j.properties");
	}
	
	private static Logger logger = Logger.getLogger(LHCLoggingExample.class);
	
	public static void main(String[] args) {
		try{
			DatabaseAccessProvider databaseAccessProvider = DatabaseAccessProvider.getInstance();
			databaseAccessProvider.initialize();
			LHCLoggingPopulator lhcLoggingPopulator = new LHCLoggingPopulator(databaseAccessProvider);
			lhcLoggingPopulator.populateByRun(8318, 8318);
		
			logger.info("Application finished successfully");
		}
		catch(Exception e){
			logger.error("Main application error", e);
		}
		finally{
			try{
				DatabaseAccessProvider.getInstance().close();
			}
			catch(TudasException e){
				logger.error("Could not close DatabaseAccessProvider.", e);
			}
		}
	}
	
}
