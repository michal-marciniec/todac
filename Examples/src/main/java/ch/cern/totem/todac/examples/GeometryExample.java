package ch.cern.totem.todac.examples;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import ch.cern.totem.todac.populators.geometry.GeometryPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

class DirFileFilter implements FileFilter {
	public boolean accept(File file) {
		return file.isDirectory();
	}
}

class XmlFilenameFilter implements FilenameFilter {
	@Override
	public boolean accept(File arg0, String arg1) {
		return arg1.endsWith(".xml");
	}
}

public class GeometryExample {
	protected static Map<String, Integer[]> mapping;
	protected static Map<Integer, String[]> runsTimestamps;

	//
	// 2010_10_24_offsets: 3589 to 3608
	// 2010_10_26_offsets: 3625 to 3634
	// 2010_10_28_offsets: 3670 to 3683
	//
	// 2011_10_20_1: 6884 to 6929
	// 2011_10_20_2: 6936 to 6943
	// 2011_10_20_3: 6944 to 6947
	//
	// 2012_07_07_1a: 8318
	// 2012_07_07_2: 8333 to 8341
	// 2012_07_12-13: 8367 to 8372
	//
	// 2012_10_24-25_1: 8539 to 8543
	// 2012_10_24-25_2a: 8544
	// 2012_10_24-25_2b: 8555 to 8562
	//
	// 2013_02_11: 9008 to 9010
	// 2013_02_12: 9047 to 9050
	// 2013_02_14: 9078
	//
	static {
		mapping = new HashMap<String, Integer[]>();

		// mapping.put("2010_10_24_offsets", new Integer[] { 3589, 3608 });
		// mapping.put("2010_10_26_offsets", new Integer[] { 3625, 3634 });
		// mapping.put("2010_10_28_offsets", new Integer[] { 3670, 3683 });

		// mapping.put("2011_10_20_1", new Integer[] { 6884, 6929 });
		// mapping.put("2011_10_20_2", new Integer[] { 6936, 6943 });
		// mapping.put("2011_10_20_3", new Integer[] { 6944, 6947 });

		mapping.put("2012_07_07_1a", new Integer[] { 8318, 8318 });
		mapping.put("2012_07_07_2", new Integer[] { 8333, 8341 });
		mapping.put("2012_07_12-13", new Integer[] { 8367, 8372 });

		// mapping.put("2012_10_24-25_1", new Integer[] { 8539, 8543 });
		// mapping.put("2012_10_24-25_2a", new Integer[] { 8544, 8544 });
		// mapping.put("2012_10_24-25_2b", new Integer[] { 8555, 8562 });

		// mapping.put("2013_02_11", new Integer[] { 9008, 9010 });
		// mapping.put("2013_02_12", new Integer[] { 9047, 9050 });
		// mapping.put("2013_02_14", new Integer[] { 9078, 9078 });
	}

	protected static Map<String, String> loadSourceFilesFrom(String path) {
		Map<String, String> directoryToFilepath = new HashMap<String, String>();

		File root = new File(path);
		File[] directories = root.listFiles(new DirFileFilter());

		for (File f : directories) {
			File[] xmlFiles = f.listFiles(new XmlFilenameFilter());
			for (File xml : xmlFiles) {
				directoryToFilepath.put(f.getName(), xml.getAbsolutePath());
			}
		}
		return directoryToFilepath;
	}

	public static void main(String[] args) throws ParseException, TudasException, IOException {

		final String GEOMETRY_DATA_ROOT = "\\\\cern.ch\\dfs\\Users\\m\\mmarcini\\Desktop\\CERN\\2-baza\\TotemData\\geometry\\";

		DatabaseAccessProvider serviceProvider = null;
		try {
			serviceProvider = DatabaseAccessProvider.getInstance();
			serviceProvider.initialize();

			GeometryPopulator geometryPopulator = new GeometryPopulator(serviceProvider);

			Map<String, String> directoryToFilepath = loadSourceFilesFrom(GEOMETRY_DATA_ROOT);
			for (String directory : directoryToFilepath.keySet()) {
				String filepath = directoryToFilepath.get(directory);

				if (mapping.containsKey(directory)) {
					int startRun = mapping.get(directory)[0];
					int endRun = mapping.get(directory)[1];

					try {
						geometryPopulator.parseAndPopulate(filepath, startRun, endRun);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} finally {
			if (serviceProvider != null) {
				try {
					serviceProvider.close();
				} catch (TudasException e) {
					System.out.println(e);
				}
			}
		}

	}
}
