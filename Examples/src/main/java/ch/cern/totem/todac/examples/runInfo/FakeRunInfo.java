package ch.cern.totem.todac.examples.runInfo;


import ch.cern.totem.todac.commons.mexer.Classmexer;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.data.manager.RunInformationManager;
import ch.cern.totem.tudas.clients.java.exception.TudasException;
import ch.cern.totem.tudas.tudasUtil.structure.Event;

public class FakeRunInfo {

	/*
	 * Usage:
	 * java FakeRunInfo <run number> [data size in MB]
	 * 		if data size is not specified then the default value (150MB) will be used
	 */
	public static void main(String[] args) {
		int megaBytes = 100;
		int runNo = 1;
		
		switch(args.length){
		case 2:
			megaBytes = Integer.parseInt(args[1]);
		case 1:
			runNo = Integer.parseInt(args[0]);
			System.out.println("Generating events for run: " + runNo);
			System.out.println("Generating " + megaBytes + "MB of data");
			break;
		default:
			System.out.println("Usage: \njava FakeRunInfo <run number> [data size in MB]");
			System.exit(1);
			break;
		}
		
		final int eventSize = 8 + 4 + 4 + 16; // bytes = long + int + int + ref
		
		int eventsCount = megaBytes * 1024 * 1024 / eventSize;
		System.out.println("Generated " + eventsCount + " events");
		
		String runDesc = "students 2013 | random test data";
		Event[] events = EventsGenerator.generate(eventsCount);
		Event[] events1 = new Event[events.length / 2];
		Event[] events2 = new Event[events.length / 2 + events.length % 2];
		for(int i = 0 ; i < events1.length; ++i)
			events1[i] = events[i];
		for(int i = 0; i < events2.length; ++i)
			events2[i] = events[events1.length + i];
		
		Classmexer.measure(events, "Events");

		DatabaseAccessProvider serviceProvider = null;
        try {
            serviceProvider = DatabaseAccessProvider.getInstance();
            serviceProvider.initialize();

            RunInformationManager runInfoManager = serviceProvider.getRunInformationManager();
            runInfoManager.saveRunInformation(runNo, runDesc, events1);
            runInfoManager.saveRunInformation(runNo, runDesc, events2);

        } catch (TudasException e) {
        	e.printStackTrace();
        } finally {
            if (serviceProvider != null) {
                try {
                    serviceProvider.close();
                } catch (TudasException e) {
                	e.printStackTrace();
                }
            }
        }
	}

}
