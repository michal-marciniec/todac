package ch.cern.totem.todac.examples;

import ch.cern.totem.todac.commons.exceptions.TodacException;
import ch.cern.totem.todac.populators.optics.OpticsPopulator;
import ch.cern.totem.tudas.clients.java.DatabaseAccessProvider;
import ch.cern.totem.tudas.clients.java.exception.TudasException;

public class OpticsExample {
	public static void main(String[] args) throws TudasException, TodacException {
		String[] opticsSource = new String[] { "\\\\cern.ch\\dfs\\Users\\m\\mmarcini\\Desktop\\CERN\\2-baza\\TotemData\\optics" };

		DatabaseAccessProvider serviceProvider = null;
		try {
			serviceProvider = DatabaseAccessProvider.getInstance();
			serviceProvider.initialize();

			OpticsPopulator opticsPopulator = new OpticsPopulator(serviceProvider);
			for (String dataset : opticsSource) {
				opticsPopulator.parseAndPopulateOptics(dataset, 0, 7);
			}

		} finally {
			if (serviceProvider != null) {
				try {
					serviceProvider.close();
				} catch (TudasException e) {
					System.out.println(e);
				}
			}
		}
	}
}
