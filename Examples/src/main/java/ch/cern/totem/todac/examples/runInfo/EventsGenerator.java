package ch.cern.totem.todac.examples.runInfo;

import ch.cern.totem.tudas.tudasUtil.structure.Event;

public class EventsGenerator {

	public static Event[] generate(int count){
		Event[] events = new Event[count];
		
		int currentTimestamp = -1;
		int eventsPerTimeStamp = 100;
		
		int currentBlock = -1;
		int eventsPerBlock = 10000;
		
		for(int i = 0 ; i < count; ++i){
			currentTimestamp += i % eventsPerTimeStamp == 0 ? 1 : 0;
			currentBlock += i % eventsPerBlock == 0 ? 1 : 0;
			
			events[i] = new Event();
			Event current = events[i];
			current.timestamp = currentTimestamp;
			current.number = i;
			current.block = currentBlock;
		}
		
		return events;
	}

}
